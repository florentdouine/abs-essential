//
//  ABS_EssentialUITests.swift
//  ABS EssentialUITests
//
//  Created by Florent Douine on 13/12/2016.
//  Copyright © 2016 Toutdanslapoche. All rights reserved.
//

import XCTest

class ABS_EssentialUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let app = XCUIApplication()
        setupSnapshot(app)
        app.launch()
        
        sleep(3)
        snapshot("01LibraryScreen")
        
        let tabBarsQuery = XCUIApplication().tabBars
        tabBarsQuery.buttons["Navigation"].tap()
        snapshot("02NavigationScreen")
        /*
        app.tables.staticTexts["Génius"].tap()
        app.tables["Chargement"].staticTexts["Pourquoi appelle-t-on les pages jaunes ainsi ?"].tap()
        
        app.buttons["Close Advertisement"].tap()
        
        let tLChargerLePodcastButton = app.buttons["Télécharger le podcast"]
        tLChargerLePodcastButton.tap()
        
        let nonMerciButton = app.alerts["Téléchargement en cours"].buttons["Non merci"]
        nonMerciButton.tap()
        
        let retourButton = app.navigationBars["Navigation"].buttons["Retour"]
        retourButton.tap()
        
        let chargementTable = app.tables["Chargement"]
        chargementTable.staticTexts["Pourquoi les jours de la semaine sont ainsi nommés ?"].tap()
        app.scrollViews.otherElements.staticTexts["Dans cet épisode de"].swipeUp()
        tLChargerLePodcastButton.tap()
        nonMerciButton.tap()
        retourButton.tap()
        chargementTable.staticTexts["Pourquoi roulons-nous à droite ?"].tap()
        tLChargerLePodcastButton.tap()
        nonMerciButton.tap()
        retourButton.tap()
        app.navigationBars["Accueil"].buttons["Navigation"].tap()
        app.tabBars.buttons["Bibliothèque"].tap()
        app.buttons["Podcasts"].tap()
 */
        
        tabBarsQuery.buttons["Bibliothèque"].tap()
        app.buttons["Podcasts"].tap()
        sleep(2)
        snapshot("03BibliothequeScreen")
        
        
    }
    
}
