//
//  WPAddCommentViewController.h
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WPAPIRequest.h"
#import <AVFoundation/AVFoundation.h>

@protocol WPAddCommentViewControllerDelegate <NSObject>
- (void) refreshCommentView;
@end

@interface WPAddCommentViewController : UIViewController <UITextFieldDelegate, WPAPIRequestDelegate, UIAlertViewDelegate> {
    
    UIView *contentView;
    
    UITextField *nomField;
    UITextField *courrielField;
    UITextView *contentField;
    
    NSString *_titre;
    NSString *_link;
    NSString *_tracker;
    int       _articleID;
    
    BOOL _isNom;
    BOOL _isEmail;
    WPComment *_aComment;
}

@property (nonatomic, assign) id<WPAddCommentViewControllerDelegate> delegate;
@property (strong, nonatomic) UIView *chargementView;

/* AUDIO */
@property (nonatomic, strong) AVAudioPlayer *soundPlayer;

- (id)initWithArticleTitre:(NSString*)titre andLink:(NSString*)link andArticleID:(int)articleID andTracker:(NSString*)tracker withDelegate:(id)delegate;

@end