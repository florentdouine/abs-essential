//
//  WPAddCommentViewController.m
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import "WPAddCommentViewController.h"
#import "WPAppDelegate.h"
#import "WPPlistRecup.h"

@implementation WPAddCommentViewController
@synthesize delegate = _delegate;
@synthesize chargementView = _chargementView;
@synthesize soundPlayer = _soundPlayer;

- (id)initWithArticleTitre:(NSString*)titre andLink:(NSString*)link andArticleID:(int)articleID andTracker:(NSString *)tracker withDelegate:(id)delegate
{
    self = [super init];
    if (self) {
        
        [self.view setBackgroundColor:kColorBackground];
        
        _delegate = delegate;
        _titre = titre;
        _link = link;
        _tracker = tracker;
        _articleID = articleID;
        
        [self setTitle:@"Commenter"];
            
        // Retour
        
        UIButton *fermerCustomButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
        [fermerCustomButton setBackgroundImage:[UIImage imageNamed:@"button_fermer_off.png"] forState:UIControlStateNormal];
        [fermerCustomButton setBackgroundImage:[UIImage imageNamed:@"button_fermer_on.png"] forState:UIControlStateHighlighted];
        [fermerCustomButton addTarget:self action:@selector(closeModal) forControlEvents:UIControlEventTouchUpInside];
        [fermerCustomButton setAccessibilityLabel:@"Retour"];
        UIBarButtonItem *fermerButton = [[UIBarButtonItem alloc] initWithCustomView:fermerCustomButton];
        [self.navigationItem setLeftBarButtonItem:fermerButton];
        
        UIButton *sendCustomButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
        [sendCustomButton setBackgroundImage:[UIImage imageNamed:@"button_envoyer_off.png"] forState:UIControlStateNormal];
        [sendCustomButton setBackgroundImage:[UIImage imageNamed:@"button_envoyer_on.png"] forState:UIControlStateHighlighted];
        [sendCustomButton addTarget:self action:@selector(verifierChamps) forControlEvents:UIControlEventTouchUpInside];
        [sendCustomButton setAccessibilityLabel:@"Envoyer"];
        UIBarButtonItem *sendButton = [[UIBarButtonItem alloc] initWithCustomView:sendCustomButton];
        [self.navigationItem setRightBarButtonItem:sendButton];
        
        // View
        CGFloat keyboardHeight = 216.0f;
        if (IS_IOS8) {
            keyboardHeight += 37;
        }
        
        contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 44.0 - keyboardHeight)];
        [contentView setBackgroundColor:kColorBackground];
        [[self view] addSubview:contentView];
        
        // Footer
        
        CGFloat y_bas = 0;
        
        // TextField
        
        [[UITextField appearance] setTintColor:kColorTextRead];
        [[UITextView appearance] setTintColor:kColorTextRead];
        
        _isNom = NO;
        _isEmail = NO;
        
        NSDictionary *dict = [[WPPlistRecup shared] getCommentaireReglages];
        if (![[dict objectForKey:@"nom"] isEqualToString:@""] && [dict objectForKey:@"nom"] != nil) {
            _isNom = YES;
        }
        if (![[dict objectForKey:@"email"] isEqualToString:@""] && [dict objectForKey:@"email"] != nil) {
            _isEmail = YES;
        }
        
        if (!_isNom) {
            UIView *fieldsFond = [[UIView alloc] initWithFrame:CGRectMake(0, y_bas, contentView.frame.size.width, 32)];
            [fieldsFond setBackgroundColor:kColorNextArticles];
            [contentView addSubview:fieldsFond];
            
            UILabel *nomLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0, contentView.frame.size.width - 10.0, 32.0)];
            [nomLabel setText:@"Votre nom :"];
            [nomLabel setBackgroundColor:[UIColor clearColor]];
            [nomLabel setTextColor:kColorTextLight];
            [nomLabel setFont:[UIFont fontWithName:kFontNameBold size:kGetFontSize*14.0]];
            [nomLabel setTextAlignment:NSTextAlignmentLeft];
            [nomLabel setContentMode:UIViewContentModeCenter];
            [nomLabel setAccessibilityElementsHidden:YES];
            [fieldsFond addSubview:nomLabel];
            
            nomField = [[UITextField alloc] initWithFrame:CGRectMake(kGetFontSize*85.0, 0, contentView.frame.size.width - kGetFontSize*90.0, 32.0)];
            [nomField setBackgroundColor:[UIColor clearColor]];
            [nomField setReturnKeyType:UIReturnKeyNext];
            [nomField setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
            [nomField setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*14.0]];
            [nomField setTextColor:kColorTextLight];
            [nomField setAutocorrectionType:UITextAutocorrectionTypeNo];
            [nomField setClearButtonMode:UITextFieldViewModeWhileEditing];
            [nomField setDelegate:self];
            [nomField setAccessibilityLabel:@"Votre nom"];
            [fieldsFond addSubview:nomField];
            
             y_bas += nomField.frame.size.height;
        }
        
        if (!_isEmail) {
            UIView *fieldsFond2 = [[UIView alloc] initWithFrame:CGRectMake(0, y_bas, contentView.frame.size.width, 32)];
            [fieldsFond2 setBackgroundColor:kColorNextArticles];
            [contentView addSubview:fieldsFond2];
            
            UILabel *courrielLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0, contentView.frame.size.width - 10.0, 32.0)];
            [courrielLabel setText:@"Votre email :"];
            [courrielLabel setBackgroundColor:[UIColor clearColor]];
            [courrielLabel setTextColor:kColorTextLight];
            [courrielLabel setFont:[UIFont fontWithName:kFontNameBold size:kGetFontSize*14.0]];
            [courrielLabel setTextAlignment:NSTextAlignmentLeft];
            [courrielLabel setContentMode:UIViewContentModeCenter];
            [courrielLabel setAccessibilityElementsHidden:YES];
            [fieldsFond2 addSubview:courrielLabel];
            
            courrielField = [[UITextField alloc] initWithFrame:CGRectMake(kGetFontSize*92, 0, contentView.frame.size.width - kGetFontSize*110, 32.0)];
            [courrielField setBackgroundColor:[UIColor clearColor]];
            [courrielField setReturnKeyType:UIReturnKeyNext];
            [courrielField setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
            [courrielField setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*14.0]];
            [courrielField setTextColor:kColorTextLight];
            [courrielField setAutocorrectionType:UITextAutocorrectionTypeNo];
            [courrielField setClearButtonMode:UITextFieldViewModeWhileEditing];
            [courrielField setKeyboardType:UIKeyboardTypeEmailAddress];
            [courrielField setDelegate:self];
            [courrielField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
            [courrielField setAccessibilityLabel:@"Votre email"];
            [fieldsFond2 addSubview:courrielField];
            
            y_bas += courrielField.frame.size.height;
        }
        
        // TextView
        
        contentField = [[UITextView alloc] initWithFrame:CGRectMake(0, y_bas, contentView.frame.size.width, contentView.frame.size.height - y_bas)];
        [contentField setBackgroundColor:kColorBackground];
        [contentField setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*14.0]];
        [contentField setShowsVerticalScrollIndicator:YES];
        [contentField setAccessibilityLabel:@"Votre commentaire"];
        [contentView addSubview:contentField];
        
        if (!_isNom) {
            [nomField becomeFirstResponder];
        } else if (!_isEmail) {
            [courrielField becomeFirstResponder];
        } else {
            [contentField becomeFirstResponder];
        }
        
        _chargementView = [[TTChargementView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];;
        [[self view] addSubview:_chargementView];
        [_chargementView setHidden:YES];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    // Google Analytics
    id<GAITracker> googleTracker = [[GAI sharedInstance] trackerWithTrackingId:kTrackingID];
    [googleTracker set:kGAIScreenName value:[NSString stringWithFormat:@"Article : %@ - Commentaires - Ajouter", _titre]];
    [googleTracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == nomField) {
        [nomField resignFirstResponder];
        if (!_isEmail) {
            [courrielField becomeFirstResponder];
        } else {
            [contentField becomeFirstResponder];
        }
        return NO;
    }
    
    if (textField == courrielField) {
        [courrielField resignFirstResponder];
        [contentField becomeFirstResponder];
        return NO;
    }
    
    return NO;
}

#pragma mark - Actions

- (void) verifierChamps {
    
    if (_isEmail && _isNom && [contentField text] != nil && ![[contentField text] isEqualToString:@""]) {
        
        [self prepareSendMessage];
        return;
    }
    
    // Vérifier que les champs ne sont pas vide
    if ([contentField text] == nil || [[contentField text] isEqualToString:@""] || (([nomField text] == nil || [[nomField text] isEqualToString:@""]) && !_isNom)) {
        
    } else {
        
        BOOL isGoodMail = NO;
        
        if (_isEmail) {
            isGoodMail = YES;
        } else {
            if ([courrielField text] != nil && ![[courrielField text] isEqualToString:@""]) {
                
                // Vérifier le format du mail
                NSArray *mail = [[courrielField text] componentsSeparatedByString:@"@"];
                if ([mail count] == 2) {
                    NSString *endMail = [mail objectAtIndex:1];
                    NSArray *endMailArr = [endMail componentsSeparatedByString:@"."];
                    if ([endMailArr count] >= 2) {
                        // Le mail est ok !
                        isGoodMail = YES;
                    }
                }
            }
        }
        
        if (isGoodMail) {
            [self prepareSendMessage];
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur" message:@"Merci de rentrer une adresse mail valide !" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
            return;
        }
    }
}

- (void) prepareSendMessage {
    
    if (_chargementView == nil) {
        [_chargementView setHidden:NO];
    }
    
    // Si ok, envoyer le message
    [self envoyerMessage];
}

- (void) envoyerMessage {
    
    NSLog(@"[AddCommentViewController] Message envoyé !");
    WPComment *aComment = [WPComment new];
    [aComment setMessage:[contentField text]];
    NSDictionary *dict = [[WPPlistRecup shared] getCommentaireReglages];
    if (_isEmail) {
        [aComment setCourriel:[dict objectForKey:@"email"]];
    } else {
        [aComment setCourriel:[courrielField text]];
    }
    if (_isNom) {
        [aComment setName:[dict objectForKey:@"nom"]];
    } else {
        [aComment setName:[nomField text]];
    }
    
    _aComment = aComment;
    [self sauvegardeReglages];
}

- (void) sauvegardeReglages {
    
    if (((!_isNom && ([nomField text] != nil && ![[nomField text] isEqualToString:@""])) || (!_isEmail && ([courrielField text] != nil && ![[courrielField text] isEqualToString:@""])))) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Infos personnelles" message:@"Voulez-vous sauvegarder votre nom et email dans les réglages ?" delegate:self cancelButtonTitle:@"Non" otherButtonTitles:@"Oui", nil];
        [alert setTag:1];
        [alert show];
    } else {
        [[WPAPIRequest shared] postCommentOnPluggin:_aComment andArticleID:_articleID WithDelegate:self];
        return;
    }
}

- (void)openTextField {
    
    [contentField becomeFirstResponder];
}

- (void)resignTextFields {
    
    [nomField resignFirstResponder];
    [courrielField resignFirstResponder];
    [contentField resignFirstResponder];
}

- (void) emptyFields {
    
    [nomField setText:@""];
    [courrielField setText:@""];
    [contentField setText:@""];
}

- (void) closeModal {
    
    [nomField resignFirstResponder];
    [courrielField resignFirstResponder];
    [contentField resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) closeModalWithPostSucceeded {
    
    [nomField resignFirstResponder];
    [courrielField resignFirstResponder];
    [contentField resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:^{
        if ([_delegate respondsToSelector:@selector(refreshCommentView)]) {
            [_delegate refreshCommentView];
        }
    }];
}

#pragma mark - UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) {
        if (buttonIndex == 1) {
            NSString *email = [courrielField text];
            NSString *nom = [nomField text];
            NSDictionary *dict = [[WPPlistRecup shared] getCommentaireReglages];
            if (_isEmail) {
                email = [dict objectForKey:@"email"];
            }
            if (_isNom) {
                nom = [dict objectForKey:@"nom"];
            }
            [[WPPlistRecup shared] sauvCommentaireReglagesWithEmail:email andNom:nom];
        }
        
        [[WPAPIRequest shared] postCommentOnPluggin:_aComment andArticleID:_articleID WithDelegate:self];
        return;
    }
}

#pragma mark - APIRequest Delegate

- (void)finishedPostCommentWithStatut:(RequestStatut)aStatut {
    
    [_chargementView setHidden:YES];
    
    if (aStatut == RequestStatutSucceeded) {
        
        if (kisAccessibiliteSounds) {
            // Son
            if ([[WPPlistRecup shared] getReglageSon]) {
                _soundPlayer = [[AVAudioPlayer alloc]
                                initWithContentsOfURL:[[NSBundle mainBundle]
                                                       URLForResource:@"sound-commentaires-abs"
                                                       withExtension:@"caf"]
                                error:0];
                [_soundPlayer play];
            }
            
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Commenter" message:@"Merci. Votre commentaire a bien été envoyé ! Il sera visible d'ici 5 à 10 minutes." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
        
        [self closeModalWithPostSucceeded];
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur" message:@"Votre commentaire n'a pas pu être envoyé, vérifiez votre connexion internet et réessayez." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
}


@end
