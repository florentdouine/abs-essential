//
//  WPArticleCell.h
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WPArticleCell : UITableViewCell {
    
    UILabel *_titleLabel;
    UILabel *_dateLabel;
    UIImageView *_image;
    BOOL _isPair;
    
    UIImageView *_commentView;
    UILabel *_commentLabel;
    UILabel *_voiceOverCommentLabel;
    
    float _width;
    
    NSMutableArray *_accessibleElements;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier andWidth:(float)width;
- (void)setPost:(TTPost*)item;

@end
