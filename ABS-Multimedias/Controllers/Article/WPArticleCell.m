//
//  WPArticleCell.m
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import "WPArticleCell.h"
#import "UIImageView+WebCache.h"
#import <QuartzCore/QuartzCore.h>

@implementation WPArticleCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier andWidth:(float)width
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _width = width;
        
        // Background
        [self setBackgroundColor:kColorBackground];
        
        // Title
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(kGetFontSize*50+15, 0, self.frame.size.width-100, 45.0*kGetFontSize)];
        [_titleLabel setFont:[UIFont fontWithName:kFontNameBold size:kGetFontSize*kCellTitleHeight]];
        [_titleLabel setTextColor:kColorText];
        [_titleLabel setNumberOfLines:2];
        [_titleLabel setBaselineAdjustment:UIBaselineAdjustmentAlignCenters];
        [_titleLabel setTextAlignment:NSTextAlignmentLeft];
        [_titleLabel setBackgroundColor:[UIColor clearColor]];
        [_titleLabel setAdjustsFontSizeToFitWidth:YES];
        [self addSubview:_titleLabel];
        
        // SubTitle
        _dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(kGetFontSize*50+15, _titleLabel.frame.size.height-10*kGetFontSize, width-kGetFontSize*50-30, 35*kGetFontSize)];
        [_dateLabel setFont:[UIFont fontWithName:kFontNameLight size:kGetFontSize*12.0]];
        [_dateLabel setTextColor:kColorText];
        [_dateLabel setBaselineAdjustment:UIBaselineAdjustmentAlignCenters];
        [_dateLabel setTextAlignment:NSTextAlignmentLeft];
        [_dateLabel setBackgroundColor:[UIColor clearColor]];
        [_dateLabel setAdjustsFontSizeToFitWidth:YES];
        [self addSubview:_dateLabel];
        
        // Image
        [_image removeFromSuperview];
        _image = [[UIImageView alloc] initWithFrame:CGRectMake(kGetFontSize*7.5, kGetFontSize*7.5, kGetFontSize*50.0, 50.0*kGetFontSize)];
        [_image setClipsToBounds:YES];
        [_image setBackgroundColor:[UIColor whiteColor]];
        [_image setContentMode:UIViewContentModeScaleAspectFill];
        [[_image layer] setCornerRadius:kCellArticleCornerRadius];
        [self addSubview:_image];
        
        // Commentaire
        if (_commentView == nil) {
            _commentView = [[UIImageView alloc] initWithFrame:CGRectMake(_image.frame.origin.x + _image.frame.size.width - 10.0, _image.frame.origin.y - 5.0, 15.0*kGetFontSize, 15.0*kGetFontSize)];
            [_commentView setImage:[UIImage imageNamed:@"comment_bulle.png"]];
            [self addSubview:_commentView];
        }
        [_commentView setHidden:YES];
        
        _commentLabel = [[UILabel alloc] initWithFrame:_commentView.frame];
        [_commentLabel setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*8.0]];
        [_commentLabel setTextColor:kColorTextLight];
        [_commentLabel setBaselineAdjustment:UIBaselineAdjustmentAlignCenters];
        [_commentLabel setTextAlignment:NSTextAlignmentCenter];
        [_commentLabel setBackgroundColor:[UIColor clearColor]];
        [_commentLabel setIsAccessibilityElement:NO];
        [self addSubview:_commentLabel];
        
        _voiceOverCommentLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width-2, self.frame.size.height-2, 2, 2)];
        [self addSubview:_voiceOverCommentLabel];
        
    }
    return self;
}

- (void)setPost:(TTPost *)item {
    
    if ([[WPPlistRecup shared] getIsReadForArticleID:item.postID] && item.type != APIWC) {
        [_titleLabel setTextColor:kColorTextRead];
    } else {
        [_titleLabel setTextColor:kColorText];
    }
    
    [_titleLabel setText:item.titre];
    
    // Sous-titre
    NSString *subTitle = @"";
    
    if (item.type == APIAnnuaire) {
        subTitle = [NSString stringWithFormat:@"De %@ - %@ chez %@", item.auteur2, item.prix, item.editeur];
    } else if (item.type == APIPodcast) {
        if ([item.presentateurs isEqualToString:@""]) {
            subTitle = [NSString stringWithFormat:@"Par %@", item.auteur2];
            if (![item.narrateur isEqualToString:@""]) {
                subTitle = [NSString stringWithFormat:@"%@ avec %@", subTitle, item.narrateur];
            }
        } else {
            subTitle = [NSString stringWithFormat:@"Par %@", item.presentateurs];
            if (![item.invites isEqualToString:@""]) {
                subTitle = [NSString stringWithFormat:@"%@ avec %@", subTitle, item.invites];
            }
        }
    } else if (item.soustitre != nil && ![item.soustitre isEqualToString:@""]){
        subTitle = item.soustitre;
    } else {
        subTitle = item.categories;
    }
    [_dateLabel setText:subTitle];
    
//    DLog(@"%@", image);
    NSURL *imageURL = [NSURL URLWithString:item.URLFeatureImg];
    if ([item.URLFeatureImg isEqualToString:@""] || item.URLFeatureImg == nil || imageURL == nil) {
        [_image setHidden:YES];
        _image.image = nil;
        
        CGFloat x = kGetFontSize*7.5;
        CGFloat commentWidth = 0;
        if (item.nbComments > 0) {
            x += 15.0*kGetFontSize+kGetFontSize*7.5;
            commentWidth = 15.0*kGetFontSize;
        }
        
        // Frame
        [_titleLabel setFrame:CGRectMake(x, 0, _width-100 + _image.frame.size.width - commentWidth, 45.0*kGetFontSize)];
        [_dateLabel setFrame:CGRectMake(x, _titleLabel.frame.size.height-10*kGetFontSize, _width - x - kGetFontSize*15, 35*kGetFontSize)];
        [_commentView setFrame:CGRectMake(kGetFontSize*7.5, kGetFontSize*7.5, 15.0*kGetFontSize, 15.0*kGetFontSize)];
        [_commentLabel setFrame:_commentView.frame];
        
    } else {
        
        [_image setHidden:NO];
        [_image sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"placeholder.png"] options:SDWebImageHighPriority completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        }];
        
        // Frame
        [_titleLabel setFrame:CGRectMake(kGetFontSize*50+kGetFontSize*15, 0, _width-100, 45.0*kGetFontSize)];
        [_dateLabel setFrame:CGRectMake(kGetFontSize*50+kGetFontSize*15, _titleLabel.frame.size.height-10*kGetFontSize, _width-kGetFontSize*50-kGetFontSize*30, 35*kGetFontSize)];
        [_commentView setFrame:CGRectMake(_image.frame.origin.x + _image.frame.size.width - 10.0, _image.frame.origin.y - kGetFontSize*5.0, 15.0*kGetFontSize, 15.0*kGetFontSize)];
        [_commentLabel setFrame:_commentView.frame];
    }
    
    if (item.nbComments > 0) {
        [_commentView setHidden:NO];
        [_commentLabel setText:[NSString stringWithFormat:@"%i", item.nbComments]];
        [_voiceOverCommentLabel setAccessibilityLabel:[NSString stringWithFormat:@"%i commentaires", item.nbComments]];
        [_commentLabel adjustsFontSizeToFitWidth];
    } else {
        [_commentView setHidden:YES];
        [_commentLabel setText:@""];
        [_voiceOverCommentLabel setText:@""];
    }
    
    [_image setFrame:CGRectMake(kGetFontSize*7.5, kGetFontSize*7.5, kGetFontSize*50.0, 50.0*kGetFontSize)];
    
    // Font
    [_titleLabel setFont:[UIFont fontWithName:kFontNameBold size:kGetFontSize*kCellTitleHeight]];
    [_dateLabel setFont:[UIFont fontWithName:kFontNameLight size:kGetFontSize*12.0]];
    [_commentLabel setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*8.0]];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}
@end
