//
//  WPArticlesViewController.h
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGORefreshTableHeaderView.h"
#import "WPAPIRequest.h"
#import <AVFoundation/AVFoundation.h>

@protocol WPArticlesViewControllerDelegate <NSObject>
- (void) openArticleViewWithArticle:(TTPost*)item andType:(ArticleType)type;
- (void) openArticleWCViewWithArticle:(TTPost*)item andType:(ArticleType)type;
@end

@interface WPArticlesViewController : UIViewController <UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate, EGORefreshTableHeaderDelegate, WPAPIRequestDelegate, UISearchBarDelegate> {
    
    EGORefreshTableHeaderView   *_refreshHeaderView;
	BOOL                        _reloading;
    
    TTChargementView            *_chargementView;
    int                         pages;
    
    UILabel *introLbl;
    UISearchBar *_searchBar;
    BOOL _isSearchBarOpen;
    
    UILabel *noResultView;
    
    int accueilResults;
}

@property (nonatomic, weak) id<WPArticlesViewControllerDelegate>delegate;
@property (strong, nonatomic) UIView *contentView;
@property (strong, nonatomic) NSArray *articles;
@property (strong, nonatomic) NSArray *podcasts;
@property (strong, nonatomic) NSArray *livreaudios;

@property (strong, nonatomic) UITableView *aTableView;
@property (strong, nonatomic) UIView *reloadView;

@property (assign, nonatomic) ArticlesType theType;
@property (assign, nonatomic) APIType apiType;

/* AUDIO */
@property (nonatomic, strong) AVAudioPlayer *soundPlayer;

- (id)initWithType:(ArticlesType)aType andAPIType:(APIType)APIType andFrame:(CGRect)frame andDelegate:(id)delegate;

- (void) startLoadingView;
- (void) stopLoadingViewWithFooter:(BOOL)isFooter;
- (void) reloadViewWithArticles;
- (void) reloadViewWithArticlesType:(ArticlesType)aType andAPIType:(APIType)APIType;
- (void) reloadArticleViewWithArticles:(NSArray*)articles;
- (void) addReloadViewForCenter:(CGPoint)center;

- (void) reloadTableViewDataSource;
- (void) doneLoadingTableViewData;

@end
