//
//  WPArticlesViewController.m
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import "WPArticlesViewController.h"
#import "UIImageView+WebCache.h"
#import "WPArticleCell.h"

@implementation WPArticlesViewController
@synthesize contentView = _contentView;
@synthesize articles = _articles;
@synthesize aTableView = _aTableView;
@synthesize theType = _theType;
@synthesize apiType = _apiType;
@synthesize delegate = _delegate;
@synthesize soundPlayer = _soundPlayer;

- (id)initWithType:(ArticlesType)aType andAPIType:(APIType)APIType andFrame:(CGRect)frame andDelegate:(id)delegate
{
    self = [super init];
    if (self) {
        
        [self.view setBackgroundColor:kColorBackground];
        
        _theType = aType;
        _apiType = APIType;
        _delegate = delegate;
        
        [self.view setFrame:frame];
        
        // Title
        if (aType == ArticlesAll) {
            [self setTitle:@"Accueil"];
        } else if (_apiType == APIPodcast) {
            UIButton *reloadCustomButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
            [reloadCustomButton setBackgroundImage:[UIImage imageNamed:@"button_reload_off.png"] forState:UIControlStateNormal];
            [reloadCustomButton setBackgroundImage:[UIImage imageNamed:@"button_reload_on.png"] forState:UIControlStateHighlighted];
            [reloadCustomButton addTarget:self action:@selector(refreshAction) forControlEvents:UIControlEventTouchUpInside];
            [reloadCustomButton setAccessibilityLabel:@"Recharger la liste"];
            UIBarButtonItem *reloadButton = [[UIBarButtonItem alloc] initWithCustomView:reloadCustomButton];
            [self.navigationItem setRightBarButtonItem:reloadButton];
            
            NSArray *categories = kArrayPodcatsName;
            [self setTitle:[categories objectAtIndex:aType-1]];
        } else {
            UIButton *reloadCustomButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
            [reloadCustomButton setBackgroundImage:[UIImage imageNamed:@"button_reload_off.png"] forState:UIControlStateNormal];
            [reloadCustomButton setBackgroundImage:[UIImage imageNamed:@"button_reload_on.png"] forState:UIControlStateHighlighted];
            [reloadCustomButton addTarget:self action:@selector(refreshAction) forControlEvents:UIControlEventTouchUpInside];
            [reloadCustomButton setAccessibilityLabel:@"Recharger la liste"];
            UIBarButtonItem *reloadButton = [[UIBarButtonItem alloc] initWithCustomView:reloadCustomButton];
            [self.navigationItem setRightBarButtonItem:reloadButton];
            
            NSArray *categories = kArrayCategoriesName;
            [self setTitle:[categories objectAtIndex:aType-1]];
        }
        
        // ContentView
        
        CGFloat height = self.view.frame.size.height;
        if (aType == 0) {
            height = kViewHeight;
        }
        
        CGFloat y = 0;
        
        if (_apiType == APIAnnuaire) {
        
            y += 10;
            
            // Mettre le texte
            introLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, y, self.view.frame.size.width-20, 0)];
            [introLbl setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*14]];
            [introLbl setText:@"Vous avez un livre ou vous souhaitez en acquérir un, mais vous ne savait pas si il existe en version audio ? Entrez le titre de votre livre dans la zone de recherche ci-dessous, et cliquez sur « Rechercher » nous allons vérifier cela pour vous :"];
            [introLbl setTextColor:kColorText];
            [introLbl setNumberOfLines:0];
            [introLbl setTextAlignment:NSTextAlignmentLeft];
            [self.view addSubview:introLbl];
            [introLbl sizeToFit];
            y += introLbl.frame.size.height+10;
            
            // Mettre un champs de recherche
            _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, y, self.view.frame.size.width, 44)];
            [_searchBar setDelegate:self];
            [self.view addSubview:_searchBar];
            y += _searchBar.frame.size.height;
        }
        
        _contentView = [[UIView alloc] initWithFrame:CGRectMake(0, y, self.view.frame.size.width, height-y)];
        [_contentView setBackgroundColor:kColorBackground];
        [self.view addSubview:_contentView];
        
        _aTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width, _contentView.frame.size.height)];
        [_aTableView setBackgroundColor:kColorBackground];
        [_aTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [_aTableView setSeparatorColor:kColorSeparator];
        [_aTableView setDelegate:self];
        [_aTableView setDataSource:self];
        [_aTableView setShowsVerticalScrollIndicator:NO];
        [_aTableView setAccessibilityLabel:@"Chargement"];
        [_contentView addSubview:_aTableView];
        
        // Load
        _chargementView = [[TTChargementView alloc] initWithFrame:_aTableView.frame];
        [_contentView addSubview:_chargementView];
        [_chargementView setHidden:YES];
        
        // Vue pas de résultats
        noResultView = [[UILabel alloc] initWithFrame:_contentView.frame];
        [noResultView setBackgroundColor:[UIColor clearColor]];
        [noResultView setBaselineAdjustment:UIBaselineAdjustmentAlignCenters];
        [noResultView setNumberOfLines:2];
        [noResultView setText:@"Aucun livre audio ne correspond\nà votre recherche."];
        [noResultView setTextAlignment:NSTextAlignmentCenter];
        [noResultView setTextColor:kColorNextArticles];
        [noResultView setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*14.0]];
        [self.view addSubview:noResultView];
        [noResultView setHidden:YES];
        
        if (_refreshHeaderView == nil) {
            EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0, 0 - _aTableView.bounds.size.height, self.view.frame.size.width, _aTableView.bounds.size.height)];
            view.delegate = self;
            [_aTableView addSubview:view];
            _refreshHeaderView = view;
        }
        
        [self startLoadingView];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[[WPAPIRequest shared] loadArticlesForType:ArticlesAll withDelegate:self];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    // Google Analytics
    id<GAITracker> googleTracker = [[GAI sharedInstance] trackerWithTrackingId:kTrackingID];
    
    NSString *value = @"";
    if (_theType == ArticlesAll) {
        value = @"Accueil";
    } else {
        NSArray *categories = kArrayCategoriesName;
        if ((int)_theType-1 >= 0 && (int)_theType-1 < [categories count]) {
            value = [NSString stringWithFormat:@"Navigation - %@", [categories objectAtIndex:_theType-1]];
        } else {
            value = @"Navigation";
        }
    }
    
    [googleTracker set:kGAIScreenName value:value];
    [googleTracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

#pragma mark - Accessibility

- (BOOL)accessibilityScroll:(UIAccessibilityScrollDirection)direction {
    
    if (direction == UIAccessibilityScrollDirectionUp && _aTableView.contentOffset.y == 0) {
        
        UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, @"Rechargement de la vue.");
        [self reloadViewWithArticles];
        return YES;
    }
    return NO;
}

#pragma mark - Actions
#pragma mark Reload

- (void) refreshAction {
    [self reloadViewWithArticles];
}

- (void)reloadViewWithArticles {
    [self reloadViewWithArticlesType:_theType andAPIType:_apiType];
}

- (void)reloadViewWithArticlesType:(ArticlesType)aType andAPIType:(APIType)APIType {
    
    if (_reloadView != nil) {
        [_reloadView removeFromSuperview];
        _reloadView = nil;
    }
    
    if (_apiType == APIAnnuaire) {
        
        CGFloat y = 10;
        
        [introLbl setFrame:CGRectMake(10, y, self.view.frame.size.width-20, 0)];
        [introLbl setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*14]];
        [introLbl sizeToFit];
        y += introLbl.frame.size.height+10;
        
        [_searchBar setFrame:CGRectMake(0, y, self.view.frame.size.width, 44)];
        y += _searchBar.frame.size.height;
        
        CGFloat height = self.view.frame.size.height;
        if (aType == 0) {
            height = kViewHeight;
        }
        
        [_contentView setFrame:CGRectMake(0, y, self.view.frame.size.width, height-y)];
        [_aTableView setFrame:CGRectMake(0, 0, _contentView.frame.size.width, _contentView.frame.size.height)];
        [_chargementView setFrame:_aTableView.frame];
        [noResultView setFrame:_contentView.frame];
        [noResultView setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*14.0]];
    }
    
    _articles = [NSArray new];
    [_aTableView reloadData];
    [_aTableView setTableFooterView:[UIView new]];
    [_aTableView setScrollEnabled:YES];
    
    _theType = aType;
    _apiType = APIType;
    pages = 1;
    [self startLoadingView];
    if (_theType == ArticlesAll && _apiType == APIPost) {
        accueilResults = 3;
        [[WPAPIRequest shared] loadLast25ArticlesForType:aType withDelegate:self];
        [[WPAPIRequest shared] loadLast5APIJSONArticlesForType:aType andAPIType:APIPodcast withDelegate:self];
        [[WPAPIRequest shared] loadLast25ArticlesFromWooCommerceWithDelegate:self];
    } else {
        if (_apiType == APIPost) {
            [[WPAPIRequest shared] loadArticlesForType:aType withDelegate:self];
        } else {
            [[WPAPIRequest shared] loadAPIJSONArticlesForType:aType andAPIType:APIType withDelegate:self];
        }
    }
}

- (void)startLoadingView {
    [_chargementView setHidden:NO];
}

- (void)stopLoadingViewWithFooter:(BOOL)isFooter {
    
    [_chargementView setHidden:YES];
    if (kisAccessibiliteSounds) {
        // Son
        if ([[WPPlistRecup shared] getReglageSon]) {
            NSError *error;
            _soundPlayer = [[AVAudioPlayer alloc]
                            initWithContentsOfURL:[[NSBundle mainBundle]
                                                   URLForResource:@"sound-rafrechire-postes-abs"
                                                   withExtension:@"caf"]
                            error:&error];
            [_soundPlayer play];
        }
        
    } else {
        UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, @"Contenu chargé");
    }
    
    // Footer
    if (isFooter) {
        UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, kButtonMoreHeight)];
        [footer setBackgroundColor:kColorNextArticles];
        UIButton *btn_charger_plus = [[UIButton alloc] initWithFrame:footer.frame];
        [btn_charger_plus setTitle:@"Articles suivants" forState:UIControlStateNormal];
        [btn_charger_plus setTitleColor:kColorTextLight forState:UIControlStateNormal];
        [btn_charger_plus setTitleColor:kColorText forState:UIControlStateHighlighted];
        [btn_charger_plus setContentMode:UIViewContentModeCenter];
        [[btn_charger_plus titleLabel] setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*16.0]];
        [btn_charger_plus addTarget:self action:@selector(chargerPlusDeNews) forControlEvents:UIControlEventTouchUpInside];
        [footer addSubview:btn_charger_plus];
        [_aTableView setScrollEnabled:YES];
        [_aTableView setTableFooterView:footer];
    } else {
        CGFloat height = _aTableView.frame.size.height - _articles.count*kGetFontSize*65;
        if (height < 0 || _aTableView.numberOfSections > 1) {
            height = 0;
            [_aTableView setScrollEnabled:YES];
        } else {
            [_aTableView setScrollEnabled:NO];
        }
        UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, height)];
        [footer setBackgroundColor:kColorBackground];
        [_aTableView setTableFooterView:footer];
    }
}

- (void)reloadArticleViewWithArticles:(NSArray*)articles {
    
    _articles = articles;
    //    NSLog(@"[ArticleViewController] articles : %@", _articles);
    
    if (_articles == nil) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur de chargement" message:@"Un problème est survenu lors du chargement des articles. Vérifiez votre connexion internet et essayez de recharger la vue." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        [self stopLoadingViewWithFooter:NO];
        [self addReloadViewForCenter:_contentView.center];
        [self doneLoadingTableViewData];
        return;
    }
    
    [_aTableView reloadData];
    
    if (_articles.count == 0) {
        [noResultView setHidden:NO];
    } else {
        [noResultView setHidden:YES];
    }
    
    if (_articles.count < 10 || _isSearchBarOpen) {
        [self stopLoadingViewWithFooter:NO];
    } else {
        [self stopLoadingViewWithFooter:YES];
    }
    [self doneLoadingTableViewData];
}

#pragma mark Charger Plus

- (void)chargerPlusDeNews {
    
//    _reloading = YES;
    [self startLoadingView];
    pages ++;
    if ([_articles count] != 0) {
        if (_apiType == APIPost) {
            [[WPAPIRequest shared] loadMoreArticlesForType:_theType andPage:pages withDelegate:self];
        } else {
            [[WPAPIRequest shared] loadMoreAPIJSONArticlesForType:_theType andAPIType:_apiType andPage:pages withDelegate:self];
        }
    }
}

- (void)addMoreArticles:(NSArray*)moreArticles {
    
    _reloading = NO;
    
    if (moreArticles == nil) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur de chargement" message:@"Un problème est survenu lors du chargement des articles. Vérifiez votre connexion internet et essayez de recharger la vue." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        [self stopLoadingViewWithFooter:YES];
        return;
    }
    
    if ([moreArticles count] == 0) {
        
        [self stopLoadingViewWithFooter:NO];
        
        // Footer en fonction du nb d'articles
        int height = _aTableView.frame.size.height - _articles.count*kGetFontSize*65;
        if (height > 0) {
            UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, height)];
            [footer setBackgroundColor:kColorBackground];
            [_aTableView setTableFooterView:footer];
            [_aTableView setScrollEnabled:NO];
        } else {
            [_aTableView setTableFooterView:[UIView new]];
            [_aTableView setScrollEnabled:YES];
        }
        return;
    }
    
    NSIndexPath *path = [NSIndexPath indexPathForRow:[_articles count]-1 inSection:0];
    
    NSMutableArray *mutArray = [[NSMutableArray alloc] initWithArray:_articles];
    [mutArray addObjectsFromArray:moreArticles];
    _articles = [NSArray arrayWithArray:(NSArray *)mutArray];
    
    [_aTableView reloadData];
    [_aTableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    [self stopLoadingViewWithFooter:YES];
}

#pragma mark - ScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}

#pragma mark - Actions

- (void) goToArticle:(id)sender {
    
    UIButton *btn = (UIButton*)sender;
    //    NSLog(@"[ArticleViewController] ID article : %i", [btn tag]);
    TTPost *item = [_articles objectAtIndex:[btn tag]];
    
    ArticleType articleType = ArticleMenu;
    NSArray *categories = kAPIArrayCategories;
    if (_theType != ArticlesAll) {
        if (categories.count >= _theType-1) {
            if ([[categories objectAtIndex:_theType-1] isEqualToString:@"SW01"]) {
                articleType = ArticleSubWebsite;
            }
        }
        if (_theType != ArticlesAll) {
            articleType = ArticleSubArticle;
        }
        if ((int)_theType >= 200) {
            articleType = ArticleSubWebsiteInfos;
        } else if ((int)_theType > 100) {
            articleType = ArticleSubWebsite;
        }
    }
    
    if ([_delegate respondsToSelector:@selector(openArticleViewWithArticle:andType:)]) {
        [_delegate openArticleViewWithArticle:item andType:articleType];
    }
}

#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (_theType == ArticlesAll && _apiType == APIPost) {
        return 3;
    }
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    NSString *titre;
    switch (section) {
        case 0:
            titre = @"Actualités récentes";
            break;
        case 1:
            titre = @"Derniers livres audios";
            break;
        case 2:
            titre = @"Derniers podcasts";
            break;
            
        default:
            break;
    }
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, kMenuHeaderHeight)];
    [view setBackgroundColor:kColorNextArticles];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0, self.view.frame.size.width-20, kMenuHeaderHeight)];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:kColorTextLight];
    [label setFont:[UIFont fontWithName:kFontNameBold size:kGetFontSize*16.0]];
    [label setTextAlignment:NSTextAlignmentLeft];
    [label setContentMode:UIViewContentModeCenter];
    [label setText:titre];
    [view addSubview:label];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (_theType == ArticlesAll && _apiType == APIPost) {
        return kMenuHeaderHeight;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kGetFontSize*65.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_theType == ArticlesAll && _apiType == APIPost) {
        switch (section) {
            case 0:
                return ([(NSArray*)_articles count]);
                break;
            case 1:
                return ([(NSArray*)_livreaudios count]);
                break;
            case 2:
                return ([(NSArray*)_podcasts count]);
                break;
                
            default:
                break;
        }
    }
    return ([(NSArray*)_articles count]);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *array;
    if (_theType == ArticlesAll && _apiType == APIPost) {
        switch (indexPath.section) {
            case 0:
                array = (NSArray*)_articles;
                break;
            case 1:
                array = (NSArray*)_livreaudios;
                break;
            case 2:
                array = (NSArray*)_podcasts;
                break;
                
            default:
                break;
        }
    } else {
        array = (NSArray*)_articles;
    }
    
    TTPost *item = [array objectAtIndex:indexPath.row];
    
    if (indexPath.section == 1) { // WooCommerce
        // Auteur et prix
        NSString *auteur = item.auteur;
        NSString *price = item.prix;
        RLMResults *livresAudio = [TTLivreAudio objectsWhere:[NSString stringWithFormat:@"identifier == \"%@\" && isBuy == YES", item.identifier]];
        if (livresAudio.count > 0) {
            price = @"Acheté";
        }
        
        NSString *authorAndPrice = [NSString stringWithFormat:@"De %@, %@", auteur, price];
        if ([auteur isEqualToString:@""] || auteur == nil) {
            authorAndPrice = [NSString stringWithFormat:@"%@", price];
        }
        [item setSoustitre:authorAndPrice];
    }
    
    static NSString *CellIdentifier = @"ArticleCell";
    WPArticleCell *cell = (WPArticleCell*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[WPArticleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier andWidth:self.view.frame.size.width];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    [cell setPost:item];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSArray *array;
    if (_theType == ArticlesAll && _apiType == APIPost) {
        switch (indexPath.section) {
            case 0:
                array = (NSArray*)_articles;
                break;
            case 1:
                array = (NSArray*)_livreaudios;
                break;
            case 2:
                array = (NSArray*)_podcasts;
                break;
                
            default:
                break;
        }
    } else {
        array = (NSArray*)_articles;
    }
    
    TTPost *item = [array objectAtIndex:indexPath.row];
    
    if (indexPath.section == 1) {
        ArticleType articleType = ArticleSubWebsite;
        
        if ([_delegate respondsToSelector:@selector(openArticleWCViewWithArticle:andType:)]) {
            [_delegate openArticleWCViewWithArticle:item andType:articleType];
        }
    } else {
        ArticleType articleType = ArticleMenu;
        NSArray *categories = kAPIArrayCategories;
        if (_theType != ArticlesAll) {
            if (categories.count >= _theType-1) {
                if ([[categories objectAtIndex:_theType-1] isEqualToString:@"SW01"]) {
                    articleType = ArticleSubWebsite;
                }
            }
            if (_theType != ArticlesAll) {
                articleType = ArticleSubArticle;
            }
            if ((int)_theType >= 200) {
                articleType = ArticleSubWebsiteInfos;
            } else if ((int)_theType > 100) {
                articleType = ArticleSubWebsite;
            }
        }
        
        if ([_delegate respondsToSelector:@selector(openArticleViewWithArticle:andType:)]) {
            [_delegate openArticleViewWithArticle:item andType:articleType];
        }
    }
}

// No separator inset !
- (void)viewDidLayoutSubviews {
    [_aTableView setSeparatorInset:UIEdgeInsetsZero];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(WPArticleCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]){
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero]; // ios 8 newly added
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - EGORefresh
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource {
    
    if (_reloading) {
        [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:_aTableView];
        return;
    }
    
    _reloading = YES;
    [self reloadViewWithArticles];
}

- (void)doneLoadingTableViewData {
    
	_reloading = NO;
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:_aTableView];
}

#pragma mark - UIScrollViewDelegate Methods

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	
	[_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

#pragma mark - EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view {
	
	[self reloadTableViewDataSource];
    //	[self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:2.0];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view {
	return _reloading;
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view {
	return [NSDate date];
}

#pragma mark - APIDelegate

- (void)finishedLoadAPIJSONArticlesWithStatut:(RequestStatut)aStatut andJSONArray:(NSArray *)aJSON {
    if (_theType == ArticlesAll && _apiType == APIPost) {
        // Ne pas recharger la vue normalement
        _podcasts = aJSON;
        [self reloadViewForAcceuil];
    } else {
        [self finishedLoadArticlesWithStatut:aStatut andJSONArray:aJSON];
    }
}

- (void)finishedLoadArticlesWithStatut:(RequestStatut)aStatut andJSONArray:(NSArray *)aJSON {
    if (_theType == ArticlesAll && _apiType == APIPost) {
        // Ne pas recharger la vue normalement
        _articles = aJSON;
        [self reloadViewForAcceuil];
    } else {
        pages = 1;
        if (aStatut == RequestStatutSucceeded) {
            [self reloadArticleViewWithArticles:aJSON];
            
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur de chargement" message:@"Un problème est survenu lors du chargement des articles. Vérifiez votre connexion internet et essayez de recharger la vue." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
            [self stopLoadingViewWithFooter:NO];
            [self addReloadViewForCenter:_contentView.center];
        }
    }
}

- (void)finishedLoadLast25ArticlesFromWooCommerceWithStatut:(RequestStatut)aStatut andJSONArray:(NSArray *)aJSON {
    if (_theType == ArticlesAll && _apiType == APIPost) {
        // Ne pas recharger la vue normalement
        _livreaudios = aJSON;
        [self reloadViewForAcceuil];
    }
}

- (void)finishedLoadMoreAPIJSONArticlesWithStatut:(RequestStatut)aStatut andJSONArray:(NSArray *)aJSON {
    [self finishedLoadMoreArticlesWithStatut:aStatut andJSONArray:aJSON];
}

- (void)finishedLoadMoreArticlesWithStatut:(RequestStatut)aStatut andJSONArray:(NSArray *)aJSON {
    
    if (aStatut == RequestStatutSucceeded) {
        [self addMoreArticles:aJSON];
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur de chargement" message:@"Un problème est survenu lors du chargement des articles. Vérifiez votre connexion internet et essayez de recharger la vue." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        [self stopLoadingViewWithFooter:YES];
    }
}

- (void)finishedLoadSearchWithStatut:(RequestStatut)aStatut andJSONArray:(NSArray *)aJSON {
    
    if (aStatut == RequestStatutFailed) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur de chargement" message:@"Un problème est survenu lors du chargement des articles. Vérifiez votre connexion internet et essayez de nouveau votre recherche." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        [_chargementView setHidden:YES];
        
        [_searchBar setText:@""];
        _isSearchBarOpen = NO;
        return;
    }
    
    [self reloadArticleViewWithArticles:aJSON];
}

#pragma mark - ReloadView

- (void) addReloadViewForCenter:(CGPoint)center {
    
    if (center.x == 0 && center.y == 0) {
        center = self.view.center;
    }
    
    _reloadView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, _contentView.frame.size.height)];
    [_reloadView setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.2]];
    [self.view addSubview:_reloadView];
    
    UIButton *reloadButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 35.0, 35.0)];
    [reloadButton setCenter:center];
    [reloadButton setBackgroundImage:[UIImage imageNamed:@"button_reload_on.png"] forState:UIControlStateNormal];
    [reloadButton setBackgroundImage:[UIImage imageNamed:@"button_reload_off.png"] forState:UIControlStateHighlighted];
    [reloadButton setAccessibilityLabel:@"Recharger la vue"];
    [reloadButton addTarget:self action:@selector(tryToReload) forControlEvents:UIControlEventTouchUpInside];
    [_reloadView addSubview:reloadButton];
}

- (void) tryToReload {
    [self reloadViewWithArticlesType:_theType andAPIType:_apiType];
}

- (void) reloadViewForAcceuil {
    
    accueilResults --;
    if (accueilResults == 0) {
        // Recharger la vue
        [_chargementView setHidden:YES];
        
        if (_articles == nil || _livreaudios == nil || _podcasts == nil) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur de chargement" message:@"Un problème est survenu lors du chargement des articles. Vérifiez votre connexion internet et essayez de recharger la vue." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
            [self stopLoadingViewWithFooter:NO];
            [self addReloadViewForCenter:_contentView.center];
            [self doneLoadingTableViewData];
            return;
        }
        
        [_aTableView reloadData];
        
        if (_articles.count == 0 && _livreaudios == 0 && _podcasts == 0) {
            [noResultView setHidden:NO];
        } else {
            [noResultView setHidden:YES];
        }
        
        [self stopLoadingViewWithFooter:NO];
        [self doneLoadingTableViewData];
    }
}

#pragma mark - SearchBar Delegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    
    [searchBar setShowsCancelButton:YES animated:YES];
    [searchBar setText:@""];
    _isSearchBarOpen = YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [searchBar setText:@""];
    [self reloadViewWithArticles];
    [searchBar resignFirstResponder];
    _isSearchBarOpen = NO;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    if (![searchBar.text isEqualToString:@""]) {
        _isSearchBarOpen = YES;
        [_chargementView setHidden:NO];
        [[WPAPIRequest shared] makeSearchWithString:searchBar.text andAPIType:APIAnnuaire withDelegate:self];
    } else {
        [self reloadViewWithArticles];
        _isSearchBarOpen = NO;
    }
    [searchBar setShowsCancelButton:NO animated:YES];
    return YES;
}

@end
