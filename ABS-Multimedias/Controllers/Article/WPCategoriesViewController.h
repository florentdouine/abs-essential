//
//  WPCategoriesViewController.h
//  Toutdanslapoche
//
//  Created by Virginie Delaitre on 06/10/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WPAPIRequest.h"

@class WPArticlesWCViewController;

@protocol WPCategoriesViewControllerDelegate <NSObject>
- (void) pushArticlesForType:(ArticlesType)type andLibrairie:(LibrairieType)type;
@end

@interface WPCategoriesViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, WPAPIRequestDelegate, UIAlertViewDelegate> {
    NSString *titleView;
    UISegmentedControl *control;
    LibrairieType _type;
}

@property (strong, nonatomic) id<WPCategoriesViewControllerDelegate>delegate;
@property (strong, nonatomic) UITableView *theTableView;
@property (strong, nonatomic) NSArray *dataSource;
@property (strong, nonatomic) WPArticlesWCViewController *articlesWCController;

- (id) initWithDelegate:(id)delegate andCategories:(NSArray*)categories andTitle:(NSString*)title andType:(LibrairieType)type;
- (void) reloadViewWithCategories:(NSArray*)categories andTitle:(NSString*)title andType:(LibrairieType)type;
- (void) refreshAction;

@end
