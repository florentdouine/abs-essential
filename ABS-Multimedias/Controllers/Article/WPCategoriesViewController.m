//
//  WPCategoriesViewController.m
//  Toutdanslapoche
//
//  Created by Virginie Delaitre on 06/10/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import "WPCategoriesViewController.h"
#import "WPLeftMenuCell.h"
#import "WPArticlesWCViewController.h"

@implementation WPCategoriesViewController
@synthesize theTableView = _theTableView;
@synthesize dataSource = _dataSource;
@synthesize delegate = _delegate;
@synthesize articlesWCController = _articlesWCController;

- (id)initWithDelegate:(id)delegate andCategories:(NSArray *)categories andTitle:(NSString*)title andType:(LibrairieType)type
{
    self = [super init];
    if (self) {
        
        _delegate = delegate;
        titleView = title;
        [self.view setBackgroundColor:kColorBackground];
        _type = type;
        
        CGFloat y = 0;
        if (_type != LibrairieNone) {
            UIView *toolbar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44.0f)];
            [toolbar setBackgroundColor:kColorNavBar];
            control = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"Derniers livres audio", @"Collections", nil]];
            [control setFrame:CGRectMake(0, 0, self.view.frame.size.width, 43.0f)];
            [control addTarget:self action:@selector(selectSubview:) forControlEvents:UIControlEventValueChanged];
            [control setSelectedSegmentIndex:0];
            [control setTintColor:[UIColor whiteColor]];
            
            NSDictionary *attributesSelected = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:kFontNameBold size:kGetFontSize*16.0f], NSFontAttributeName, kColorText, NSForegroundColorAttributeName, nil];
            [control setTitleTextAttributes:attributesSelected forState:UIControlStateSelected];
            
            NSDictionary *attributesUnselected = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*16.0f], NSFontAttributeName, [UIColor whiteColor], NSForegroundColorAttributeName, nil];
            [control setTitleTextAttributes:attributesUnselected forState:UIControlStateNormal];
            
            [toolbar addSubview:control];
            [[self view] addSubview:toolbar];
            y += 44;
        }
        
        _theTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, y, self.view.frame.size.width, kViewHeight - 44.0 -y)];
        [_theTableView setDelegate:self];
        [_theTableView setDataSource:self];
        [_theTableView setRowHeight:kMenuCellHeight];
        [_theTableView setSectionHeaderHeight:kMenuHeaderHeight];
        [_theTableView setBackgroundColor:kColorBackground];
        [_theTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [_theTableView setSeparatorColor:kColorSeparator];
        if (_theTableView.frame.size.height - kMenuCellHeight*[categories count] < 0) {
            [_theTableView setScrollEnabled:YES];
        } else {
            [_theTableView setScrollEnabled:NO];
        }
        [[self view] addSubview:_theTableView];
        
        // Data Source
        _dataSource = categories;
        
        // Articles
        if (_type != LibrairieNone) {
            _articlesWCController = [[WPArticlesWCViewController alloc] initWithType:ArticlesAll andFrame:CGRectMake(0, y, self.view.frame.size.width, self.view.frame.size.height - 44.0 -y) andDelegate:delegate];
            [_articlesWCController reloadViewWithArticlesType:ArticlesAll];
            [[self view] addSubview:_articlesWCController.view];
        }
        
        // Footer
        CGFloat height = _theTableView.frame.size.height - [_dataSource count]*kMenuCellHeight;
        if (height < 0) {
            height = 0;
        }
        UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, height)];
        [footer setBackgroundColor:kColorBackground];
        [_theTableView setTableFooterView:footer];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated {
    
    [_theTableView scrollRectToVisible:CGRectMake(0, 0, _theTableView.frame.size.width, _theTableView.frame.size.height) animated:NO];
    [super viewDidAppear:animated];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    // Google Analytics
    id<GAITracker> googleTracker = [[GAI sharedInstance] trackerWithTrackingId:kTrackingID];
    [googleTracker set:kGAIScreenName value:titleView];
    [googleTracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (void) refreshAction {
    [self reloadViewWithCategories:_dataSource andTitle:titleView andType:_type];
}

- (void)reloadViewWithCategories:(NSArray *)categories andTitle:(NSString *)title andType:(LibrairieType)type {
    
    _type = type;
    titleView = title;
    _dataSource = categories;
    [_theTableView setRowHeight:kMenuCellHeight];
    [_theTableView reloadData];
    
    if (_type != LibrairieNone) {
        [_articlesWCController reloadViewWithArticlesType:ArticlesAll];
    }
    
    NSDictionary *attributesSelected = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:kFontNameBold size:kGetFontSize*16.0f], NSFontAttributeName, kColorText, NSForegroundColorAttributeName, nil];
    [control setTitleTextAttributes:attributesSelected forState:UIControlStateSelected];
    
    NSDictionary *attributesUnselected = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*16.0f], NSFontAttributeName, [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    [control setTitleTextAttributes:attributesUnselected forState:UIControlStateNormal];
    
    // Footer
    CGFloat height = _theTableView.frame.size.height - [_dataSource count]*kMenuCellHeight;
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, height)];
    [footer setBackgroundColor:kColorBackground];
    [_theTableView setTableFooterView:footer];
}

- (void) selectSubview:(UISegmentedControl*)segmentedControl {
    
    switch (segmentedControl.selectedSegmentIndex) {
        case 0:
            _type = LibrairieTous;
            break;
        case 1:
            _type = LibrairieCollection;
            break;
            
        default:
            break;
    }
    if (_type == LibrairieTous) {
        [_articlesWCController.view setHidden:NO];
    } else {
        [_articlesWCController.view setHidden:YES];
    }
}

#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_dataSource count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 35)];
    [header setBackgroundColor:kColorBackground2];
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(20, 7, self.view.frame.size.width-20, 25)];
    [lbl setText:@"Ici la liste des collections..."];
    [lbl setFont:[UIFont fontWithName:kFontNameBold size:16]];
    [header addSubview:lbl];
    [lbl sizeToFit];
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"WPCategorieCell";
    WPLeftMenuCell *cell = (WPLeftMenuCell*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[WPLeftMenuCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier andHeight:kMenuCellHeight];
    }
    
    [cell setTitre:[[_dataSource objectAtIndex:indexPath.row] lastPathComponent] andHeight:kMenuCellHeight];
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kMenuCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    int index = (int)indexPath.row+1;
    
    NSString *categorie = [_dataSource objectAtIndex:indexPath.row];
    NSArray *components = [categorie pathComponents];
    if (components.count > 0) {
        if ([[components objectAtIndex:0] isEqualToString:@"SW01"]) {
            // SubSite
            index +=100;
        }
    }
    
    if ([_delegate respondsToSelector:@selector(pushArticlesForType:andLibrairie:)]) {
        [_delegate pushArticlesForType:index andLibrairie:_type];
    }
}

- (void)viewDidLayoutSubviews {
    [_theTableView setSeparatorInset:UIEdgeInsetsZero];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(WPLeftMenuCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]){
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero]; // ios 8 newly added
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end
