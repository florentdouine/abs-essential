//
//  WPCommentCell.h
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WPCommentCell : UITableViewCell {
    
    UILabel *_pseudoLabel;
    UILabel *_dateLabel;
    
    UIWebView *_commentWebView;
    UILabel *_commentLabel;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier andHeight:(float)height andWidth:(float)width;
- (void)setPseudo:(NSString*)pseudo andDate:(NSString*)date andComment:(NSString*)comment;

@end
