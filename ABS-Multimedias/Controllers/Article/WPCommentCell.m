//
//  WPCommentCell.m
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import "WPCommentCell.h"
#import "WPPlistRecup.h"

@implementation WPCommentCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier andHeight:(float)height andWidth:(float)width
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // Background
        [self setBackgroundColor:kColorBackground];
        
        // Pseudo
        _pseudoLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 10.0, width-120, 20.0)];
        [_pseudoLabel setFont:[UIFont fontWithName:kFontNameBold size:kGetFontSize*14.0]];
        [_pseudoLabel setTextColor:kColorNextArticles];
        [_pseudoLabel setBaselineAdjustment:UIBaselineAdjustmentAlignCenters];
        [_pseudoLabel setTextAlignment:NSTextAlignmentLeft];
        [_pseudoLabel setBackgroundColor:[UIColor clearColor]];
        [self addSubview:_pseudoLabel];
        
        // Date
        _dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(110.0, 10.0, width-120, 20.0)];
        [_dateLabel setFont:[UIFont fontWithName:kFontNameLight size:kGetFontSize*12.0]];
        [_dateLabel setTextColor:kColorNextArticles];
        [_dateLabel setBaselineAdjustment:UIBaselineAdjustmentAlignCenters];
        [_dateLabel setTextAlignment:NSTextAlignmentRight];
        [_dateLabel setBackgroundColor:[UIColor clearColor]];
        [self addSubview:_dateLabel];
        
        // Commentaire
        _commentLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 30, width-20, height-10)];
        [_commentLabel setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*14]];
        [_commentLabel setTextColor:[UIColor blackColor]];
        [_commentLabel setTextAlignment:NSTextAlignmentLeft];
        [_commentLabel setBackgroundColor:[UIColor clearColor]];
        [_commentLabel setNumberOfLines:0];
        [_commentLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [self addSubview:_commentLabel];
        
    }
    return self;
}

- (void)setPseudo:(NSString *)pseudo andDate:(NSString *)date andComment:(NSString *)comment {
    
    [_pseudoLabel setText:pseudo];
    [_dateLabel setText:date];
    [_commentLabel setText:comment];
    
    [_pseudoLabel setFont:[UIFont fontWithName:kFontNameBold size:kGetFontSize*14.0]];
    [_dateLabel setFont:[UIFont fontWithName:kFontNameLight size:kGetFontSize*12.0]];
    [_commentLabel setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*14]];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
