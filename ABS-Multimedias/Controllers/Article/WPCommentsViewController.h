//
//  WPCommentsViewController.h
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WPAPIRequest.h"
#import "WPAddCommentViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface WPCommentsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, WPAPIRequestDelegate, WPAddCommentViewControllerDelegate> {
    
    int _articleID;
    NSString *_titre;
    NSString *_link;
    ArticleType _type;
    
    UILabel *noResultView;
    TTChargementView *_chargementView;
    
    NSString *_tracker;
}

@property (strong, nonatomic) NSArray *commentaires;
@property (strong, nonatomic) UITableView *aTableView;

/* AUDIO */
@property (nonatomic, strong) AVAudioPlayer *soundPlayer;

- (id)initWithArticleType:(ArticleType)type andArticleTitle:(NSString*)title andArticleLink:(NSString*)link andArticleID:(int)articleID;

@end