//
//  WPCommentsViewController.m
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import "WPCommentsViewController.h"
#import "WPCommentCell.h"
#import "WPAddCommentViewController.h"
#import "WPAppDelegate.h"
#import <TTCommons/NSString+HTML.h>

@implementation WPCommentsViewController
@synthesize commentaires = _commentaires;
@synthesize aTableView = _aTableView;
@synthesize soundPlayer = _soundPlayer;

- (id)initWithArticleType:(ArticleType)type andArticleTitle:(NSString*)title andArticleLink:(NSString*)link andArticleID:(int)articleID {
    
    self = [super init];
    if (self) {
        
        [self.view setBackgroundColor:kColorBackground];
        
        _articleID = articleID;
        _titre = title;
        _type = type;
        _link = link;
        
        [self setTitle:@"Commentaires"];
        
        UIButton *fermerCustomButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
        [fermerCustomButton setBackgroundImage:[UIImage imageNamed:@"button_retour_off.png"] forState:UIControlStateNormal];
        [fermerCustomButton setBackgroundImage:[UIImage imageNamed:@"button_retour_on.png"] forState:UIControlStateHighlighted];
        [fermerCustomButton addTarget:self action:@selector(backToList) forControlEvents:UIControlEventTouchUpInside];
        [fermerCustomButton setAccessibilityLabel:@"Retour"];
        UIBarButtonItem *fermerButton = [[UIBarButtonItem alloc] initWithCustomView:fermerCustomButton];
        [self.navigationItem setLeftBarButtonItem:fermerButton];
        
        UIButton *plusCustomButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
        [plusCustomButton setBackgroundImage:[UIImage imageNamed:@"button_plus_off.png"] forState:UIControlStateNormal];
        [plusCustomButton setBackgroundImage:[UIImage imageNamed:@"button_plus_on.png"] forState:UIControlStateHighlighted];
        [plusCustomButton addTarget:self action:@selector(addComment) forControlEvents:UIControlEventTouchUpInside];
        [plusCustomButton setAccessibilityLabel:@"Ajouter commentaire"];
        UIBarButtonItem *plusButton = [[UIBarButtonItem alloc] initWithCustomView:plusCustomButton];
        
        UIButton *reloadCustomButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
        [reloadCustomButton setBackgroundImage:[UIImage imageNamed:@"button_reload_off.png"] forState:UIControlStateNormal];
        [reloadCustomButton setBackgroundImage:[UIImage imageNamed:@"button_reload_on.png"] forState:UIControlStateHighlighted];
        [reloadCustomButton addTarget:self action:@selector(startLoadingView) forControlEvents:UIControlEventTouchUpInside];
        [reloadCustomButton setAccessibilityLabel:@"Recharger la vue"];
        UIBarButtonItem *reloadButton = [[UIBarButtonItem alloc] initWithCustomView:reloadCustomButton];
        
        NSArray *buttonsRight = [NSArray arrayWithObjects: plusButton, reloadButton, nil];
        [self.navigationItem setRightBarButtonItems:buttonsRight];
        
        // Table View
        
        _aTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 44.0)];
        [_aTableView setBackgroundColor:[UIColor clearColor]];
        [_aTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [_aTableView setSeparatorColor:kColorSeparator];
        [_aTableView setDelegate:self];
        [_aTableView setDataSource:self];
        [_aTableView setShowsVerticalScrollIndicator:NO];
        [_aTableView setAccessibilityLabel:@"Chargement"];
        [self.view addSubview:_aTableView];
        
        // Vue pas de résultats
        noResultView = [[UILabel alloc] initWithFrame:_aTableView.frame];
        [noResultView setBackgroundColor:[UIColor clearColor]];
        [noResultView setBaselineAdjustment:UIBaselineAdjustmentAlignCenters];
        [noResultView setNumberOfLines:2];
        NSString *noCommentTitle = @"Aucun commentaire.";
        [noResultView setText:noCommentTitle];
        [noResultView setTextAlignment:NSTextAlignmentCenter];
        [noResultView setTextColor:kColorNextArticles];
        [noResultView setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*14.0]];
        [self.view addSubview:noResultView];
        [noResultView setHidden:YES];
        
        // Footer
        UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, _aTableView.frame.size.height)];
        [footer setBackgroundColor:kColorBackground];
        [_aTableView setTableFooterView:footer];
        
        // Load
        _chargementView = [[TTChargementView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];;
        [[self view] addSubview:_chargementView];
        [_chargementView setHidden:YES];
        
        [self startLoadingView];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    // Google Analytics
    id<GAITracker> googleTracker = [[GAI sharedInstance] trackerWithTrackingId:kTrackingID];
    [googleTracker set:kGAIScreenName value:[NSString stringWithFormat:@"Article : %@ - Commentaires", _titre]];
    [googleTracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

#pragma mark - Actions
#pragma mark Reload

- (void)startLoadingView {
    
    [_chargementView setHidden:NO];
    [_aTableView setAccessibilityLabel:@"Chargement"];
    
    // API Request
    [[WPAPIRequest shared] loadCommentairesForArticleID:_articleID withDelegate:self];
}

- (void)stopLoadingViewWithFooter:(BOOL)isFooter {
    
    [_chargementView setHidden:YES];
    if (kisAccessibiliteSounds) {
        // Son
        if ([[WPPlistRecup shared] getReglageSon]) {
            _soundPlayer = [[AVAudioPlayer alloc]
                            initWithContentsOfURL:[[NSBundle mainBundle]
                                                   URLForResource:@"sound-rafrechire-postes-abs"
                                                   withExtension:@"caf"]
                            error:0];
            [_soundPlayer play];
        }
       
    } else {
        UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, @"Contenu chargé");
    }
    
    // Footer
    if (_aTableView.tableFooterView == nil && isFooter) {
        UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, kButtonMoreHeight)];
        [footer setBackgroundColor:kColorNextArticles];
        UIButton *btn_charger_plus = [[UIButton alloc] initWithFrame:footer.frame];
        [btn_charger_plus setTitle:@"Commentaires suivants" forState:UIControlStateNormal];
        [btn_charger_plus setTitleColor:kColorTextLight forState:UIControlStateNormal];
        [btn_charger_plus setTitleColor:kColorText forState:UIControlStateHighlighted];
        [btn_charger_plus setContentMode:UIViewContentModeCenter];
        [[btn_charger_plus titleLabel] setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*16.0]];
        [btn_charger_plus addTarget:self action:@selector(chargerPlusDeNews) forControlEvents:UIControlEventTouchUpInside];
        [footer addSubview:btn_charger_plus];
        [_aTableView setTableFooterView:footer];
    }
}

- (void)reloadViewWithCommentaires:(NSArray*)commentaires {
    
    _commentaires = commentaires;
    //    NSLog(@"[CommentsViewController] commentaires : %@", _commentaires);
    
    if (_commentaires == nil) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur de chargement" message:@"Un problème est survenu lors du chargement des commentaires. Vérifiez votre connexion internet et essayez de recharger la vue." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        [self stopLoadingViewWithFooter:NO];
        return;
    }
    
    if ([_commentaires count] == 0) {
        
        
        [_aTableView setAccessibilityLabel:@"Aucun commentaire"];
        
        [self stopLoadingViewWithFooter:NO];
        [noResultView setHidden:NO];
        [_aTableView reloadData];
        
        // Footer
        UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, _aTableView.frame.size.height)];
        [footer setBackgroundColor:kColorBackground];
        [_aTableView setTableFooterView:footer];
        
        return;
    }
    
    [noResultView setHidden:YES];
    [noResultView setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*14.0]];
    
    [_aTableView reloadData];
    //    NSIndexPath *path = [NSIndexPath indexPathForRow:0 inSection:0];
    //    [_aTableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionTop animated:YES];
    [_aTableView scrollRectToVisible:CGRectMake(0, 0, _aTableView.frame.size.width, _aTableView.frame.size.height) animated:YES];
    [_aTableView setTableFooterView:[UIView new]];
    
    if ([_commentaires count] < 20) {
        [self stopLoadingViewWithFooter:NO];
    } else {
        [self stopLoadingViewWithFooter:YES];
    }
}

#pragma mark Charger Plus

- (void)chargerPlusDeNews {
    
    //    NSLog(@"[CommentsViewController] chargerPlusDeNews");
    
    [_chargementView setHidden:NO];
    
    if ([_commentaires count] != 0) {
        NSDictionary *lastCommentaire = [_commentaires objectAtIndex:[_commentaires count]-1];
        [[WPAPIRequest shared] loadMoreCommentairesForArticleID:_articleID andDate:[lastCommentaire objectForKey:@"date"] withDelegate:self];
    }
}

- (void)addMoreCommentaires:(NSArray*)moreCommentaires {
    
    //    NSLog(@"[CommentsViewController] addMoreCommentaires");
    
    if (moreCommentaires == nil) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur de chargement" message:@"Un problème est survenu lors du chargement des commentaires. Vérifiez votre connexion internet et essayez de recharger la vue." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        [self stopLoadingViewWithFooter:YES];
        return;
    }
    
    if ([moreCommentaires count] == 0) {
        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Il n'y a plus de commentaires à charger pour cet article !" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//        [alert show];
        [self stopLoadingViewWithFooter:NO];
        
        return;
    }
    
    NSIndexPath *path = [NSIndexPath indexPathForRow:[_commentaires count]-1 inSection:0];
    
    NSMutableArray *mutArray = [[NSMutableArray alloc] initWithArray:_commentaires];
    [mutArray addObjectsFromArray:moreCommentaires];
    _commentaires = [NSArray arrayWithArray:(NSArray *)mutArray];
    
    [_aTableView reloadData];
    [_aTableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    if ([moreCommentaires count] < 20) {
        [self stopLoadingViewWithFooter:NO];
    } else {
        [self stopLoadingViewWithFooter:YES];
    }
}

#pragma mark - Actions

- (void) backToList {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) addComment {
    
    WPAddCommentViewController *addCommentController = [[WPAddCommentViewController alloc] initWithArticleTitre:_titre andLink:_link andArticleID:_articleID andTracker:_tracker withDelegate:self];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:addCommentController];
    [navController.navigationBar setTranslucent:NO];
    [self presentViewController:navController animated:YES completion:nil];
}

#pragma - AddComment Delegate

- (void)refreshCommentView {
    [self startLoadingView];
}

#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *aCommentaire = [_commentaires objectAtIndex:indexPath.row];
    NSString *comment = [aCommentaire objectForKey:@"content"];
    
    // Texte
    NSArray *components = [comment componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    
    NSMutableArray *componentsToKeep = [NSMutableArray array];
    for (int i = 0; i < [components count]; i = i + 2) {
        [componentsToKeep addObject:[components objectAtIndex:i]];
    }
    
    NSString *plainText = [componentsToKeep componentsJoinedByString:@""];
    plainText = [plainText stringByDecodingHTMLEntities];
    
    //    NSLog(@"[CommentsViewController] PlainText : %@", plainText);
    
    UILabel *test = [[UILabel alloc] init];
    [test setLineBreakMode:NSLineBreakByWordWrapping];
    [test setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*14]];
    
    CGSize maximumLabelSize = CGSizeMake(self.view.frame.size.width-20,9999);
    CGSize expectedLabelSize = [plainText sizeWithFont:test.font constrainedToSize:maximumLabelSize lineBreakMode:test.lineBreakMode];
    
    float heigh_cell = (int)expectedLabelSize.height + 45.0-15;
    if (expectedLabelSize.height == 0) {
        return 0;
    }
    return heigh_cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [(NSArray*)_commentaires count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *aCommentaire = [_commentaires objectAtIndex:indexPath.row];
//    NSLog(@"[CommentsViewController] Commentaire : %@", aCommentaire);
    NSString *comment = [aCommentaire objectForKey:@"content"];
    NSString *date = [aCommentaire objectForKey:@"date"];
    
    // Pseudo
    NSDictionary *auteur = [aCommentaire objectForKey:@"author"];
    NSString *pseudo = [auteur objectForKey:@"name"];
    
    // Date
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    NSDate *theDate = [df dateFromString:date];
    NSString *articleDate = [self dateFormatter:theDate];
    
    static NSString *CellIdentifier = @"CommentCell";
    WPCommentCell *cell = (WPCommentCell*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Texte
    NSArray *components = [comment componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    
    NSMutableArray *componentsToKeep = [NSMutableArray array];
    for (int i = 0; i < [components count]; i = i + 2) {
        [componentsToKeep addObject:[components objectAtIndex:i]];
    }
    
    NSString *plainText = [componentsToKeep componentsJoinedByString:@""];
    plainText = [plainText stringByDecodingHTMLEntities];
    
    UILabel *test = [[UILabel alloc] init];
    [test setLineBreakMode:NSLineBreakByWordWrapping];
    [test setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*14]];
    
    CGSize maximumLabelSize = CGSizeMake(self.view.frame.size.width-20,9999);
    CGSize expectedLabelSize = [plainText sizeWithFont:test.font constrainedToSize:maximumLabelSize lineBreakMode:test.lineBreakMode];
    
    //    float heigh_cell = (int)expectedLabelSize.height + 40;
    
    //    NSLog(@"[CommentsViewController] Height cell : %f", heigh_cell);
    
    //    if (cell == nil)
    //    {
    cell = [[WPCommentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier andHeight:expectedLabelSize.height andWidth:self.view.frame.size.width];
    //    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell setPseudo:pseudo andDate:articleDate andComment:plainText];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)viewDidLayoutSubviews {
    [_aTableView setSeparatorInset:UIEdgeInsetsZero];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(WPCommentCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]){
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero]; // ios 8 newly added
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - APIDelegate

- (void)finishedLoadCommentairesWithStatut:(RequestStatut)aStatut andJSONArray:(NSArray *)aJSON {
    
    if (aStatut == RequestStatutSucceeded) {
        [self reloadViewWithCommentaires:aJSON];
        
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur de chargement" message:@"Un problème est survenu lors du chargement des commentaires. Vérifiez votre connexion internet et essayez de recharger la vue." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        [self stopLoadingViewWithFooter:YES];
    }
}

- (void)finishedLoadMoreCommentairesWithStatut:(RequestStatut)aStatut andJSONArray:(NSArray *)aJSON {
    
    //    NSLog(@"[CommentsViewController] finishedLoadMoreCommentairesWithStatut");
    
    if (aStatut == RequestStatutSucceeded) {
        [self addMoreCommentaires:aJSON];
        
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur de chargement" message:@"Un problème est survenu lors du chargement des commentaires. Vérifiez votre connexion internet et essayez de recharger la vue." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        [self stopLoadingViewWithFooter:YES];
    }
}

#pragma mark - Formatter

- (NSString*)dateFormatter:(NSDate *)dateEntree {
    
    NSTimeInterval interval = [dateEntree timeIntervalSinceNow];
    NSUInteger secondes = fabs(interval);
    NSUInteger minutes = secondes/60;
    NSUInteger heures = minutes/60;
    NSUInteger jours = heures/24;
    
    NSString *dateSortie;
    
    if (secondes < 60) {
        dateSortie = @"Maintenant";
    } else {
        if (minutes < 60) {
            if (minutes == 1) {
                dateSortie = @"Il y a 1 minute";
            } else {
                dateSortie = [NSString stringWithFormat:@"Il y a %lu minutes", (unsigned long)minutes];
            }
        } else {
            if (heures < 24) {
                if (heures == 1) {
                    dateSortie = @"Il y a 1 heure";
                } else {
                    dateSortie = [NSString stringWithFormat:@"Il y a %lu heures", (unsigned long)heures];
                }
            } else {
                if (jours <= 7) {
                    if (jours == 1) {
                        dateSortie = @"Il y a 1 jour";
                    } else {
                        dateSortie = [NSString stringWithFormat:@"Il y a %lu jours", (unsigned long)jours];
                    }
                } else {
                    // Le tweet a été posté il y a plus de 7 jours, affiché la date
                    NSDateFormatter *dateFormatTweet = [[NSDateFormatter alloc] init];
                    [dateFormatTweet setDateFormat:@"'Le' d MMMM yyyy"];
                    NSString *jourDuTweet = [dateFormatTweet stringFromDate:dateEntree];
                    dateSortie = jourDuTweet;
                }
            }
        }
    }
    
    return dateSortie;
}

@end