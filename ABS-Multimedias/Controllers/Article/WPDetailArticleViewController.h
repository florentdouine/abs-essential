//
//  WPDetailArticleViewController.h
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WPAPIRequest.h"
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import <AVFoundation/AVFoundation.h>
#if kisInterstitielArticles
#import <GoogleMobileAds/GADInterstitial.h>
#endif
#import <MediaPlayer/MediaPlayer.h>
#import <MediaAccessibility/MediaAccessibility.h>
#import "TTDownloadManager.h"

@protocol WPDetailArticleViewControllerDelegate <NSObject>
- (void) backToArticlesListWithType:(ArticleType)type;
- (void) openSuivantArticle:(TTPost*)item andType:(ArticleType)type;
- (void) openPrecedentArticle:(TTPost*)item andType:(ArticleType)type;
@optional
- (void) backToArticlesListWithType:(ArticleType)type andReload:(BOOL)isNeededReload;
- (void) goToSubWebsite01Category;
- (void) goToSubWebsite01CategoryWithArticleID:(int)articleID;
- (void) goToSubWebsite01CategoryFromTabBarWithArticleID:(int)articleID;
- (void) reloadArticlesWithType:(ArticlesType)aType;
- (void) reloadArticles;
- (void) openBibliothequeWithType:(BiblioType)type;
@end

@interface WPDetailArticleViewController : UIViewController <WPAPIRequestDelegate, UIWebViewDelegate, UIScrollViewDelegate, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate, UIGestureRecognizerDelegate, UIActionSheetDelegate, UIAlertViewDelegate, TTDownloadManagerDelegate
#if kisInterstitielArticles
,GADInterstitialDelegate
#endif
> {
    
    UIScrollView *_scrollView;
    UIWebView *_webView;
    ArticleType _type;
    BOOL _isArticleView;
    BOOL _isArticleUrl;
    BOOL isOpenView;
    
    // NavBar
    UIBarButtonItem *commentButton;
    UIBarButtonItem *favorisButton;
    UIBarButtonItem *shareButton;
    
    // Favoris
    BOOL _isFavoris;
    
    // Header
    UIImageView *_image;
    UIView *_header;
    UILabel *_titre;
    UILabel *_subtitle;
    
    // SubHeader
    UIView *_subHeader;
    
    CGFloat yWebView;
    
    // ToolBar
    UIToolbar *_toolbar;
    BOOL _isToolbar;
    
    SLComposeViewController *mySLComposerSheet;
    
    /* AUDIO */
    NSURL *_audioFileUrl;
    BOOL _isAudioFile;
    UIView *_audioPlayerView;
    UIButton *_playButton;
    UIButton *_playButton2;
    BOOL _isPlaying;
    
    TTChargementView *_chargementView;
    UIWebView *_browserWebView;
    
    int livreAudioID;
    BOOL videoLoaded;
    
    /* PUBLICITÉ */
#if kisInterstitielArticles
    GADInterstitial         *interstitial_;
#endif
}

@property (strong, nonatomic) id<WPDetailArticleViewControllerDelegate>delegate;
@property (strong, nonatomic) TTPost *item;
@property (assign, nonatomic) BOOL isArticlesNeededReload;

@property (strong, nonatomic) AVAudioPlayer* player;
@property (strong, nonatomic) MPMoviePlayerViewController *moviePlayerViewController;
@property (strong, nonatomic) MPMoviePlayerViewController *interneMoviePlayerViewController;
@property (nonatomic, strong) TTLivreAudio *currentListening;

// Buttons
@property (strong, nonatomic) UIButton *commentBtn;
@property (strong, nonatomic) UIButton *rechargerCustomButton;
@property (strong, nonatomic) UIButton *favorisCustomButton;

// Scroll direction
@property (nonatomic, assign) NSInteger lastContentOffset;

/* AUDIO */
@property (nonatomic, strong) AVAudioPlayer *soundPlayer;

- (id)initWithArticle:(TTPost *)item andDelegate:(id)delegate andType:(ArticleType)type;
- (id)initWithArticleID:(int)articleID andDelegate:(id)delegate andType:(ArticleType)type;
- (void) reloadViewWithArticleID:(int)articleID;
- (void) reloadViewWithArticle:(TTPost *)item;

@end
