//
//  WPDetailArticleViewController.m
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import "WPDetailArticleViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "WPBrowserViewController.h"
#import "WPAppDelegate.h"
#import "WPCommentsViewController.h"
#import <TTRequest/Reachability.h>
#import <Realm/Realm.h>
#import "UIImageView+WebCache.h"

@implementation WPDetailArticleViewController
@synthesize delegate = _delegate;
@synthesize item = _item;
@synthesize player = _player;
@synthesize favorisCustomButton;
@synthesize rechargerCustomButton;
@synthesize commentBtn = _commentBtn;
@synthesize moviePlayerViewController = _moviePlayerViewController;
@synthesize interneMoviePlayerViewController = _interneMoviePlayerViewController;
@synthesize currentListening = _currentListening;
@synthesize isArticlesNeededReload = _isArticlesNeededReload;
@synthesize soundPlayer = _soundPlayer;

- (id)initWithArticleID:(int)articleID andDelegate:(id)delegate andType:(ArticleType)type
{
    self = [super init];
    if (self) {
        
        _delegate = delegate;
        _item = [TTPost new];
        [_item setPostID:articleID];
        [_item setIsCompletePost:NO];
        _isArticlesNeededReload = NO;
        
        [self initTheView];
    }
    return self;
}

- (id)initWithArticle:(TTPost *)item andDelegate:(id)delegate andType:(ArticleType)type
{
    self = [super init];
    if (self) {
        
        _delegate = delegate;
        _item = item;
        [_item setIsCompletePost:YES];
        _type = type;
        _isArticlesNeededReload = NO;
        
        [self initTheView];
    }
    return self;
}

- (void) initTheView {
    
    // Publicité AdMob
#if kisInterstitielArticles
    if ([[WPPlistRecup shared] isInterstitialWithFrequence:kFreqInterstitielArticles] && !UIAccessibilityIsVoiceOverRunning()) {
        interstitial_ = [[GADInterstitial alloc] init];
        interstitial_.adUnitID = kAdInterstitielArticles;
        [interstitial_ setDelegate:self];
        [interstitial_ loadRequest:[GADRequest request]];
    }
#endif
    
    _isFavoris = NO;
    _isArticleView = YES;
    _isArticleUrl = YES;
    
    if (_type != ArticleSubWebsite) {
        // Marquer l'article comme "lu"
        [[WPPlistRecup shared] setIsReadForArticleID:_item.postID];
    }
    
    [self.view setBackgroundColor:kColorBackground];
    
    UIButton *fermerCustomButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    if (_type == ArticleStandalone) {
        [fermerCustomButton setBackgroundImage:[UIImage imageNamed:@"button_fermer_off.png"] forState:UIControlStateNormal];
        [fermerCustomButton setBackgroundImage:[UIImage imageNamed:@"button_fermer_on.png"] forState:UIControlStateHighlighted];
    } else {
        [fermerCustomButton setBackgroundImage:[UIImage imageNamed:@"button_retour_off.png"] forState:UIControlStateNormal];
        [fermerCustomButton setBackgroundImage:[UIImage imageNamed:@"button_retour_on.png"] forState:UIControlStateHighlighted];
    } 
    [fermerCustomButton addTarget:self action:@selector(backToList) forControlEvents:UIControlEventTouchUpInside];
    [fermerCustomButton setAccessibilityLabel:@"Retour"];
    UIBarButtonItem *fermerButton = [[UIBarButtonItem alloc] initWithCustomView:fermerCustomButton];
    
    rechargerCustomButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 150.0, 32.0)];
    [rechargerCustomButton setBackgroundColor:[UIColor clearColor]];
    [rechargerCustomButton setTitle:@"Revenir sur l'article" forState:UIControlStateNormal];
    [rechargerCustomButton setTitleColor:kColorTextLight forState:UIControlStateNormal];
    [[rechargerCustomButton titleLabel] setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*14.0]];
    [rechargerCustomButton addTarget:self action:@selector(rechargerWebView) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rechargerButton = [[UIBarButtonItem alloc] initWithCustomView:rechargerCustomButton];
    [self.navigationItem setLeftBarButtonItem:rechargerButton];
    [rechargerCustomButton setHidden:YES];
    
    NSArray *buttonsLeft = [NSArray arrayWithObjects:fermerButton, rechargerButton, nil];
    [self.navigationItem setLeftBarButtonItems:buttonsLeft];
        
    _commentBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    [_commentBtn setBackgroundImage:[UIImage imageNamed:@"button_comment_off.png"] forState:UIControlStateNormal];
    [_commentBtn setBackgroundImage:[UIImage imageNamed:@"button_comment_on.png"] forState:UIControlStateHighlighted];
    [_commentBtn setTitle:@"0" forState:UIControlStateNormal];
    [_commentBtn setAccessibilityLabel:@"0 commentaire"];
    [[_commentBtn titleLabel] setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*12.0]];
    [[_commentBtn titleLabel] setTextAlignment:NSTextAlignmentCenter];
    [_commentBtn setTitleColor:kColorTextBulle forState:UIControlStateNormal];
    [_commentBtn addTarget:self action:@selector(goToComments) forControlEvents:UIControlEventTouchUpInside];
    commentButton = [[UIBarButtonItem alloc] initWithCustomView:_commentBtn];
    
    favorisCustomButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    [favorisCustomButton setBackgroundImage:[UIImage imageNamed:@"button_favoris_off.png"] forState:UIControlStateNormal];
    [favorisCustomButton setBackgroundImage:[UIImage imageNamed:@"button_favoris_on.png"] forState:UIControlStateHighlighted];
    [favorisCustomButton setBackgroundImage:[UIImage imageNamed:@"button_favoris_selected.png"] forState:UIControlStateSelected];
    [favorisCustomButton addTarget:self action:@selector(mettreEnFavoris) forControlEvents:UIControlEventTouchUpInside];
    favorisButton = [[UIBarButtonItem alloc] initWithCustomView:favorisCustomButton];
    
    UIButton *shareCustomButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    [shareCustomButton setBackgroundImage:[UIImage imageNamed:@"button_partager_off.png"] forState:UIControlStateNormal];
    [shareCustomButton setBackgroundImage:[UIImage imageNamed:@"button_partager_on.png"] forState:UIControlStateHighlighted];
    [shareCustomButton addTarget:self action:@selector(shareArticle) forControlEvents:UIControlEventTouchUpInside];
    [shareCustomButton setAccessibilityLabel:@"Partager"];
    shareButton = [[UIBarButtonItem alloc] initWithCustomView:shareCustomButton];
    
    if (_type == ArticleSubWebsite || _type == ArticleSubWebsiteInfos) {
        [self.navigationItem setRightBarButtonItem:nil];
    } else {
#warning La vue commentaire est temporairement désactivée.
        NSArray *buttonsRight = [NSArray arrayWithObjects:shareButton, favorisButton/*, commentButton*/, nil];
        [self.navigationItem setRightBarButtonItems:buttonsRight];
    }
    
    //
    
    // ScrollView
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 44.0 - 40)];
    [_scrollView setShowsVerticalScrollIndicator:NO];
    [_scrollView setDelegate:self];
    [_scrollView setBackgroundColor:[UIColor clearColor]];
    [_scrollView setDelaysContentTouches:NO];
    [self.view addSubview:_scrollView];
    
    CGFloat y = 0;
    
    // Header
    _header = [[UIView alloc] initWithFrame:CGRectMake(0, y, self.view.frame.size.width, 0)];
    [_header setBackgroundColor:[UIColor clearColor]];
    [_scrollView addSubview:_header];
    
    CGFloat yHeader = 0;
    
    if (![_item.URLFeatureImg isEqualToString:@""] && _item.URLFeatureImg != nil) {
        
        _image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 150)];
        [_image setClipsToBounds:YES];
        [_image setBackgroundColor:[UIColor whiteColor]];
        [_image setContentMode:UIViewContentModeScaleAspectFill];
        
        [_image sd_setImageWithURL:[NSURL URLWithString:_item.URLFeatureImg] placeholderImage:[UIImage imageNamed:@"placeholder.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        }];
        [_header addSubview:_image];
        yHeader += _image.frame.size.height;
    }
    yHeader += 5;
    
    _titre = [[UILabel alloc] initWithFrame:CGRectMake(10.0, yHeader, _header.frame.size.width - 20.0, 50*kGetFontSize)];
    [_titre setFont:[UIFont fontWithName:kFontNameBold size:kGetFontSize*18.0]];
    [_titre setTextColor:[UIColor blackColor]];
    [_titre setNumberOfLines:3];
    [_titre setBaselineAdjustment:UIBaselineAdjustmentAlignCenters];
    [_titre setTextAlignment:NSTextAlignmentLeft];
    [_titre setBackgroundColor:[UIColor clearColor]];
    [_titre setAdjustsFontSizeToFitWidth:YES];
    [_header addSubview:_titre];
    yHeader += _titre.frame.size.height+5;
    
    [_header setFrame:CGRectMake(0, y, self.view.frame.size.width, yHeader)];
    y += _header.frame.size.height;
    
    //
    // Footer
    
    if (_item.type != APIAnnuaire) {
        
        _subHeader = [[UIView alloc] initWithFrame:CGRectMake(0, y, self.view.frame.size.width, kGetFontSize*40)];
        [_subHeader setBackgroundColor:[UIColor clearColor]];
        [_subHeader setHidden:YES];
        [_scrollView addSubview:_subHeader];
        
        _subtitle = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0, _subHeader.frame.size.width - 20.0, _subHeader.frame.size.height)];
        [_subtitle setFont:[UIFont fontWithName:kFontNameLight size:kGetFontSize*14.0]];
        [_subtitle setTextColor:[UIColor blackColor]];
        [_subtitle setBaselineAdjustment:UIBaselineAdjustmentAlignCenters];
        [_subtitle setTextAlignment:NSTextAlignmentLeft];
        [_subtitle setBackgroundColor:[UIColor clearColor]];
        [_subtitle setNumberOfLines:2];
        [_subHeader addSubview:_subtitle];
        
        y += _subHeader.frame.size.height;
    }
    y += 5;
    
    // Gesture
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideOrRevealToolBar)];
    [tap setNumberOfTapsRequired:2];
    [tap setNumberOfTouchesRequired:1];
    [tap setCancelsTouchesInView:YES];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
    [_scrollView addGestureRecognizer:tap];
    [_header addGestureRecognizer:tap];
    [_subHeader addGestureRecognizer:tap];
    
    //
    
    UIView *border = [[UIView alloc] initWithFrame:CGRectMake(0, y, self.view.frame.size.width, 1)];
    [border setBackgroundColor:kColorNextArticles];
    [_scrollView addSubview:border];
    
    y += border.frame.size.height;
    
    //
    // Player audio
    
    _audioPlayerView = [[UIView alloc] initWithFrame:CGRectMake(0, y, self.view.frame.size.width, 40)];
    [_audioPlayerView setBackgroundColor:kColorNextArticles];
    
    _playButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, _audioPlayerView.frame.size.width, _audioPlayerView.frame.size.height)];
    if (_type == ArticleSubWebsite) {
        [_playButton setTitle:@"Écouter le livre audio" forState:UIControlStateNormal];
    } else {
        [_playButton setTitle:@"Écouter le podcast" forState:UIControlStateNormal];
    }
    [_playButton setTitleColor:kColorTextLight forState:UIControlStateNormal];
    [_playButton setTitleColor:kColorText forState:UIControlStateHighlighted];
    [_playButton setContentMode:UIViewContentModeCenter];
    [[_playButton titleLabel] setFont:[UIFont fontWithName:kFontNameBold size:kGetFontSize*16.0]];
    [_playButton addTarget:self action:@selector(lireFichierAudio) forControlEvents:UIControlEventTouchUpInside];
    [_audioPlayerView addSubview:_playButton];
    
    [_audioPlayerView setHidden:YES];
    [_scrollView addSubview:_audioPlayerView];
    
    //
    
    if (_isAudioFile) {
        y += _audioPlayerView.frame.size.height;
    }
    yWebView = y;
    
    // WebView
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, yWebView, self.view.frame.size.width, 10.0f)];
    [_webView setBackgroundColor:[UIColor clearColor]];
    [_webView setDelegate:self];
    [_webView setUserInteractionEnabled:YES];
    [_webView.scrollView setMinimumZoomScale:0.1];
    [_webView.scrollView setMaximumZoomScale:5.0];
    [_webView.scrollView setScrollEnabled:NO];
    [_scrollView addSubview:_webView];
    
    // Retirer l'ombre de la webview
    id scrollview = [_webView.subviews objectAtIndex:0];
    for (UIView *subview in [scrollview subviews])
        if ([subview isKindOfClass:[UIImageView class]])
            subview.hidden = YES;
    //
    
    [_webView addGestureRecognizer:tap];
    
    //
    
    _playButton2 = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    NSString *telechargerTexte = @"";
    NSString *ecouterTexte = @"";
    if (_type == ArticleSubWebsite) {
        telechargerTexte = @"Télécharger le livre audio";
        ecouterTexte = @"Écouter le livre audio";
    } else {
        telechargerTexte = @"Télécharger le podcast";
        ecouterTexte = @"Écouter le podcast";
    }
    if (_item.type == APIAnnuaire) {
        [_playButton2 setTitle:@"Voir sur le site de l’éditeur" forState:UIControlStateNormal];
        [_playButton2 addTarget:self action:@selector(openEditorWebSite) forControlEvents:UIControlEventTouchUpInside];
    } else {
        RLMResults *livresAudio = [TTLivreAudio objectsWhere:[NSString stringWithFormat:@"articleID == %i", _item.postID]];
        if (livresAudio.count > 0) {
            TTLivreAudio *livreAudio = (TTLivreAudio*)[livresAudio firstObject];
            if (livreAudio.statut != LivreAudioDownloading) {
                [_playButton2 setTitle:ecouterTexte forState:UIControlStateNormal];
                [_playButton2 addTarget:self action:@selector(lireFichierAudio) forControlEvents:UIControlEventTouchUpInside];
            } else {
                [_playButton2 setTitle:telechargerTexte forState:UIControlStateNormal];
                [_playButton2 addTarget:self action:@selector(telechargerFichierAudio) forControlEvents:UIControlEventTouchUpInside];
            }
        } else {
            [_playButton2 setTitle:telechargerTexte forState:UIControlStateNormal];
            [_playButton2 addTarget:self action:@selector(telechargerFichierAudio) forControlEvents:UIControlEventTouchUpInside];
        }
    }

    [_playButton2 setTitleColor:kColorTextLight forState:UIControlStateNormal];
    [_playButton2 setTitleColor:kColorText forState:UIControlStateHighlighted];
    [_playButton2 setContentMode:UIViewContentModeCenter];
    [_playButton2 setBackgroundColor:kColorNextArticles];
    [[_playButton2 titleLabel] setFont:[UIFont fontWithName:kFontNameBold size:kGetFontSize*16.0]];
    [_scrollView addSubview:_playButton2];
    [_playButton2 setHidden:YES];
    
    //
    
    _toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, MyAppDelegate.window.frame.size.height, self.view.frame.size.width, 40)];
    if ([_toolbar respondsToSelector:@selector(setBarTintColor:)]) {
        [_toolbar setBarTintColor:kColorNavBar2];
        [_toolbar setTintColor:kColorTextNavBar2];
        [_toolbar setTranslucent:NO];
    } else {
        // iOS 6
        [_toolbar setTintColor:kColorNavBar2];
    }
    if (_type != ArticleSubWebsiteInfos && _type != ArticleFavoris) {
        [[self view] addSubview:_toolbar];
        _isToolbar = YES;
    } else {
        _isToolbar = NO;
    }
    
    UIButton *leftArticleCustomButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    [leftArticleCustomButton setBackgroundImage:[UIImage imageNamed:@"button_precedent_off.png"] forState:UIControlStateNormal];
    [leftArticleCustomButton setBackgroundImage:[UIImage imageNamed:@"button_precedent_on.png"] forState:UIControlStateHighlighted];
    [leftArticleCustomButton setBackgroundImage:[UIImage imageNamed:@"button_precedent_on.png"] forState:UIControlStateSelected];
    [leftArticleCustomButton addTarget:self action:@selector(goToPrecedentArticle) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftArticleButton = [[UIBarButtonItem alloc] initWithCustomView:leftArticleCustomButton];
    
    UIButton *rightArticleCustomButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    [rightArticleCustomButton setBackgroundImage:[UIImage imageNamed:@"button_suivant_off.png"] forState:UIControlStateNormal];
    [rightArticleCustomButton setBackgroundImage:[UIImage imageNamed:@"button_suivant_on.png"] forState:UIControlStateHighlighted];
    [rightArticleCustomButton setBackgroundImage:[UIImage imageNamed:@"button_suivant_on.png"] forState:UIControlStateSelected];
    [rightArticleCustomButton addTarget:self action:@selector(goToSuivantArticle) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightArticleButton = [[UIBarButtonItem alloc] initWithCustomView:rightArticleCustomButton];
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    [_toolbar setItems:[NSArray arrayWithObjects:leftArticleButton, flexibleSpace, rightArticleButton, nil]];
    
    // Load
    _chargementView = [[TTChargementView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];;
    [[self view] addSubview:_chargementView];
    
    [_chargementView setHidden:YES];
    [self enabledBarButtonItem:YES];
    
    if (!_item.isCompletePost) {
        [self startLoadingView];
    } else {
        [self reloadTheView:_item];
    }
    _isPlaying = NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [[TTDownloadManager shared] setDelegate:self];
    
    // Google Analytics
    id<GAITracker> googleTracker = [[GAI sharedInstance] trackerWithTrackingId:kTrackingID];
    [googleTracker set:kGAIScreenName value:[NSString stringWithFormat:@"Article : %@", _item.titre]];
    [googleTracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    if (_player.isPlaying) {
        [_player pause];
        if (_type == ArticleSubWebsite) {
            [_playButton setTitle:@"Reprendre le livre audio" forState:UIControlStateNormal];
        } else {
            [_playButton setTitle:@"Reprendre le podcast" forState:UIControlStateNormal];
        }
    }
    
    if (_moviePlayerViewController != nil && !_isPlaying) {
        [_moviePlayerViewController dismissMoviePlayerViewControllerAnimated];
        [_moviePlayerViewController.moviePlayer stop];
        [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
        [[AVAudioSession sharedInstance] setActive:NO error:nil];
    }
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (kWantReloadOnAppear && !isOpenView) {
        if ([_delegate respondsToSelector:@selector(reloadArticles)]) {
            [_delegate reloadArticles];
        }
    }
    isOpenView = NO;
}

- (void)dealloc {
    if (_browserWebView != nil) {
        [_browserWebView removeFromSuperview];
        _browserWebView = nil;
    }
    
    if (_moviePlayerViewController != nil) {
        [_moviePlayerViewController dismissMoviePlayerViewControllerAnimated];
        [_moviePlayerViewController.moviePlayer stop];
        [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
        [[AVAudioSession sharedInstance] setActive:NO error:nil];
    }
    
    // Vider les caches
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    DLog(@"%@", event);
    switch (event.subtype) {
        case UIEventSubtypeRemoteControlPlay:
            [_moviePlayerViewController.moviePlayer play];
            break;
        case UIEventSubtypeRemoteControlPause:
            [_moviePlayerViewController.moviePlayer pause];
            break;
        default:
            break;
    }
}

#pragma mark - Load

- (void)reloadViewWithArticleID:(int)articleID {
    
    _item = [TTPost new];
    [_item setIsCompletePost:NO];
    [_item setPostID:articleID];
    [self startLoadingView];
}

- (void)reloadViewWithArticle:(TTPost *)item {
    _item = item;
    [_item setIsCompletePost:YES];
    [_chargementView setHidden:NO];
    [self enabledBarButtonItem:NO];
    [self reloadTheView:_item];
}

- (void)startLoadingView {
    
    [_chargementView setHidden:NO];
    [self enabledBarButtonItem:NO];
    [[WPAPIRequest shared] loadOneArticleWithID:_item.postID andArticleType:_type withDelegate:self];
}

- (void) enabledBarButtonItem:(BOOL)isEnabled {
    for (UIBarButtonItem *item in self.navigationController.navigationItem.rightBarButtonItems) {
        [item setEnabled:isEnabled];
    }
}

- (void)reloadTheView:(TTPost*)item {
    
    if (item == nil) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur de chargement" message:@"Un problème est survenu lors du chargement de l'article. Vérifiez votre connexion internet et essayez de nouveau votre recherche." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        [_chargementView setHidden:YES];
        [self enabledBarButtonItem:YES];
        [self backToList];
        return;
    }
    
    _item = item;
//    NSLog(@"[DetailArticleViewController] Article : %@", _article);
    
    [_playButton2 setHidden:YES];
    
    [_webView setFrame:CGRectMake(0, yWebView, self.view.frame.size.width, 40)];
    [self rechargerWebView];
    _isArticleUrl = YES;
    
    [_titre setText:item.titre];
    
    if (_item.type == APIPost) {
        
        // Date
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"d MMMM yyyy"];
        [_subtitle setText:[NSString stringWithFormat:@"Publié par %@ le %@", _item.auteur, [df stringFromDate:_item.date]]];
        [_subtitle setAdjustsFontSizeToFitWidth:YES];
        
    } else {
        
        // Date
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"d MMMM yyyy"];
        NSString *articleDate = [df stringFromDate:item.date];
        
        NSString *subTitle = @"";
        if ([item.presentateurs isEqualToString:@""]) {
            subTitle = [NSString stringWithFormat:@"Par %@", item.auteur2];
            if (![item.narrateur isEqualToString:@""]) {
                subTitle = [NSString stringWithFormat:@"%@ avec %@", subTitle, item.narrateur];
            }
        } else {
            subTitle = [NSString stringWithFormat:@"Par %@", item.presentateurs];
            if (![item.invites isEqualToString:@""]) {
                subTitle = [NSString stringWithFormat:@"%@ avec %@", subTitle, item.invites];
            }
        }
        
        if (![item.codeISRC isKindOfClass:[NSNull class]] && item.codeISRC != nil && ![item.codeISRC isEqualToString:@""]) {
            [_subtitle setText:[NSString stringWithFormat:@"%@ le %@\nCode ISRC : %@", subTitle, articleDate, item.codeISRC]];
        } else {
            [_subtitle setText:[NSString stringWithFormat:@"%@ le %@", subTitle, articleDate]];
        }
        [_subtitle setAdjustsFontSizeToFitWidth:YES];
    }
    
    // Favoris
    _isFavoris = [[WPPlistRecup shared] isFavorisForID:_item.postID];
    [favorisCustomButton setSelected:_isFavoris];
    if (_isFavoris) {
        [favorisCustomButton setAccessibilityLabel:@"Retirer des favoris"];
    } else {
        [favorisCustomButton setAccessibilityLabel:@"Mettre en favoris"];
    }
    
    // Commentaires
    if (_type == ArticleSubWebsite || _type == ArticleSubWebsiteInfos) {
        [self.navigationItem setRightBarButtonItems:@[shareButton]];
    } else {
        if (!_item.isCommentsOpen && _item.nbComments == 0) {
            NSArray *buttonsRight = [NSArray arrayWithObjects:shareButton, favorisButton, nil];
            [self.navigationItem setRightBarButtonItems:buttonsRight];
        } else {
            NSArray *buttonsRight = [NSArray arrayWithObjects:shareButton, favorisButton/*, commentButton*/, nil];
            [self.navigationItem setRightBarButtonItems:buttonsRight];
        }
    }
    [_commentBtn setTitle:[NSString stringWithFormat:@"%i", _item.nbComments] forState:UIControlStateNormal];
    [_commentBtn setAccessibilityLabel:[NSString stringWithFormat:@"%i commentaires", _item.nbComments]];
    
    [_chargementView setHidden:YES];
    [self enabledBarButtonItem:YES];
}

- (void) rechargerWebView {
    
    // Article
    NSString *content = _item.content;
    if (_item.type == APIPost) {
        //    NSLog(@"[WPDetailArticleViewController] Content : %@", content);
        
        
        _audioFileUrl = [self getUrlInContent:content forBeggining:@"http://medias-abs.abs-multimedias.com/" andEnd:@".mp3"];
        if (_audioFileUrl != nil) {
            _isAudioFile = YES;
        } else {
            _audioFileUrl = [self getUrlInContent:content forBeggining:@"http://abs-podcast.abs-multimedias.com/" andEnd:@".mp3"];
            if (_audioFileUrl != nil) {
                _isAudioFile = YES;
            } else {
                _audioFileUrl = [self getUrlInContent:content forBeggining:@"http://media.blubrry.com/abs/p/abs-podcast.abs-multimedias.com/" andEnd:@".mp3"];
                if (_audioFileUrl != nil) {
                    _isAudioFile = YES;
                } else {
                    _audioFileUrl = [self getUrlInContent:content forBeggining:@"http://app-audiobook.abs-multimedias.com/medias/" andEnd:@".m4b"];
                    if (_audioFileUrl != nil) {
                        _isAudioFile = YES;
                    } else {
                        _audioFileUrl = [self getUrlInContent:content forBeggining:@"http://app-audio.abs-multimedias.com/medias/" andEnd:@".m4b"];
                        if (_audioFileUrl != nil) {
                            _isAudioFile = YES;
                        } else {
                            _audioFileUrl = [self getUrlInContent:content forBeggining:@"http://media.blubrry.com/abs/p/medias-abs.abs-multimedias.com/" andEnd:@".mp3"];
                            if (_audioFileUrl != nil) {
                                _isAudioFile = YES;
                            } else {
                                _audioFileUrl = [self getUrlInContent:content forBeggining:@"http://abs-podcast.abs-multimedias.com/" andEnd:@".m4b"];
                                if (_audioFileUrl != nil) {
                                    _isAudioFile = YES;
                                } else {
                                    _audioFileUrl = [self getUrlInContent:content forBeggining:@"http://app-audiobook.abs-multimedias.com/medias/" andEnd:@".mp3"];
                                    if (_audioFileUrl != nil) {
                                        _isAudioFile = YES;
                                    } else {
                                        _isAudioFile = NO;
                                    }
                                }
                            } 
                        }
                    }
                }
            }
        }
    } else {
        _audioFileUrl = [NSURL URLWithString:_item.URLMp3File];
        if (_audioFileUrl != nil) {
            _isAudioFile = YES;
        }
    }
    
    /** **/
    
    // WebView
    [_webView setHidden:YES];
    NSURL *myBaseURL = [[NSURL alloc] initFileURLWithPath:[[NSBundle mainBundle] bundlePath]];
    NSString* htmlContentString = [NSString stringWithFormat:
                                   @"<html>"
                                   "<style type=\"text/css\">"
                                   "body {font-family:%@; font-size:%f; max-width:%fpx; height: auto; width: auto;}"
                                   "img {max-width:%fpx; height: auto; width: auto; align:center}"
                                   "a {color:#%@}"
                                   "iframe {max-width:%fpx; height: auto; width: auto; align:center}"
                                   "</style>"
                                   "<body>"
                                   "<p>%@</p>"
                                   "</body></html>", kFontNameRegular, (14*kGetFontSize), self.view.frame.size.width-20, self.view.frame.size.width-70, kColorTextLink, self.view.frame.size.width-20, content];
    [_webView loadHTMLString:htmlContentString baseURL:myBaseURL];
    _isArticleView = YES;
}

#pragma mark - WebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if (webView.tag == 1) { // WebView de l'audio
        [_chargementView setHidden:YES];
        [self enabledBarButtonItem:YES];
        return;
    }
    
    if (webView.isLoading)
        return;
    
    if (_isArticleView) {
        [rechargerCustomButton setHidden:YES];
        _isArticleView = NO;
    } else {
        [rechargerCustomButton setHidden:NO];
    }
    
    CGFloat height = [[_webView stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight"] floatValue]+30;
    //CGFloat width = [[_webView stringByEvaluatingJavaScriptFromString:@"document.width"] floatValue];
    CGRect frame = _webView.frame;
    frame.size.height = height;
    frame.size.width = self.view.frame.size.width;
    _webView.frame = frame;
    
    // Header height
    int output = (int)frame.size.height;
    
    float y = yWebView;
    if (_isAudioFile) {
        y += _playButton.frame.size.height;
    }
    
    //    NSLog(@"[DetailArticleViewController] Ouput : %i", output);
    [_webView setFrame:CGRectMake(0, y, _webView.frame.size.width, output)];

    output += y;
    
    // Audio
    if (_isAudioFile || _item.type == APIAnnuaire) {
        [_playButton2 setFrame:CGRectMake(0, output, self.view.frame.size.width, 40)];
        output += _playButton2.frame.size.height;
        [_playButton2 setHidden:NO];
    } else {
        [_playButton2 setHidden:YES];
    }
    
    [_webView setHidden:NO];
    [_scrollView setContentSize:CGSizeMake(_scrollView.frame.size.width, output)];
    [_scrollView setFrame:CGRectMake(0, 0, self.view.frame.size.width, _scrollView.frame.size.height)];
    [_scrollView scrollRectToVisible:CGRectMake(0, 0, _scrollView.frame.size.width, _scrollView.frame.size.height) animated:YES];
    
    // Audio
    if (_isAudioFile) {
        [_audioPlayerView setHidden:NO];
    }
    
    // Ajouter subHeader
//    [_subHeader setFrame:CGRectMake(0, _scrollView.contentSize.height - 40, self.view.frame.size.width, 40)];
    [_subHeader setHidden:NO];
    
    //
    [self hideOrRevealToolBar];
    
    _isArticleUrl = NO;
}

- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {
    
   NSURL *url = [request URL];
    
    NSString *string = request.URL.absoluteString;
    NSArray *strings = [string componentsSeparatedByString:@"https://www.abs-multimedias.com/livre-audio/"];
    if (strings.count > 1) {
        NSArray *app = [(NSString*)[strings objectAtIndex:1] componentsSeparatedByString:@"?app="];
        if (app.count > 1) {
            livreAudioID = [[app objectAtIndex:1] intValue];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Livre Audio" message:@"Voulez-vous vous rendre dans la catégorie Librairie pour écouter ce fichier ?" delegate:self cancelButtonTitle:@"Plus tard" otherButtonTitles:@"Continuer", nil];
            [alert setTag:1];
            [alert show];
            return NO;
        }
    }
    
    if (webView.tag == 1) { // WebView de l'audio
        return YES;
    }
    
    if ([request.URL.absoluteString isEqualToString:_audioFileUrl.absoluteString]) { // Ne pas ouvrir le browser
        [self lireFichierAudio];
        return NO;
    }
    
    if (_isArticleUrl) {
        return YES;
    }
    
    if ([[[request URL] absoluteString] isEqualToString:@"about:blank"]) {
        return YES;
    }
    
    // Vérification début
    NSArray *arr = [[url absoluteString] componentsSeparatedByString:@"file://"];
    if (arr.count == 2) {
        return YES;
    }

    NSLog(@"[DetailArticleViewController] Url : %@", [url absoluteString]);
    
    WPBrowserViewController *browserController = [[WPBrowserViewController alloc] init];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:browserController];
    [navController.navigationBar setTranslucent:NO];
    [browserController setStringRequest:[url absoluteString]];
    [MyAppDelegate.window.rootViewController presentViewController:navController animated:YES completion:nil];
    
    return NO;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    if (webView.tag == 1) { // WebView de l'audio
        [_chargementView setHidden:YES];
        [self enabledBarButtonItem:YES];
        return;
    }
}

#pragma mark - UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {

    if (alertView.tag == 1 && buttonIndex == 1) {
        if ([_delegate respondsToSelector:@selector(goToSubWebsite01CategoryFromTabBarWithArticleID:)]) {
            [_delegate goToSubWebsite01CategoryFromTabBarWithArticleID:livreAudioID];
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    if (alertView.tag == 2 && buttonIndex == 1) {
        [self lireFichierAudioWithWifi];
    }
    
    if (alertView.tag == 3 && buttonIndex == 1) {
        [self openBibliotheque];
    }
    
    if (alertView.tag == 4 && buttonIndex == 1) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Téléchargement en cours" message:@"Le livre audio va être téléchargé dans votre bibliothèque. Voulez-vous vous rendre dans cet onglet ?" delegate:self cancelButtonTitle:@"Non merci" otherButtonTitles:@"S'y rendre", nil];
        [alert setTag:3];
        [alert show];
        [self startTelechargerFichierAudio];
    }
}

- (void) openBibliotheque {
    BiblioType type = BiblioPodcasts;
    if (_type == ArticleSubWebsite) {
        type = BiblioLivresAudio;
    }
    if ([_delegate respondsToSelector:@selector(openBibliothequeWithType:)]) {
        [_delegate openBibliothequeWithType:type];
    }
}

#pragma mark - Gesture

- (void) hideOrRevealToolBar {
    
    if (_type == ArticleStandalone || _type == ArticleFavoris) {
        _isToolbar = YES;
    }
    
    if (!_isToolbar) {
        [_scrollView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 40)];
        [UIView animateWithDuration:0.2 animations:^{
            [_toolbar setFrame:CGRectMake(0, _scrollView.frame.size.height, self.view.frame.size.width, 40)];
        }];
        
    } else {
        
        [_scrollView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        [UIView animateWithDuration:0.2 animations:^{
            [_toolbar setFrame:CGRectMake(0, _scrollView.frame.size.height, self.view.frame.size.width, 40)];
        }];
    }
    
    _isToolbar = !_isToolbar;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    CGPoint translation = [scrollView.panGestureRecognizer translationInView:scrollView];
    if (translation.y > 0) {
        //        NSLog(@"[DetailArticleViewController] Vers le haut");
        if (!_isToolbar) {
            [self hideOrRevealToolBar];
        }
    } else {
        //        NSLog(@"[DetailArticleViewController] Vers le bas");
        if (_isToolbar) {
            [self hideOrRevealToolBar];
        }
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

#pragma mark - APIDelegate

- (void)finishedLoadAnArticleWithStatut:(RequestStatut)aStatut andItem:(TTPost *)item {
    
    if (aStatut == RequestStatutSucceeded) {
        [self reloadTheView:item];
        
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur de chargement" message:@"Un problème est survenu lors du chargement de l'article. Vérifiez votre connexion internet et essayez de nouveau votre recherche." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        [_chargementView setHidden:YES];
        [self enabledBarButtonItem:YES];
        [self backToList];
        if (_type == ArticleFavoris) {
            [[WPPlistRecup shared] retirerUnFavorisForID:_item.postID];
        }
    }
}

#pragma mark - Actions

- (void) backToList {
    
    if ([_delegate respondsToSelector:@selector(backToArticlesListWithType:andReload:)]) {
        [_delegate backToArticlesListWithType:_type andReload:_isArticlesNeededReload];
    } else {
        [[self navigationController] popViewControllerAnimated:YES];
    }
    [self dismissViewControllerAnimated:YES completion:^{
        if (_type == ArticleStandalone) {
            if ([_delegate respondsToSelector:@selector(reloadArticlesWithType:)]) {
                [_delegate reloadArticlesWithType:ArticlesAll];
            }
        }
    }];
    
    if (_moviePlayerViewController != nil) {
        [MyAppDelegate.window.rootViewController dismissMoviePlayerViewControllerAnimated];
        [_moviePlayerViewController dismissMoviePlayerViewControllerAnimated];
        [_moviePlayerViewController.moviePlayer stop];
        _moviePlayerViewController = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL new]];
        [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
        [[AVAudioSession sharedInstance] setActive:NO error:nil];
    }
}

- (void) goToComments {
    
    if (_item == nil) {
        return;
    }
    
    isOpenView = YES;
    WPCommentsViewController *commentsController = [[WPCommentsViewController alloc] initWithArticleType:_type andArticleTitle:_titre.text andArticleLink:_item.URLArticle andArticleID:_item.postID];
    [self.navigationController pushViewController:commentsController animated:YES];
}

#pragma mark Navigation

- (void) goToPrecedentArticle {
    
    isOpenView = YES;
    if ([_delegate respondsToSelector:@selector(openPrecedentArticle:andType:)]) {
        [_delegate openPrecedentArticle:_item andType:_type];
    }
}

- (void) goToSuivantArticle {
    
    isOpenView = YES;
    if ([_delegate respondsToSelector:@selector(openSuivantArticle:andType:)]) {
        [_delegate openSuivantArticle:_item andType:_type];
    }
}

#pragma mark - Partager

- (void) shareArticle {
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Annuler" destructiveButtonTitle:nil otherButtonTitles:@"Ouvrir sur Safari", @"Partager sur Twitter", @"Partager sur Facebook", @"Envoyer par SMS", @"Envoyer par Mail", nil];
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0:
            [self goToSafari];
            break;
        case 1:
            [self shareOnTwitter];
            break;
        case 2:
            [self shareOnFacebook];
            break;
        case 3:
            [self partagerParMessage];
            break;
        case 4:
            [self partagerParMail];
            break;
            
        default:
            break;
    }
}

#pragma mark Safari

- (void) openEditorWebSite {
    
    WPBrowserViewController *browserController = [[WPBrowserViewController alloc] init];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:browserController];
    [navController.navigationBar setTranslucent:NO];
    [browserController setStringRequest:_item.URLEditor];
    [MyAppDelegate.window.rootViewController presentViewController:navController animated:YES completion:^{
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
    }];
}

- (void) goToSafari {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_item.URLArticle]];
}

#pragma mark - Facebook
#pragma mark Share

- (void) shareOnFacebook {
    
    NSString *title_article = _item.titre;
    
    NSString *message = [NSString stringWithFormat:@"%@ via %@", title_article, kSiteName];
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        mySLComposerSheet = [[SLComposeViewController alloc] init];
        mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [mySLComposerSheet setInitialText:message];
//        [mySLComposerSheet addImage:[UIImage imageNamed:@"icon_120x120.png"]];
        [mySLComposerSheet addURL:[NSURL URLWithString:_item.URLArticle]];
        [self presentViewController:mySLComposerSheet animated:YES completion:nil];
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook non disponible" message:@"Vous ne pouvez actuellement pas utiliser votre compte Facebook, assurez-vous que votre appareil dispose d'une connexion internet et que vous avez au moins une configuration de compte Facebook." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
        NSString *output;
        
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                output = nil;
                break;
            case SLComposeViewControllerResultDone:
                output = @"Merci d'avoir partagé ! :)";
                break;
            default:
                break;
        }
        
        if (output != nil) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook" message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
    }];
}

#pragma mark - Twitter
#pragma mark Share

- (void) shareOnTwitter {
    
    NSString *title_article = _item.titre;
    
    NSString *message = [NSString stringWithFormat:@"%@ via @%@", title_article, kTwitterName];
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        
        mySLComposerSheet = [[SLComposeViewController alloc] init];
        mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [mySLComposerSheet setInitialText:message];
        [mySLComposerSheet addURL:[NSURL URLWithString:_item.URLArticle]];
        [self presentViewController:mySLComposerSheet animated:YES completion:nil];
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter non disponible" message:@"Vous ne pouvez actuellement pas utiliser votre compte Twitter, assurez-vous que votre appareil dispose d'une connexion internet et que vous avez au moins une configuration de compte Twitter." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    [mySLComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
        NSString *output;
        
        switch (result) {
            case SLComposeViewControllerResultCancelled:
                output = nil;
                break;
            case SLComposeViewControllerResultDone:
                output = @"Merci d'avoir partagé ! :)";
                break;
            default:
                break;
        }
        
        if (output != nil) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter" message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alert show];
        }
    }];
}

#pragma mark SMS

- (void) partagerParMessage {
    
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    if ([MFMessageComposeViewController canSendText]) {
        
        NSString *typeStr = @"article";
        switch (_item.type) {
            case APIPodcast:
                typeStr = @"podcast";
                break;
            case APIAnnuaire:
                typeStr = @"livre audio";
                break;
            case APIWC:
                typeStr = @"livre audio";
                break;
            case APILivresAudios:
                typeStr = @"livre audio";
                break;
                
            default:
                typeStr = @"article";
                break;
        }
        
        controller.body = [NSString stringWithFormat:@"Salut, voici un %@ qui devrait t'intéresser : %@, via %@ %@", typeStr, _item.URLArticle, kSiteName, kUrlApp];
        controller.messageComposeDelegate = self;
        [MyAppDelegate.window.rootViewController presentViewController:controller animated:YES completion:nil];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur" message:@"votre appareil n'est pas configuré pour envoyer des messages. Merci de le configurer dans les paramètres et de réessayer." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
}

- (void) messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    
    [MyAppDelegate.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
    if (result == MessageComposeResultCancelled) {
        //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Partager" message:@"Vous avez annulé l'envoi de votre message." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        //        [alert show];
    } else if (result == MessageComposeResultSent) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Partager" message:@"Votre message a bien été envoyé." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Partager" message:@"Une erreur s'est produite pendant l'envoi du message. Merci de réessayer ultérieurement." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark Mail

- (void) partagerParMail {
    
    MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
    if ([MFMailComposeViewController canSendMail]) {
        
        NSString *typeStr = @"article";
        switch (_item.type) {
            case APIPodcast:
                typeStr = @"podcast";
                break;
            case APIAnnuaire:
                typeStr = @"livre audio";
                break;
            case APIWC:
                typeStr = @"livre audio";
                break;
            case APILivresAudios:
                typeStr = @"livre audio";
                break;
                
            default:
                typeStr = @"article";
                break;
        }
        
        [controller setMessageBody:[NSString stringWithFormat:@"Salut,\n\nVoici un %@ qui devrait t'intéresser : %@\n\nVia %@ [%@]", typeStr, _item.URLArticle, kSiteName, kUrlApp] isHTML:NO];
        [controller setSubject:[NSString stringWithFormat:@"%@, via %@", _item.titre, kSiteName]];
        controller.mailComposeDelegate = self;
        [MyAppDelegate.window.rootViewController presentViewController:controller animated:YES completion:nil];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur" message:@"Votre appareil n'est pas configuré pour envoyer des mails. Merci de le configurer dans les paramètres et de réessayer." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    [MyAppDelegate.window.rootViewController dismissViewControllerAnimated:YES completion:Nil];
    if (result == MFMailComposeResultCancelled) {
        //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Partager" message:@"Vous avez annulé l'envoi de votre message." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        //        [alert show];
    } else if (result == MFMailComposeResultSent) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Partager" message:@"Votre message a bien été envoyé." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    } else if (result == MFMailComposeResultSaved) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Partager" message:@"Votre message a bien été sauvegardé dans vos brouillons." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Partager" message:@"Une erreur s'est produite pendant l'envoi du message. Merci de réessayer ultérieurement." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
}


#pragma mark Favoris

- (void) mettreEnFavoris {
    
    if (_isFavoris) {
        
        [[WPPlistRecup shared] retirerUnFavorisForID:_item.postID];
        _isFavoris = NO;
        
    } else {
        _isFavoris = [[WPPlistRecup shared] ajouterUnFavoris:_item];
    }
    
    [favorisCustomButton setSelected:_isFavoris];
}

#pragma mark - Audio

- (void) lireFichierAudio {
    
    BOOL isFichierAudio = NO;
    RLMResults *livresAudio = [TTLivreAudio objectsWhere:[NSString stringWithFormat:@"articleID == %i", _item.postID]];
    
    if (livresAudio.count > 0) {
        // Lecture depuis la bibliothèque depuis le début
        TTLivreAudio *livreAudio = [livresAudio firstObject];
        
        NSString *documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
        NSString *downloadFolder = [documentsPath stringByAppendingPathComponent:@"livres-audio"];
        NSString *downloadFilename = [downloadFolder stringByAppendingPathComponent:[livreAudio.URLFichier lastPathComponent]];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:downloadFilename]) {
            
            _currentListening = livreAudio;
            
            isFichierAudio = YES;
            _interneMoviePlayerViewController = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL fileURLWithPath:downloadFilename]];
            
            [[NSNotificationCenter defaultCenter] removeObserver:_interneMoviePlayerViewController
                                                            name:MPMoviePlayerPlaybackDidFinishNotification
                                                          object:_interneMoviePlayerViewController.moviePlayer];
            
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(movieFinishedCallback:)
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:_interneMoviePlayerViewController.moviePlayer];
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieLoadStateDidChange:) name:MPMoviePlayerLoadStateDidChangeNotification object:nil];
            videoLoaded = NO;
            
            [[_interneMoviePlayerViewController moviePlayer] prepareToPlay];
            [_interneMoviePlayerViewController.moviePlayer setControlStyle:MPMovieControlStyleFullscreen];
            [_interneMoviePlayerViewController.moviePlayer setShouldAutoplay:YES];
            [_interneMoviePlayerViewController.moviePlayer setAllowsAirPlay:YES];
            [_interneMoviePlayerViewController.moviePlayer setFullscreen:YES animated:YES];
            [_interneMoviePlayerViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
            [_interneMoviePlayerViewController.moviePlayer setScalingMode:MPMovieScalingModeNone];
            [_interneMoviePlayerViewController.moviePlayer setIsAccessibilityElement:YES];
            _interneMoviePlayerViewController.moviePlayer.movieSourceType = MPMovieSourceTypeFile;
            
            [[_interneMoviePlayerViewController moviePlayer] play];
            [self presentMoviePlayerViewControllerAnimated:_interneMoviePlayerViewController];
            
            // Background audio title
            NSDictionary *info = @{ MPMediaItemPropertyTitle: livreAudio.titre,
                                    MPMediaItemPropertyArtist: @"ABS Essential",
                                    MPMediaItemPropertyPlaybackDuration: [NSNumber numberWithFloat:livreAudio.duree]};
            [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = info;
            [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
            return;
        }
    }
    
    if (!isFichierAudio) {
    
        // Lecture depuis internet
        Reachability *reachability = [Reachability reachabilityForInternetConnection];
        [reachability startNotifier];
        
        NetworkStatus status = [reachability currentReachabilityStatus];
        
        if (status == ReachableViaWiFi) {
            [self lireFichierAudioWithWifi];
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Attention" message:@"Vous ne disposez pas d'une connexion Wifi, la lecture de ce fichier audio pourra en être affectée.\nVoulez-vous tout de même continuer ?" delegate:self cancelButtonTitle:@"Annuler" otherButtonTitles:@"Continuer", nil];
            [alert setTag:2];
            [alert show];
        }
    }
}

- (void)movieFinishedCallback:(NSNotification*)aNotification
{
    // Lecture depuis la bibliothèque depuis le début
    TTLivreAudio *livreAudio = _currentListening;
    
    // Sauvegarder avancement du livre audio
    [[RLMRealm defaultRealm] beginWriteTransaction];
    if (livreAudio.duree > _interneMoviePlayerViewController.moviePlayer.currentPlaybackTime) {
        [livreAudio setStatut:(int)LivreAudioListening];
    } else {
        [livreAudio setStatut:(int)LivreAudioListen];
    }
    [livreAudio setCurrent:_interneMoviePlayerViewController.moviePlayer.currentPlaybackTime];
    [[RLMRealm defaultRealm] commitWriteTransaction];

    NSNumber *finishReason = [[aNotification userInfo] objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    if ([finishReason intValue] != MPMovieFinishReasonPlaybackEnded) {
        
        MPMoviePlayerViewController *moviePlayer = [aNotification object];
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:MPMoviePlayerPlaybackDidFinishNotification
                                                      object:moviePlayer];
        
        [MyAppDelegate.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)movieStreamingFinishedCallback:(NSNotification*)aNotification
{
    NSNumber *finishReason = [[aNotification userInfo] objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    if ([finishReason intValue] != MPMovieFinishReasonPlaybackEnded) {
        
        MPMoviePlayerViewController *moviePlayer = [aNotification object];
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:MPMoviePlayerPlaybackDidFinishNotification
                                                      object:moviePlayer];
        
        [MyAppDelegate.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)movieLoadStateDidChange:(NSNotification *)notification {
    MPMoviePlayerController *player = notification.object;
    MPMovieLoadState loadState = player.loadState;
    
    /* Enough data has been buffered for playback to continue uninterrupted. */
    if (loadState & MPMovieLoadStatePlaythroughOK)
    {
        //should be called only once , so using self.videoLoaded - only for  first time  movie loaded , if required. This function wil be called multiple times after stalled
        if(!videoLoaded)
        {
            [[_interneMoviePlayerViewController moviePlayer] setCurrentPlaybackTime:_currentListening.current];
            videoLoaded = YES;
        }
    }
}

- (void) lireFichierAudioWithWifi {
    
    DLog(@"%@", _audioFileUrl);
    
    if (_audioFileUrl == nil) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Une erreur s'est produite lors du chargement du fichier audio, merci de réessayer ultérieurement." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        return;
    }
    if (!IS_IOS8) {
        [_chargementView setHidden:NO];
        [self enabledBarButtonItem:NO];
        // Lire à travers le browser
        _browserWebView = [[UIWebView alloc] initWithFrame:CGRectMake(self.view.frame.size.width, 0, 0, 0)];
        [_browserWebView setDelegate:self];
        [_browserWebView setTag:1];
        [_browserWebView setMediaPlaybackAllowsAirPlay:YES];
        [_browserWebView setMediaPlaybackRequiresUserAction:NO];
        [MyAppDelegate.window addSubview:_browserWebView];
        NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:_audioFileUrl];
        [_browserWebView loadRequest:urlRequest];
    } else {
        
        _isPlaying = YES;
        _moviePlayerViewController = [[MPMoviePlayerViewController alloc] initWithContentURL:_audioFileUrl];
        [_moviePlayerViewController.moviePlayer setControlStyle:MPMovieControlStyleFullscreen];
        [_moviePlayerViewController.moviePlayer setShouldAutoplay:YES];
        [_moviePlayerViewController.moviePlayer setAllowsAirPlay:YES];
        [_moviePlayerViewController.moviePlayer setFullscreen:YES animated:YES];
        [_moviePlayerViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
        [_moviePlayerViewController.moviePlayer setScalingMode:MPMovieScalingModeNone];
        [_moviePlayerViewController.moviePlayer setIsAccessibilityElement:YES];
        
        [[NSNotificationCenter defaultCenter] removeObserver:_moviePlayerViewController
                                                        name:MPMoviePlayerPlaybackDidFinishNotification
                                                      object:_moviePlayerViewController.moviePlayer];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(movieStreamingFinishedCallback:)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:_moviePlayerViewController.moviePlayer];
        
        [self presentMoviePlayerViewControllerAnimated:_moviePlayerViewController];
        _moviePlayerViewController.moviePlayer.movieSourceType = MPMovieSourceTypeStreaming;
        
        //[_moviePlayerViewController.moviePlayer prepareToPlay];
        //[_moviePlayerViewController.moviePlayer play];
    }
}

- (void) telechargerFichierAudio {
    
    RLMResults *livresAudio = [TTLivreAudio objectsWhere:[NSString stringWithFormat:@"articleID == %i", _item.postID]];
    if (livresAudio.count > 0) {
        TTLivreAudio *livreAudio = [livresAudio objectAtIndex:0];
        if (livreAudio.statut == LivreAudioDownloading) {
            return;
        }
    }
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    
    NetworkStatus status = [reachability currentReachabilityStatus];
    
    if (status == ReachableViaWiFi) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Téléchargement en cours" message:@"Le livre audio va être téléchargé dans votre bibliothèque. Voulez-vous vous rendre dans cet onglet ?" delegate:self cancelButtonTitle:@"Non merci" otherButtonTitles:@"S'y rendre", nil];
        [alert setTag:3];
        [alert show];
        [self startTelechargerFichierAudio];
    } else {
        NSString *texteTitle = @"Télécharger le podcast";
        NSString *texteContent = @"Vous n'êtes pas connecté en WiFi, êtes-vous sûr de vouloir télécharger le podcast ?";
        if (_type == ArticleSubWebsite) {
            texteTitle = @"Télécharger le livre audio";
            texteContent = @"Vous n'êtes pas connecté en WiFi, êtes-vous sûr de vouloir télécharger le livre audio ?";
        }
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:texteTitle message:texteContent delegate:self cancelButtonTitle:@"Plus tard" otherButtonTitles:@"Oui", nil];
        [alert setTag:4];
        [alert show];
    }
}

- (void) startTelechargerFichierAudio {
    if ([_playButton2.titleLabel.text isEqualToString:@"Téléchargement en attente"]) {
        return;
    }
    if (_audioFileUrl == nil) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Une erreur s'est produite lors du téléchargement du fichier audio, merci de réessayer ultérieurement." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    // Check downloading
    RLMResults *livresAudio = [TTLivreAudio objectsWhere:[NSString stringWithFormat:@"articleID == %i", _item.postID]];
    if (livresAudio.count > 0) {
        TTLivreAudio *livreAudio = [livresAudio objectAtIndex:0];
        if (livreAudio.statut != LivreAudioDownloading) {
            if (_type == ArticleSubWebsite) {
                [_playButton2 setTitle:@"Écouter le livre audio" forState:UIControlStateNormal];
            } else {
                [_playButton2 setTitle:@"Écouter le podcast" forState:UIControlStateNormal];
            }
            [_playButton2 addTarget:self action:@selector(lireFichierAudio) forControlEvents:UIControlEventTouchUpInside];
            return;
        } else {
            [[TTDownloadManager shared] setDelegate:self];
            [[TTDownloadManager shared] startDownload];
            
            [_playButton2 setTitle:@"Téléchargement en attente" forState:UIControlStateNormal];
            return;
        }
    }
    
    // Download
    TTLivreAudio *livreAudio = [[TTLivreAudio alloc] init];
    [livreAudio setArticleID:_item.postID];
    [livreAudio setStatut:(int)LivreAudioDownloading];
    
    // Titre
    NSString *title_article = _item.titre;
    [livreAudio setTitre:title_article];
    [livreAudio setContent:_item.content];
    [livreAudio setImageURL:_item.URLFeatureImg];
    [livreAudio setDate:_item.date];
    
    [livreAudio setURLFichier:_audioFileUrl.absoluteString];
    [livreAudio setCurrent:0];
    
    if (_type == ArticleSubWebsite) {
        [livreAudio setType:1];
    } else {
        [livreAudio setType:0];
    }
    
    // Pour achat
    [livreAudio setIsBuy:NO];
    [livreAudio setPrice:0];
    [livreAudio setIsDownload:YES];
    [livreAudio setIdentifier:@""];
    
    // Get the default Realm
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm addObject:livreAudio];
    [realm commitWriteTransaction];
    
    [[TTDownloadManager shared] setDelegate:self];
    [[TTDownloadManager shared] startDownload];
    
    [_playButton2 setTitle:@"Téléchargement en attente" forState:UIControlStateNormal];
}

#pragma mark - Download Delegate

- (void)progress:(float)progress downloadForURL:(NSString *)URLStr {
    if ([URLStr isEqualToString:_audioFileUrl.absoluteString]) {
        [_playButton2 setTitle:[NSString stringWithFormat:@"Téléchargement en cours %.0f%%", progress] forState:UIControlStateNormal];
    }
}

- (void)finishedDownloadForURL:(NSString *)URLStr {
    if ([URLStr isEqualToString:_audioFileUrl.absoluteString]) {
        if (_type == ArticleSubWebsite) {
            [_playButton2 setTitle:@"Écouter le livre audio" forState:UIControlStateNormal];
        } else {
            [_playButton2 setTitle:@"Écouter le podcast" forState:UIControlStateNormal];
        }
        [_playButton2 addTarget:self action:@selector(lireFichierAudio) forControlEvents:UIControlEventTouchUpInside];
        [_playButton2 removeTarget:self action:@selector(telechargerFichierAudio) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)failDownloadForURL:(NSString *)URLStr {
    if (_type == ArticleSubWebsite) {
        [_playButton2 setTitle:@"Télécharger le livre audio" forState:UIControlStateNormal];
    } else {
        [_playButton2 setTitle:@"Télécharger le podcast" forState:UIControlStateNormal];
    }
    [_playButton2 addTarget:self action:@selector(telechargerFichierAudio) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - URL

- (NSURL*) getUrlInContent:(NSString*)content forBeggining:(NSString*)beginUrl andEnd:(NSString*)endUrl {
    
    NSArray *strings = [content componentsSeparatedByString:beginUrl];
    if ([strings count] > 1) {
        NSArray *strings2 = [[strings objectAtIndex:1] componentsSeparatedByString:endUrl];
        if ([strings count] > 0) {
            NSString *audioFile = [strings2 objectAtIndex:0];
            // Vérification de l'URL récupérée
            if ([NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@", beginUrl, audioFile, endUrl]] != nil) {
                return [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@", beginUrl, audioFile, endUrl]];
            }
        }
    }
    return nil;
}

#pragma mark - Publicité
#if kisInterstitielArticles
#pragma mark Interstitial

- (void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, @"Ouverture de la publicité");
    if ([self isFirstResponder]) {
        [interstitial_ presentFromRootViewController:MyAppDelegate.window.rootViewController];
    } else {
        interstitial_ = nil;
    }
}

- (void)interstitialDidDismissScreen:(GADInterstitial *)ad {
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, @"Fermeture de la publicité");
    interstitial_ = nil;
}

- (void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error {
    interstitial_ = nil;
}
#endif

@end
