//
//  WPArticlesViewController.h
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGORefreshTableHeaderView.h"
#import "WPAPIRequest.h"
#import <AVFoundation/AVFoundation.h>

@protocol WPArticlesWCViewControllerDelegate <NSObject>
- (void) openArticleWCViewWithArticle:(TTPost*)item andType:(ArticleType)type;
@end

@interface WPArticlesWCViewController : UIViewController <UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate, EGORefreshTableHeaderDelegate, WPAPIRequestDelegate> {
    
    EGORefreshTableHeaderView   *_refreshHeaderView;
	BOOL                        _reloading;
    
    TTChargementView            *_chargementView;
}

@property (nonatomic, weak) id<WPArticlesWCViewControllerDelegate>delegate;
@property (strong, nonatomic) UIView *contentView;
@property (strong, nonatomic) NSArray *articles;

@property (strong, nonatomic) UITableView *aTableView;
@property (strong, nonatomic) UIView *reloadView;

@property (assign, nonatomic) ArticlesType theType;

/* AUDIO */
@property (nonatomic, strong) AVAudioPlayer *soundPlayer;

- (id)initWithType:(ArticlesType)aType andFrame:(CGRect)frame andDelegate:(id)delegate;

- (void) startLoadingView;
- (void) stopLoadingViewWithFooter:(BOOL)isFooter;
- (void) reloadViewWithArticles;
- (void) reloadViewWithArticlesType:(ArticlesType)aType;
- (void) reloadArticleViewWithArticles:(NSArray*)articles;
- (void) addReloadViewForCenter:(CGPoint)center;

- (void) reloadTableViewDataSource;
- (void) doneLoadingTableViewData;

@end
