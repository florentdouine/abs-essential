//
//  WPArticlesViewController.m
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import "WPArticlesWCViewController.h"
#import "UIImageView+WebCache.h"
#import "WPArticleCell.h"
#import <Realm/Realm.h>

@implementation WPArticlesWCViewController
@synthesize contentView = _contentView;
@synthesize articles = _articles;
@synthesize aTableView = _aTableView;
@synthesize theType = _theType;
@synthesize delegate = _delegate;
@synthesize soundPlayer = _soundPlayer;

- (id)initWithType:(ArticlesType)aType andFrame:(CGRect)frame andDelegate:(id)delegate
{
    self = [super init];
    if (self) {
        
        [self.view setBackgroundColor:kColorBackground];
        
        _theType = aType;
        _delegate = delegate;
        
        [self.view setFrame:frame];
        
        // Title
        if (aType == ArticlesAll) {
        } else {
            UIButton *reloadCustomButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
            [reloadCustomButton setBackgroundImage:[UIImage imageNamed:@"button_reload_off.png"] forState:UIControlStateNormal];
            [reloadCustomButton setBackgroundImage:[UIImage imageNamed:@"button_reload_on.png"] forState:UIControlStateHighlighted];
            [reloadCustomButton addTarget:self action:@selector(refreshAction) forControlEvents:UIControlEventTouchUpInside];
            [reloadCustomButton setAccessibilityLabel:@"Recharger la liste"];
            UIBarButtonItem *reloadButton = [[UIBarButtonItem alloc] initWithCustomView:reloadCustomButton];
            [self.navigationItem setRightBarButtonItem:reloadButton];
            
            if ((int)_theType > 100) {
                NSArray *categories = kArraySubWebsite01CategoriesName;
                [self setTitle:[[categories objectAtIndex:aType-1-100] lastPathComponent]];
            }
        }
        
        // ContentView
        
        CGFloat height = self.view.frame.size.height;
        if (aType == 0) {
            height = kViewHeight-20;
        }
        
        _contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, height)];
        [_contentView setBackgroundColor:kColorBackground];
        [self.view addSubview:_contentView];

        _aTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, _contentView.frame.size.width, _contentView.frame.size.height)];
        [_aTableView setBackgroundColor:kColorBackground];
        [_aTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [_aTableView setSeparatorColor:kColorSeparator];
        [_aTableView setDelegate:self];
        [_aTableView setDataSource:self];
        [_aTableView setShowsVerticalScrollIndicator:YES];
        [_aTableView setAccessibilityLabel:@"Chargement"];
        [_contentView addSubview:_aTableView];
        
        // Load
        _chargementView = [[TTChargementView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];;
        [[self view] addSubview:_chargementView];
        [_chargementView setHidden:YES];
        
        if (_refreshHeaderView == nil) {
            EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0, 0 - _aTableView.bounds.size.height, self.view.frame.size.width, _aTableView.bounds.size.height)];
            view.delegate = self;
            [_aTableView addSubview:view];
            _refreshHeaderView = view;
        }
        
        [self startLoadingView];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[[WPAPIRequest shared] loadArticlesForType:ArticlesAll withDelegate:self];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    // Google Analytics
    id<GAITracker> googleTracker = [[GAI sharedInstance] trackerWithTrackingId:kTrackingID];
    
    NSString *value = @"";
    if (_theType == ArticlesAll) {
        value = @"Librairie - Tous";
    } else {
        NSArray *categories = kArraySubWebsite01CategoriesName;
        if ((int)_theType-1-100 >= 0 && (int)_theType-1-100 < [categories count]) {
            value = [NSString stringWithFormat:@"Librairie - %@", [categories objectAtIndex:_theType-1-100]];
        } else {
            value = @"Librairie";
        }
    }
    
    [googleTracker set:kGAIScreenName value:value];
    [googleTracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

#pragma mark - Accessibility

- (BOOL)accessibilityScroll:(UIAccessibilityScrollDirection)direction {
    
    if (direction == UIAccessibilityScrollDirectionUp && _aTableView.contentOffset.y == 0) {
        
        UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, @"Rechargement de la vue.");
        [self reloadViewWithArticles];
        return YES;
    }
    return NO;
}

#pragma mark - Actions
#pragma mark Reload

- (void) refreshAction {
    [self reloadViewWithArticles];
}

- (void)reloadViewWithArticles {
    [self reloadViewWithArticlesType:_theType];
}

- (void)reloadViewWithArticlesType:(ArticlesType)aType {
    
    if (_reloadView != nil) {
        [_reloadView removeFromSuperview];
        _reloadView = nil;
    }
    _articles = [NSArray new];
    [_aTableView reloadData];
    [_aTableView setTableFooterView:[UIView new]];
    [_aTableView setScrollEnabled:YES];
    
    _theType = aType;
    [self startLoadingView];
    if (aType == ArticlesAll) {
        [[WPAPIRequest shared] loadLast25ArticlesFromWooCommerceWithDelegate:self];
    } else {
        NSArray *categories = kAPISubWebsite01ArrayCategories;
        [[WPAPIRequest shared] loadArticlesFromWooCommerceForCategory:[[categories objectAtIndex:aType-1-100] lastPathComponent] withDelegate:self];
    }
}

- (void)startLoadingView {
    [_chargementView setHidden:NO];
}

- (void)stopLoadingViewWithFooter:(BOOL)isFooter {
    
    [_chargementView setHidden:YES];
    if (kisAccessibiliteSounds) {
        // Son
        if ([[WPPlistRecup shared] getReglageSon]) {
            NSError *error;
            _soundPlayer = [[AVAudioPlayer alloc]
                            initWithContentsOfURL:[[NSBundle mainBundle]
                                                   URLForResource:@"sound-rafrechire-postes-abs"
                                                   withExtension:@"caf"]
                            error:&error];
            [_soundPlayer play];
        }
        
    } else {
        UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, @"Contenu chargé");
    }
    
    // Footer
    /*CGFloat height = _aTableView.frame.size.height - _articles.count*kGetFontSize*65;
    [_aTableView setScrollEnabled:YES];
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, height)];
    [footer setBackgroundColor:kColorBackground];*/
    [_aTableView setTableFooterView: [UIView new]];
}

- (void)reloadArticleViewWithArticles:(NSArray*)articles {
    
    _articles = articles;
    //    NSLog(@"[ArticleViewController] articles : %@", _articles);
    
    if (_articles == nil) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur de chargement" message:@"Un problème est survenu lors du chargement de la librairie. Vérifiez votre connexion internet et essayez de recharger la vue." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        [self stopLoadingViewWithFooter:NO];
        [self addReloadViewForCenter:_contentView.center];
        [self doneLoadingTableViewData];
        return;
    }
    
    if (_articles.count == 0) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Aucun livre audio" message:@"Cette catégorie ne comporte pas encore de livre audio, merci de la consulter de nouveau ultérieurement." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        [[self navigationController] popViewControllerAnimated:YES];
        return;
    }
    
    [_aTableView reloadData];
    //    NSIndexPath *path = [NSIndexPath indexPathForRow:0 inSection:0];
    //    [_aTableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionTop animated:YES];
    //[_aTableView scrollRectToVisible:CGRectMake(0, 0, _aTableView.frame.size.width, _aTableView.frame.size.height) animated:YES];
    
    [self stopLoadingViewWithFooter:NO];
    [self doneLoadingTableViewData];
}

#pragma mark - ScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {

    [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}

#pragma mark - Actions

- (void) goToArticle:(id)sender {
    
    UIButton *btn = (UIButton*)sender;
    //    NSLog(@"[ArticleViewController] ID article : %i", [btn tag]);
    TTPost *item = [_articles objectAtIndex:[btn tag]];
    
    ArticleType articleType = ArticleMenu;
    NSArray *categories = kAPIArrayCategories;
    if (_theType != ArticlesAll) {
        if (categories.count >= _theType-1) {
            if ([[categories objectAtIndex:_theType-1] isEqualToString:@"SW01"]) {
                articleType = ArticleSubWebsite;
            }
        }
        if (_theType != ArticlesAll) {
            articleType = ArticleSubArticle;
        }
        if ((int)_theType >= 200) {
            articleType = ArticleSubWebsiteInfos;
        } else if ((int)_theType > 100) {
            articleType = ArticleSubWebsite;
        }
    }
    
    if ([_delegate respondsToSelector:@selector(openArticleWCViewWithArticle:andType:)]) {
        [_delegate openArticleWCViewWithArticle:item andType:articleType];
    }
}

#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 35)];
    [header setBackgroundColor:kColorBackground2];
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(20, 7, self.view.frame.size.width-20, 25)];
    [lbl setText:@"Ici la liste des 5 derniers livres audios"];
    [lbl setFont:[UIFont fontWithName:kFontNameBold size:16]];
    [header addSubview:lbl];
    [lbl sizeToFit];
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kGetFontSize*65.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return ([(NSArray*)_articles count]);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TTPost *item = [_articles objectAtIndex:indexPath.row];
    
    // Auteur et prix
    NSString *auteur = item.auteur;
    NSString *price = item.prix;
    RLMResults *livresAudio = [TTLivreAudio objectsWhere:[NSString stringWithFormat:@"identifier == \"%@\" && isBuy == YES", item.identifier]];
    if (livresAudio.count > 0) {
        price = @"Acheté";
    }
    
    NSString *authorAndPrice = [NSString stringWithFormat:@"De %@, %@", auteur, price];
    if ([auteur isEqualToString:@""] || auteur == nil) {
        authorAndPrice = [NSString stringWithFormat:@"%@", price];
    }
    [item setSoustitre:authorAndPrice];
    
    static NSString *CellIdentifier = @"WooCommerceCell";
    WPArticleCell *cell = (WPArticleCell*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[WPArticleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier andWidth:self.view.frame.size.width];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    [cell setPost:item];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    TTPost *item = [_articles objectAtIndex:indexPath.row];
//    NSString *articleID = [anArticle objectForKey:@"ID"];
    //    NSLog(@"[ArticleViewController] ID article : %@", articleID);
    
    ArticleType articleType = ArticleSubWebsite;
    
    if ([_delegate respondsToSelector:@selector(openArticleWCViewWithArticle:andType:)]) {
        [_delegate openArticleWCViewWithArticle:item andType:articleType];
    }
    
}

// No separator inset !
- (void)viewDidLayoutSubviews {
    [_aTableView setSeparatorInset:UIEdgeInsetsZero];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(WPArticleCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]){
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero]; // ios 8 newly added
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - EGORefresh
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource {
    
    if (_reloading) {
        [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:_aTableView];
        return;
    }
    
    _reloading = YES;
    if (_theType == ArticlesAll) {
        [[WPAPIRequest shared] loadLast25ArticlesFromWooCommerceWithDelegate:self];
    } else {
        NSArray *categories = kAPISubWebsite01ArrayCategories;
        [[WPAPIRequest shared] loadArticlesFromWooCommerceForCategory:[[categories objectAtIndex:_theType-1-100] lastPathComponent] withDelegate:self];
    }
}

- (void)doneLoadingTableViewData {
    
	_reloading = NO;
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:_aTableView];
}

#pragma mark - UIScrollViewDelegate Methods

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	
	[_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

#pragma mark - EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view {
	
	[self reloadTableViewDataSource];
    //	[self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:2.0];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view {
	return _reloading;
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view {
	return [NSDate date];
}

#pragma mark - APIDelegate

- (void)finishedLoadArticlesFromWooCommerceWithStatut:(RequestStatut)aStatut andJSONArray:(NSArray *)aJSON {
    
    if (aStatut == RequestStatutSucceeded) {
        [self reloadArticleViewWithArticles:aJSON];
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur de chargement" message:@"Un problème est survenu lors du chargement de la librairie. Vérifiez votre connexion internet et essayez de recharger la vue." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        [self stopLoadingViewWithFooter:NO];
        [self addReloadViewForCenter:_contentView.center];
    }
}

- (void)finishedLoadLast25ArticlesFromWooCommerceWithStatut:(RequestStatut)aStatut andJSONArray:(NSArray *)aJSON {
    
    if (aStatut == RequestStatutSucceeded) {
        [self reloadArticleViewWithArticles:aJSON];
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur de chargement" message:@"Un problème est survenu lors du chargement de la librairie. Vérifiez votre connexion internet et essayez de recharger la vue." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        [self stopLoadingViewWithFooter:NO];
        [self addReloadViewForCenter:_contentView.center];
    }
}

#pragma mark - ReloadView

- (void) addReloadViewForCenter:(CGPoint)center {
    
    if (center.x == 0 && center.y == 0) {
        center = self.view.center;
    }
    
    _reloadView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, _contentView.frame.size.height)];
    [_reloadView setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.2]];
    [self.view addSubview:_reloadView];
    
    UIButton *reloadButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 35.0, 35.0)];
    [reloadButton setCenter:center];
    [reloadButton setBackgroundImage:[UIImage imageNamed:@"button_reload_on.png"] forState:UIControlStateNormal];
    [reloadButton setBackgroundImage:[UIImage imageNamed:@"button_reload_off.png"] forState:UIControlStateHighlighted];
    [reloadButton setAccessibilityLabel:@"Recharger la vue"];
    [reloadButton addTarget:self action:@selector(tryToReload) forControlEvents:UIControlEventTouchUpInside];
    [_reloadView addSubview:reloadButton];
}

- (void) tryToReload {
    [self reloadViewWithArticlesType:_theType];
}

@end
