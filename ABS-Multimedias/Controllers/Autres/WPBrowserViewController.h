//
//  WPBrowserViewController.h
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WPBrowserViewController : UIViewController <UIWebViewDelegate, UIActionSheetDelegate> {
    
    NSString *_url;
}

@property (nonatomic, retain) UILabel						*titre;
@property (nonatomic, retain) UIView						*activityIndicator;
@property (nonatomic, retain) UIWebView						*browserWebView;

- (void) setStringRequest:(NSString*)aStringRequest;

- (void) refreshAction;
- (void) doneAction;

@end