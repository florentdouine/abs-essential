//
//  WPBrowserViewController.m
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import "WPBrowserViewController.h"

@implementation WPBrowserViewController

@synthesize titre;
@synthesize activityIndicator;
@synthesize browserWebView;

- (id)init {
    
    self = [super init];
    if (self) {
        // Custom initialization
        [self.view setBackgroundColor:kColorBackground];
        
        // BarButtonItem
        
        UIButton *fermerCustomButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
        [fermerCustomButton setBackgroundImage:[UIImage imageNamed:@"button_fermer_off.png"] forState:UIControlStateNormal];
        [fermerCustomButton setBackgroundImage:[UIImage imageNamed:@"button_fermer_on.png"] forState:UIControlStateHighlighted];
        [fermerCustomButton addTarget:self action:@selector(doneAction) forControlEvents:UIControlEventTouchUpInside];
        [fermerCustomButton setAccessibilityLabel:@"Retour"];
        UIBarButtonItem *fermerButton = [[UIBarButtonItem alloc] initWithCustomView:fermerCustomButton];
        
        UIButton *safariCustomButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 24.0, 24.0)];
        [safariCustomButton setBackgroundImage:[UIImage imageNamed:@"button_safari_off.png"] forState:UIControlStateNormal];
        [safariCustomButton setBackgroundImage:[UIImage imageNamed:@"button_safari_on.png"] forState:UIControlStateHighlighted];
        [safariCustomButton addTarget:self action:@selector(goToSafari) forControlEvents:UIControlEventTouchUpInside];
        [safariCustomButton setAccessibilityLabel:@"Ouvrir dans safari"];
        UIBarButtonItem *safariButton = [[UIBarButtonItem alloc] initWithCustomView:safariCustomButton];
        
        UIButton *reloadCustomButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
        [reloadCustomButton setBackgroundImage:[UIImage imageNamed:@"button_reload_off.png"] forState:UIControlStateNormal];
        [reloadCustomButton setBackgroundImage:[UIImage imageNamed:@"button_reload_on.png"] forState:UIControlStateHighlighted];
        [reloadCustomButton addTarget:self action:@selector(refreshAction) forControlEvents:UIControlEventTouchUpInside];
        [reloadCustomButton setAccessibilityLabel:@"Recharger la page"];
        UIBarButtonItem *reloadButton = [[UIBarButtonItem alloc] initWithCustomView:reloadCustomButton];
        
        [self.navigationItem setLeftBarButtonItem:fermerButton];
        [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:safariButton, reloadButton, nil]];
        
        self.browserWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 44.0)];
        [browserWebView setScalesPageToFit:YES];
        [browserWebView setUserInteractionEnabled:YES];
        [browserWebView setDelegate:self];
        [browserWebView setBackgroundColor:kColorBackground];
        [self.view addSubview:browserWebView];
        
        // Retirer l'ombre de la webview
        id scrollview = [browserWebView.subviews objectAtIndex:0];
        for (UIView *subview in [scrollview subviews])
            if ([subview isKindOfClass:[UIImageView class]])
                subview.hidden = YES;
        //
        
        self.activityIndicator = [[UIView alloc] initWithFrame:browserWebView.frame];
        [activityIndicator setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.5]];
        UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [activity setCenter:self.browserWebView.center];
        [activity startAnimating];
        [activity setAlpha:1.0];
        [activityIndicator addSubview:activity];
        [[self view] addSubview:activityIndicator];
    }
    return self;
}

- (void) setStringRequest:(NSString*)aStringRequest {
    
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:aStringRequest]];
//    [titre setText:aStringRequest];
    [browserWebView	loadRequest:urlRequest];
    _url = aStringRequest;
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    // Google Analytics
    id<GAITracker> googleTracker = [[GAI sharedInstance] trackerWithTrackingId:kTrackingID];
    [googleTracker set:kGAIScreenName value:[NSString stringWithFormat:@"Article url : %@", _url]];
    [googleTracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

#pragma mark - Delegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
	if([[[request URL] description] isEqualToString:@"about:blank"])
		return NO;
    
	return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
	[activityIndicator setAlpha:1.0];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
	[activityIndicator setAlpha:0];
	
	// Title
//	[titre setText:[webView stringByEvaluatingJavaScriptFromString:@"document.title"]];
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
	[activityIndicator setAlpha:0];
}

#pragma mark - Button

- (void)previousAction {
	[browserWebView goBack];
}

- (void)nextAction {
	[browserWebView goForward];
}

- (void)refreshAction {
	[browserWebView reload];
}

- (void)stopAction {
	[browserWebView stopLoading];
}

- (void)doneAction {
	[browserWebView stopLoading];
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void) goToSafari {
    
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Annuler" destructiveButtonTitle:nil otherButtonTitles:@"Ouvrir dans Safari", nil];
    [sheet setTag:1];
    [sheet showInView:self.view];
}

#pragma mark - Memory

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIAlertView Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (actionSheet.tag == 1 && buttonIndex == 0) {
        BOOL isOpenUrl = [[UIApplication sharedApplication] openURL:browserWebView.request.URL];
        if (!isOpenUrl) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur" message:@"L'application ne parvient pas ouvrir cet url dans safari." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }
    }
}

@end