//
//  WPContactViewController.h
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface WPContactViewController : UIViewController <MFMailComposeViewControllerDelegate, UIAlertViewDelegate>

@end
