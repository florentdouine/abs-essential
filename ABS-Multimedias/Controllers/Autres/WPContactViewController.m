//
//  WPContactViewController.m
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import "WPContactViewController.h"
#import "WPAppDelegate.h"
#import "WPBrowserViewController.h"

@implementation WPContactViewController

- (id)init
{
    self = [super init];
    if (self) {
        
        
        [self.view setBackgroundColor:kColorBackground];
        [self setTitle:@"Contact"];
        
        UIScrollView *scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-64)];
        [[self view] addSubview:scroll];
        
        CGFloat y = 20.0;
        
        UILabel *lblContact = [[UILabel alloc] initWithFrame:CGRectMake(20.0, y, self.view.frame.size.width - 40, self.view.frame.size.height - 44.0 - 40)];
        [lblContact setText:@"ABS-Multimédias est une association régie par la loi de 1901. Elle est donc à but non lucratif. Et elle a pour objectif :\nDe mettre à la disposition d’éditeurs de contenus écrits (livres, articles, autres), un moyen de les porter sur un support sonore de qualité professionnelle pour les rendre accessibles à tout public ;\nDe favoriser l’accès des non-voyants à la culture littéraire dans des conditions sonores de qualité professionnelle ;\nDe favoriser l’accès de tout public aux techniques de réalisation de contenus multimédias (sonores, vidéos, animations diverses, musiques assistées par ordinateur) en studio. Ces publics incluent notamment les non-voyants.\nNous réalisons donc des livres audio à la demande, des livres audio à acheter ; et des prestations diverses touchant au multimédia."];
        [lblContact setFont:[UIFont fontWithName:kFontNameLight size:kGetFontSize*16.0f]];
        [lblContact setTextColor:[UIColor blackColor]];
        [lblContact setBackgroundColor:[UIColor clearColor]];
        [lblContact setNumberOfLines:0];
        [lblContact setContentMode:UIViewContentModeTopLeft];
        [scroll addSubview:lblContact];
        [lblContact sizeToFit];
        
        y += lblContact.frame.size.height + 20.0;
        
        CGFloat distanceBtn = (self.view.frame.size.width - 140*2)/3;
        
        UIButton *enSavoirPlus = [[UIButton alloc] initWithFrame:CGRectMake(distanceBtn, y, 140, 40)];
        [enSavoirPlus setBackgroundColor:kColorNextArticles];
        [enSavoirPlus setTitle:@"En savoir plus" forState:UIControlStateNormal];
        [enSavoirPlus setTitleColor:kColorTextLight forState:UIControlStateNormal];
        [enSavoirPlus setTitleColor:kColorText forState:UIControlStateHighlighted];
        [enSavoirPlus setContentMode:UIViewContentModeCenter];
        [[enSavoirPlus titleLabel] setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*16.0]];
        [enSavoirPlus addTarget:self action:@selector(enSavoirPlus) forControlEvents:UIControlEventTouchUpInside];
        [scroll addSubview:enSavoirPlus];
        
        UIButton *faireDon = [[UIButton alloc] initWithFrame:CGRectMake(distanceBtn*2+140, y, 140, 40)];
        [faireDon setBackgroundColor:kColorNextArticles];
        [faireDon setTitle:@"Faire un don" forState:UIControlStateNormal];
        [faireDon setTitleColor:kColorTextLight forState:UIControlStateNormal];
        [faireDon setTitleColor:kColorText forState:UIControlStateHighlighted];
        [faireDon setContentMode:UIViewContentModeCenter];
        [[faireDon titleLabel] setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*16.0]];
        [faireDon addTarget:self action:@selector(faireDon) forControlEvents:UIControlEventTouchUpInside];
        [scroll addSubview:faireDon];
        
        y += faireDon.frame.size.height + distanceBtn;
        
        if (![kContactEmail isEqualToString:@""]) {
            UIButton *sendEmail = [[UIButton alloc] initWithFrame:CGRectMake(distanceBtn, y, 140, 40)];
            [sendEmail setBackgroundColor:kColorNextArticles];
            [sendEmail setTitle:@"Écrire un mail" forState:UIControlStateNormal];
            [sendEmail setTitleColor:kColorTextLight forState:UIControlStateNormal];
            [sendEmail setTitleColor:kColorText forState:UIControlStateHighlighted];
            [sendEmail setContentMode:UIViewContentModeCenter];
            [[sendEmail titleLabel] setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*16.0]];
            [sendEmail addTarget:self action:@selector(sendEmail) forControlEvents:UIControlEventTouchUpInside];
            [scroll addSubview:sendEmail];
        }
        
        if (![kContactNumero isEqualToString:@""]) {
            UIButton *makeCall = [[UIButton alloc] initWithFrame:CGRectMake(distanceBtn*2+140, y, 140, 40)];
            [makeCall setBackgroundColor:kColorNextArticles];
            [makeCall setTitle:@"Appeler" forState:UIControlStateNormal];
            [makeCall setTitleColor:kColorTextLight forState:UIControlStateNormal];
            [makeCall setTitleColor:kColorText forState:UIControlStateHighlighted];
            [makeCall setContentMode:UIViewContentModeCenter];
            [[makeCall titleLabel] setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*16.0]];
            [makeCall addTarget:self action:@selector(makePhoneCall) forControlEvents:UIControlEventTouchUpInside];
            [scroll addSubview:makeCall];
            
            y += makeCall.frame.size.height + 20.0;
        }
        
        [scroll setContentSize:CGSizeMake(self.view.frame.size.width, y)];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    // Google Analytics
    id<GAITracker> googleTracker = [[GAI sharedInstance] trackerWithTrackingId:kTrackingID];
    [googleTracker set:kGAIScreenName value:@"Contact"];
    [googleTracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

#pragma mark - Actions

- (void) enSavoirPlus {
    WPBrowserViewController *browserController = [[WPBrowserViewController alloc] init];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:browserController];
    [navController.navigationBar setTranslucent:NO];
    [browserController setStringRequest:@"https://www.abs-multimedias.com/abs-plus-info/"];
    [MyAppDelegate.window.rootViewController presentViewController:navController animated:YES completion:nil];
}

- (void) faireDon {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.abs-multimedias.com/donation/"]];
}

- (void) sendEmail {
 
    MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
    if ([MFMailComposeViewController canSendMail]) {
        
        [controller setSubject:[NSString stringWithFormat:@"Contact via %@ v%@", kAppName, kCurrentAppVersion]];
        controller.mailComposeDelegate = self;
        if ([controller.navigationBar respondsToSelector:@selector(setBarTintColor:)]) {
            [controller.navigationBar setTintColor:kColorNavBar];
        }
        [MyAppDelegate.window.rootViewController presentViewController:controller animated:YES completion:nil];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur" message:@"Votre appareil n'est pas configuré pour envoyer des mails. Merci de le configurer dans les paramètres et de réessayer." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
}

- (void) makePhoneCall {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Appeler" message:[NSString stringWithFormat:@"Êtes-vous sûr de vouloir appeler %@?", kAppName] delegate:self cancelButtonTitle:@"Annuler" otherButtonTitles:@"Oui", nil];
    [alert setTag:1];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1 && buttonIndex == 1) {
//        DLog(@"Make Call");
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@", kContactNumero]]];
    }
}

#pragma mark Mail Delegate

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    [MyAppDelegate.window.rootViewController dismissViewControllerAnimated:YES completion:Nil];
    if (result == MFMailComposeResultCancelled) {
        //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Partager" message:@"Vous avez annulé l'envoi de votre message." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        //        [alert show];
    } else if (result == MFMailComposeResultSent) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Contact" message:@"Votre message a bien été envoyé." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    } else if (result == MFMailComposeResultSaved) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Contact" message:@"Votre message a bien été sauvegardé dans vos brouillons." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Contact" message:@"Une erreur s'est produite pendant l'envoi du message. Merci de réessayer ultérieurement." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
}

@end
