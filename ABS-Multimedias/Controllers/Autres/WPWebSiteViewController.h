//
//  KeynoteViewController.h
//  MacinPoche
//
//  Created by Virginie on 27/06/13.
//  Copyright (c) 2013 toutdanslapoche. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WPWebSiteViewController : UIViewController <UIWebViewDelegate> {
    
    NSString *_urlString;
}

@property (strong, nonatomic) UIWebView	*browserWebView;
@property (strong, nonatomic) UIView *activityIndicator;

- (id)initWithUrl:(NSString*)urlString;
- (void)reloadKeynoteDataWithUrl:(NSString*)urlString;

@end
