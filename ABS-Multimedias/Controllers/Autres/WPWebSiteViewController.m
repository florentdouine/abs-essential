//
//  KeynoteViewController.m
//  MacinPoche
//
//  Created by Virginie on 27/06/13.
//  Copyright (c) [browserWebView	loadRequest:urlRequest];13 toutdanslapoche. All rights reserved.
//

#import "WPWebSiteViewController.h"

@implementation WPWebSiteViewController
@synthesize browserWebView = _browserWebView;
@synthesize activityIndicator = _activityIndicator;

- (id)initWithUrl:(NSString*)urlString
{
    self = [super init];
    if (self) {
        
        _urlString = urlString;
        [self setTitle:self.title];
        
        _browserWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 44.0)];
        [_browserWebView setScalesPageToFit:YES];
        [_browserWebView setOpaque:YES];
        [_browserWebView setBackgroundColor:kColorBackground];
        [_browserWebView setUserInteractionEnabled:YES];
        [_browserWebView setDelegate:self];
        [self.view addSubview:_browserWebView];
        
        // Retirer l'ombre de la webview
        id scrollview = [_browserWebView.subviews objectAtIndex:0];
        for (UIView *subview in [scrollview subviews])
            if ([subview isKindOfClass:[UIImageView class]])
                subview.hidden = YES;
        //
        
        _activityIndicator = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 44.0)];
        [_activityIndicator setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.5]];
        UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [activity setFrame:CGRectMake((self.view.frame.size.width-30)/2, (self.view.frame.size.height- 44.0-30)/2, 30.0, 30.0)];
        [activity startAnimating];
        [activity setAlpha:1.0];
        [_activityIndicator addSubview:activity];
        [[self view] addSubview:_activityIndicator];
        
        // Load Request
         NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        [_browserWebView loadRequest:urlRequest];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    // Google Analytics
    id<GAITracker> googleTracker = [[GAI sharedInstance] trackerWithTrackingId:kTrackingID];
    [googleTracker set:kGAIScreenName value:[NSString stringWithFormat:@"Url : %@", _urlString]];
    [googleTracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

#pragma mark - Reload

- (void)reloadKeynoteDataWithUrl:(NSString*)urlString {
    
    // Load Request
    NSURLRequest *urlRequest = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    [_browserWebView loadRequest:urlRequest];
    
    // Scroll To Top
    [_browserWebView.scrollView scrollRectToVisible:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) animated:YES];
}

#pragma mark - UIWebView Delegate

- (void)webViewDidStartLoad:(UIWebView *)webView {
	[_activityIndicator setAlpha:1.0];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
	[_activityIndicator setAlpha:0];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
	[_activityIndicator setAlpha:0];
}

@end
