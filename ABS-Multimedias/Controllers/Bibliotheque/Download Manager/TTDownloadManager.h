//
//  TTDownloadCircuitsManager.h
//  Randoland
//
//  Created by Virginie Delaitre on 16/03/2015.
//  Copyright (c) 2015 Toutdanslapoche. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DownloadManager.h"

@protocol TTDownloadManagerDelegate <NSObject>
@optional
- (void) startDownloaded;
- (void) progress:(float)progress downloadForURL:(NSString*)URLStr;
- (void) finishedDownloadForURL:(NSString*)URLStr;
- (void) failDownloadForURL:(NSString*)URLStr;
@end

@interface TTDownloadManager : NSObject <DownloadManagerDelegate> {
    
    BOOL _isDownloading;
}

@property (nonatomic, strong) DownloadManager *downloadManager;
@property (nonatomic, strong) id<TTDownloadManagerDelegate>delegate;

+ (TTDownloadManager*) shared;
- (void) startDownload;
- (BOOL) isLivreAudioToDownload;

@end
