//
//  TTDownloadCircuitsManager.m
//  Randoland
//
//  Created by Virginie Delaitre on 16/03/2015.
//  Copyright (c) 2015 Toutdanslapoche. All rights reserved.
//

#import "TTDownloadManager.h"
#import "RLMResults.h"
#import "RLMRealm.h"
#import <AVFoundation/AVFoundation.h>

@implementation TTDownloadManager
@synthesize downloadManager = _downloadManager;
@synthesize delegate = _delegate;

- (id) init {
    
    self = [super init];
    
    if (self) {
        _isDownloading = NO;
        
        self.downloadManager = [[DownloadManager alloc] initWithDelegate:self];
        [self.downloadManager setMaxConcurrentDownloads:4];
    }
    
    return self;
}

#pragma mark - singleton

+ (TTDownloadManager*) shared {
    
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

#pragma mark -

- (void) startDownload {
    
    if (_isDownloading) {
        return;
    }
    _isDownloading = YES;
    [self performSelectorInBackground:@selector(startDownloadInBackground) withObject:nil];
}

- (void) startDownloadInBackground {
    
    NSString *documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *downloadFolder = [documentsPath stringByAppendingPathComponent:@"livres-audio"];
    
    // Create download manager instance
    
    RLMResults *livresAudio = [TTLivreAudio objectsWhere:[NSString stringWithFormat:@"statut == %i", LivreAudioDownloading]];
    for (int i = 0; i<livresAudio.count; i++) {
        TTLivreAudio *livreAudio = [livresAudio objectAtIndex:i];
        NSString *downloadFilename = [downloadFolder stringByAppendingPathComponent:[livreAudio.URLFichier lastPathComponent]];
        BOOL isDownloading = NO;
        for (Download *down in _downloadManager.downloads) {
            if ([down.url.absoluteString isEqualToString: livreAudio.URLFichier]) {
                isDownloading = YES;
            }
        }
        if (!isDownloading) {
            [self.downloadManager addDownloadWithFilename:downloadFilename URL:[NSURL URLWithString:livreAudio.URLFichier] livreAudio:livreAudio];
        }
    }
    if ([_delegate respondsToSelector:@selector(startDownloaded)]) {
        [_delegate startDownloaded];
    }
    _isDownloading = NO;
}

#pragma mark Verification

- (BOOL)isLivreAudioToDownload {
    RLMResults *livresAudio = [TTLivreAudio objectsWhere:[NSString stringWithFormat:@"statut == %i", LivreAudioDownloading]];
    return (livresAudio.count > 0);
}

#pragma mark - Télécharger le fichier

- (void)downloadManager:(DownloadManager *)downloadManager downloadDidReceiveData:(Download *)download;
{

    float progress = download.progressContentLength;
    float expected = download.expectedContentLength;
    float pourcentage = (progress/expected)*100;
    //    DLog(@"%@ : %f", download.filename, pourcentage);
    
    // filename is retrieved from `download.filename`
    // the bytes downloaded thus far is `download.progressContentLength`
    // if the server reported the size of the file, it is returned by `download.expectedContentLength`
    
    if ([_delegate respondsToSelector:@selector(progress:downloadForURL:)]) {
        [_delegate progress:pourcentage downloadForURL:download.url.absoluteString];
    }
}

- (void)downloadManager:(DownloadManager *)downloadManager downloadDidFinishLoading:(Download *)download;
{
    RLMResults *livresAudio = [TTLivreAudio objectsWhere:[NSString stringWithFormat:@"URLFichier == '%@'", download.url.absoluteString]];
    if (livresAudio.count > 0) {
        
        for (TTLivreAudio *livreAudio in livresAudio) {
            // Durée
            NSString *documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
            NSString *downloadFolder = [documentsPath stringByAppendingPathComponent:@"livres-audio"];
            NSString *downloadFilename = [downloadFolder stringByAppendingPathComponent:[livreAudio.URLFichier lastPathComponent]];
            
            NSError *error = nil;
            AVAudioPlayer* avAudioPlayer = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL URLWithString:downloadFilename] error:&error];
            CGFloat duration = avAudioPlayer.duration;
            avAudioPlayer = nil;
            
            [[RLMRealm defaultRealm] beginWriteTransaction];
            [livreAudio setStatut:(int)LivreAudioDownloaded];
            [livreAudio setDuree:duration];
            [[RLMRealm defaultRealm] commitWriteTransaction];
        }
    }
    
    if ([self isLivreAudioToDownload]) {
        [self startDownload];
    }
   
    if ([_delegate respondsToSelector:@selector(finishedDownloadForURL:)]) {
        [_delegate finishedDownloadForURL:download.url.absoluteString];
    }
}

- (void)downloadManager:(DownloadManager *)downloadManager downloadDidFail:(Download *)download;
{
    DLog(@"Download Failed : %@, FileName : %@", [download error], [download filename]);
    
    if ([_delegate respondsToSelector:@selector(failDownloadForURL:)]) {
        [_delegate failDownloadForURL:download.url.absoluteString];
    }
}


@end
