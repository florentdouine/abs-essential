//
//  WPBibliothequeCell.h
//  ABS-Multimedias
//
//  Created by Virginie Delaitre on 07/04/2015.
//  Copyright (c) 2015 Toutdanslapoche. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WPBibliothequeCell : UITableViewCell {
    
    UILabel *_titleLabel;
    UILabel *_dureeLabel;
    UIImageView *_image;
    
    float _width;
    
    UIImageView *_commentView;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier andWidth:(float)width;
- (void)setTitle:(NSString*)title andDuree:(NSString*)duree andVoiceOver:(NSString*)voiceOver andImage:(NSString *)image andStatut:(LivreAudioStatut)statut;
- (void) setProgress:(NSString*)progress;

@end
