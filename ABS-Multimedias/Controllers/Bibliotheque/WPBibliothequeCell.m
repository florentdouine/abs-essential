//
//  WPBibliothequeCell.m
//  ABS-Multimedias
//
//  Created by Virginie Delaitre on 07/04/2015.
//  Copyright (c) 2015 Toutdanslapoche. All rights reserved.
//

#import "WPBibliothequeCell.h"
#import "UIImageView+WebCache.h"
#import <QuartzCore/QuartzCore.h>

@implementation WPBibliothequeCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier andWidth:(float)width
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _width = width;
        
        // Background
        [self setBackgroundColor:kColorBackground];
        
        // Title
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(kGetFontSize*50+15, 0, self.frame.size.width-100, 45.0*kGetFontSize)];
        [_titleLabel setFont:[UIFont fontWithName:kFontNameBold size:kGetFontSize*kCellTitleHeight]];
        [_titleLabel setTextColor:kColorText];
        [_titleLabel setNumberOfLines:2];
        [_titleLabel setBaselineAdjustment:UIBaselineAdjustmentAlignCenters];
        [_titleLabel setTextAlignment:NSTextAlignmentLeft];
        [_titleLabel setBackgroundColor:[UIColor clearColor]];
        [_titleLabel setAdjustsFontSizeToFitWidth:YES];
        [self addSubview:_titleLabel];
        
        // SubTitle
        _dureeLabel = [[UILabel alloc] initWithFrame:CGRectMake(kGetFontSize*50+15, _titleLabel.frame.size.height-10*kGetFontSize, self.frame.size.width-kGetFontSize*50+15, 35*kGetFontSize)];
        [_dureeLabel setFont:[UIFont fontWithName:kFontNameLight size:kGetFontSize*12.0]];
        [_dureeLabel setTextColor:kColorText];
        [_dureeLabel setBaselineAdjustment:UIBaselineAdjustmentAlignCenters];
        [_dureeLabel setTextAlignment:NSTextAlignmentLeft];
        [_dureeLabel setBackgroundColor:[UIColor clearColor]];
        [_dureeLabel setAdjustsFontSizeToFitWidth:YES];
        [self addSubview:_dureeLabel];
        
        // Image
        [_image removeFromSuperview];
        _image = [[UIImageView alloc] initWithFrame:CGRectMake(kGetFontSize*7.5, kGetFontSize*7.5, kGetFontSize*50.0, 50.0*kGetFontSize)];
        [_image setClipsToBounds:YES];
        [_image setBackgroundColor:[UIColor whiteColor]];
        [_image setContentMode:UIViewContentModeScaleAspectFill];
        [[_image layer] setCornerRadius:kCellArticleCornerRadius];
        [self addSubview:_image];
        
        _commentView = [[UIImageView alloc] initWithFrame:CGRectMake(_image.frame.origin.x + _image.frame.size.width - 10.0, _image.frame.origin.y - 5.0, 15.0*kGetFontSize, 15.0*kGetFontSize)];
        [[_commentView layer] setCornerRadius:7.5*kGetFontSize];
        [_commentView setBackgroundColor:[UIColor redColor]];
        [self addSubview:_commentView];
        
    }
    return self;
}

- (void)setTitle:(NSString*)title andDuree:(NSString*)duree andVoiceOver:(NSString *)voiceOver andImage:(NSString *)imageString andStatut:(LivreAudioStatut)statut {
    
    [_titleLabel setText:title];
    [_dureeLabel setText:duree];
    [_dureeLabel setAccessibilityLabel:voiceOver];
    
    switch (statut) {
        case LivreAudioDownloading:
            [_commentView setBackgroundColor:[UIColor grayColor]];
            break;
        case LivreAudioDownloaded:
            [_commentView setBackgroundColor:[UIColor colorWithRed:182.0/255.0 green:50.0/255.0 blue:1.0/255.0 alpha:1.0]];
            break;
        case LivreAudioListening:
            [_commentView setBackgroundColor:[UIColor colorWithRed:235.0/255. green:144.0/255.0 blue:2.0/255.0 alpha:1.0]];
            break;
        case LivreAudioListen:
            [_commentView setBackgroundColor:[UIColor colorWithRed:38.0/255.0 green:181.0/255.0 blue:2.0/255.0 alpha:1.0]];
            break;
            
        default:
            [_commentView setBackgroundColor:[UIColor colorWithRed:182.0/255.0 green:50.0/255.0 blue:1.0/255.0 alpha:1.0]];
            break;
    }
    
    //    DLog(@"%@", image);
    NSURL *imageURL = [NSURL URLWithString:imageString];
    
    if ([imageString isEqualToString:@""] || imageString == nil || imageURL == nil) {
        [_image setHidden:YES];
        _image.image = nil;
        
        CGFloat x = kGetFontSize*7.5;
        CGFloat commentWidth = 0;
        x += 15.0*kGetFontSize+kGetFontSize*7.5;
        commentWidth = 15.0*kGetFontSize;
        
        // Frame
        [_titleLabel setFrame:CGRectMake(x, 0, _width -2*x - commentWidth, 45.0*kGetFontSize)];
        [_dureeLabel setFrame:CGRectMake(x, _titleLabel.frame.size.height-10*kGetFontSize, _width -2*x -commentWidth, 35*kGetFontSize)];
        [_commentView setFrame:CGRectMake(kGetFontSize*7.5, kGetFontSize*7.5, 15.0*kGetFontSize, 15.0*kGetFontSize)];
        
    } else {
        
        [_image setHidden:NO];
        [_image sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"placeholder.png"] options:SDWebImageHighPriority completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        }];
        
        CGFloat x = kGetFontSize*7.5;
        
        // Frame
        [_titleLabel setFrame:CGRectMake(kGetFontSize*50+kGetFontSize*15, 0, _width-(kGetFontSize*50+kGetFontSize*15)-2*x, 45.0*kGetFontSize)];
        [_dureeLabel setFrame:CGRectMake(kGetFontSize*50+kGetFontSize*15, _titleLabel.frame.size.height-10*kGetFontSize, _width - (kGetFontSize*50+kGetFontSize*15)-2*x, 35*kGetFontSize)];
        [_commentView setFrame:CGRectMake(_image.frame.origin.x + _image.frame.size.width - 10.0, _image.frame.origin.y - kGetFontSize*5.0, 15.0*kGetFontSize, 15.0*kGetFontSize)];
    }
    
    [_image setFrame:CGRectMake(kGetFontSize*7.5, kGetFontSize*7.5, kGetFontSize*50.0, 50.0*kGetFontSize)];
    
    // Font
    [_titleLabel setFont:[UIFont fontWithName:kFontNameBold size:kGetFontSize*kCellTitleHeight]];
    [_dureeLabel setFont:[UIFont fontWithName:kFontNameLight size:kGetFontSize*12.0]];
}

- (void) setProgress:(NSString*)progress {
    [_dureeLabel setText:progress];
    [_dureeLabel setAccessibilityLabel:progress];
}

@end
