//
//  WPBibliothequeViewController.h
//  ABS-Multimedias
//
//  Created by Virginie Delaitre on 07/04/2015.
//  Copyright (c) 2015 Toutdanslapoche. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTDownloadManager.h"
#import <MediaPlayer/MediaPlayer.h>

@class RLMResults;

@protocol WPBibliothequeViewControllerDelegate <NSObject>
@end

@interface WPBibliothequeViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, TTDownloadManagerDelegate, UIAlertViewDelegate> {
    
    UILabel *noResultView;
    NSIndexPath *deletePath;
    
    BOOL videoLoaded;
    UISegmentedControl *control;
    BiblioType _type;
}

@property (nonatomic, strong) id<WPBibliothequeViewControllerDelegate>delegate;
@property (strong, nonatomic) UITableView *aTableView;
@property (nonatomic, strong) RLMResults *livresAudio;
@property (strong, nonatomic) MPMoviePlayerViewController *moviePlayerViewController;
@property (nonatomic, strong) TTLivreAudio *currentListening;

- (id) initWithType:(BiblioType)type andDelegate:(id)delegate;
- (void) reloadViewWithType:(BiblioType)type;

@end
