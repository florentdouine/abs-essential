//
//  WPBibliothequeViewController.m
//  ABS-Multimedias
//
//  Created by Virginie Delaitre on 07/04/2015.
//  Copyright (c) 2015 Toutdanslapoche. All rights reserved.
//

#import "WPBibliothequeViewController.h"
#import "TTLivreAudio.h"
#import "WPBibliothequeCell.h"
#import "UIImageView+WebCache.h"
#import <AVFoundation/AVFoundation.h>
#import "WPAppDelegate.h"

@interface WPBibliothequeViewController ()

@end

@implementation WPBibliothequeViewController
@synthesize aTableView = _aTableView;
@synthesize livresAudio = _livresAudio;
@synthesize moviePlayerViewController = _moviePlayerViewController;
@synthesize currentListening = _currentListening;

- (id)initWithType:(BiblioType)type andDelegate:(id)delegate {
    self = [super init];
    if (self) {
        
        _type = type;
        [self.view setBackgroundColor:kColorBackground];
        // ContentView
        
        UIView *toolbar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44.0f)];
        [toolbar setBackgroundColor:kColorNavBar];
        control = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"Livres Audios", @"Podcasts", nil]];
        [control setFrame:CGRectMake(0, 0, self.view.frame.size.width, 43.0f)];
        [control addTarget:self action:@selector(selectSubview:) forControlEvents:UIControlEventValueChanged];
        if (_type == BiblioLivresAudio) {
            [control setSelectedSegmentIndex:0];
        } else {
            [control setSelectedSegmentIndex:1];
        }
        [control setTintColor:[UIColor whiteColor]];
        
        NSDictionary *attributesSelected = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:kFontNameBold size:kGetFontSize*16.0f], NSFontAttributeName, kColorText, NSForegroundColorAttributeName, nil];
        [control setTitleTextAttributes:attributesSelected forState:UIControlStateSelected];
        
        NSDictionary *attributesUnselected = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*16.0f], NSFontAttributeName, [UIColor whiteColor], NSForegroundColorAttributeName, nil];
        [control setTitleTextAttributes:attributesUnselected forState:UIControlStateNormal];

        [toolbar addSubview:control];
        [[self view] addSubview:toolbar];
        
        CGFloat height = kViewHeight;
        _aTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 44, self.view.frame.size.width, height -44 -44)];
        [_aTableView setBackgroundColor:kColorBackground];
        [_aTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [_aTableView setSeparatorColor:kColorSeparator];
        [_aTableView setDelegate:self];
        [_aTableView setDataSource:self];
        [_aTableView setShowsVerticalScrollIndicator:NO];
        [_aTableView setAccessibilityLabel:@"Chargement"];
        [self.view addSubview:_aTableView];
        
        _livresAudio = [[TTLivreAudio objectsWhere:[NSString stringWithFormat:@"type == %i && isDownload == YES", type]] sortedResultsUsingProperty:@"titre" ascending:YES];
        
        // Vue pas de résultats
        noResultView = [[UILabel alloc] initWithFrame:_aTableView.frame];
        [noResultView setBackgroundColor:[UIColor clearColor]];
        [noResultView setBaselineAdjustment:UIBaselineAdjustmentAlignCenters];
        [noResultView setNumberOfLines:2];
        [noResultView setText:@"Vous n'avez pas encore\ntéléchargé de livre audio."];
        [noResultView setTextAlignment:NSTextAlignmentCenter];
        [noResultView setTextColor:kColorNextArticles];
        [noResultView setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*14.0]];
        [self.view addSubview:noResultView];
        [noResultView setHidden:YES];
        
        // Cas où le resultat est vide
        if (_livresAudio.count == 0) {
            if (!UIAccessibilityIsVoiceOverRunning()) {
                [noResultView setHidden:NO];
            }
            [_aTableView setAccessibilityLabel:@"Vous n'avez pas encore téléchargé de livre audio."];
            UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _aTableView.frame.size.width, _aTableView.frame.size.height)];
            [_aTableView setTableFooterView:footer];
            [_aTableView setScrollEnabled:NO];
        } else {
            [noResultView setHidden:YES];
            CGFloat height = _aTableView.frame.size.height-_livresAudio.count*kGetFontSize*65;
            if (height <= 0) {
                height = 0;
                [_aTableView setScrollEnabled:YES];
            } else {
                [_aTableView setScrollEnabled:NO];
            }
            UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _aTableView.frame.size.width, height)];
            [_aTableView setTableFooterView:footer];
            [_aTableView setAccessibilityLabel:@"Chargement"];
        }
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [[TTDownloadManager shared] setDelegate:self];
    
    // Google Analytics
    id<GAITracker> googleTracker = [[GAI sharedInstance] trackerWithTrackingId:kTrackingID];
    [googleTracker set:kGAIScreenName value:@"Bibliothèque"];
    [googleTracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

#pragma mark - Load

- (void) reloadView {
    [self reloadViewWithType:_type];
}

- (void) reloadViewWithType:(BiblioType)type {
    
    _type = type;
    
    if (_type == BiblioLivresAudio) {
        [control setSelectedSegmentIndex:0];
    } else {
        [control setSelectedSegmentIndex:1];
    }
    
    NSDictionary *attributesSelected = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:kFontNameBold size:kGetFontSize*16.0f], NSFontAttributeName, kColorText, NSForegroundColorAttributeName, nil];
    [control setTitleTextAttributes:attributesSelected forState:UIControlStateSelected];
    
    NSDictionary *attributesUnselected = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*16.0f], NSFontAttributeName, [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    [control setTitleTextAttributes:attributesUnselected forState:UIControlStateNormal];
    [noResultView setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*14.0]];
    
    //
    _livresAudio = [[TTLivreAudio objectsWhere:[NSString stringWithFormat:@"type == %i && isDownload == YES", type]] sortedResultsUsingProperty:@"titre" ascending:YES];
    [_aTableView reloadData];
    
    
    // Cas où le resultat est vide
    if (_livresAudio.count == 0) {
        if (!UIAccessibilityIsVoiceOverRunning()) {
            [noResultView setHidden:NO];
        }
        switch (type) {
            case BiblioPodcasts:
                [noResultView setText:@"Vous n'avez pas encore\ntéléchargé de podcast."];
                [_aTableView setAccessibilityLabel:@"Vous n'avez pas encore téléchargé de podcast."];
                break;
            case BiblioLivresAudio:
                [noResultView setText:@"Vous n'avez pas encore\ntéléchargé de livre audio."];
                [_aTableView setAccessibilityLabel:@"Vous n'avez pas encore téléchargé de livre audio."];
                break;
                
            default:
                break;
        }
        UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _aTableView.frame.size.width, _aTableView.frame.size.height)];
        [_aTableView setTableFooterView:footer];
        [_aTableView setScrollEnabled:NO];
    } else {
        [noResultView setHidden:YES];
        CGFloat height = _aTableView.frame.size.height-_livresAudio.count*kGetFontSize*65;
        if (height <= 0) {
            height = 0;
            [_aTableView setScrollEnabled:YES];
        } else {
            [_aTableView setScrollEnabled:NO];
        }
        UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _aTableView.frame.size.width, height)];
        [_aTableView setTableFooterView:footer];
        [_aTableView setAccessibilityLabel:@"Chargement"];
    }
    
    [[TTDownloadManager shared] setDelegate:self];
}

#pragma mark - Actions

- (void) selectSubview:(UISegmentedControl*)segmentedControl {
    
    switch (segmentedControl.selectedSegmentIndex) {
        case 0:
            _type = BiblioLivresAudio;
            break;
        case 1:
            _type = BiblioPodcasts;
            break;
            
        default:
            break;
    }
    [self reloadView];
}

#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kGetFontSize*65.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return ([(NSArray*)_livresAudio count]);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TTLivreAudio *livreAudio = [_livresAudio objectAtIndex:indexPath.row];
    
    static NSString *CellIdentifier = @"BiblioCell";
    WPBibliothequeCell *cell = (WPBibliothequeCell*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[WPBibliothequeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier andWidth:self.view.frame.size.width];
    }

    int minutes = (int)(livreAudio.duree/60);
    float secondes = livreAudio.duree -60*minutes;
    
    NSMutableString *duree = [NSMutableString stringWithFormat:@"%imin", minutes];
    if (secondes <= 0) {
        secondes = 0;
    } else {
        [duree appendFormat:@" %is", (int)secondes];
    }
    
    int minutesC = (int)(livreAudio.current/60);
    
    NSMutableString *current = [NSMutableString stringWithFormat:@"%imin", minutesC];
    float secondesC = livreAudio.current -60*minutesC;
    if (secondesC <= 0) {
        secondesC = 0;
    } else {
        [current appendFormat:@" %is", (int)secondesC];
    }
    
    NSString *time = [NSString stringWithFormat:@"%@ / %@", current, duree];
    if (minutesC <= 0 || [duree isEqualToString:current]) {
        time = [NSString stringWithFormat:@"%@", duree];
    }
    
    // Voice Over
    NSString *dureeVO = [[duree stringByReplacingOccurrencesOfString:@"s" withString:@" secondes"] stringByReplacingOccurrencesOfString:@"min" withString:@" minutes"];
    NSString *currentVO = [NSString stringWithFormat:@"%@", [[current stringByReplacingOccurrencesOfString:@"s" withString:@" secondes"] stringByReplacingOccurrencesOfString:@"min" withString:@" minutes"]];
    
    NSString *timeVO = [NSString stringWithFormat:@"%@ sur %@", currentVO, dureeVO];
    if (minutesC <= 0 || [dureeVO isEqualToString:currentVO]) {
        timeVO = [NSString stringWithFormat:@"%@", dureeVO];
    }
    
    switch (livreAudio.statut) {
        case LivreAudioDownloading:
            timeVO = [NSString stringWithFormat:@"%@", timeVO];
            break;
        case LivreAudioDownloaded:
            timeVO = [NSString stringWithFormat:@"Non lu %@", timeVO];
            break;
        case LivreAudioListening:
            timeVO = [NSString stringWithFormat:@"Partiellement lu %@", timeVO];
            break;
        case LivreAudioListen:
            timeVO = [NSString stringWithFormat:@"Lu %@", timeVO];
            break;
            
        default:
            break;
    }
    
    if (livreAudio.statut == LivreAudioDownloading) {
        timeVO = @"Téléchargement en attente";
        time = @"Téléchargement en attente";
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    [cell setAccessoryType:UITableViewCellAccessoryNone];
    [cell setTitle:livreAudio.titre andDuree:time andVoiceOver:timeVO andImage:livreAudio.imageURL andStatut:livreAudio.statut];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    TTLivreAudio *livreAudio = [_livresAudio objectAtIndex:indexPath.row];
    // Lire fichier audio
    NSString *documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *downloadFolder = [documentsPath stringByAppendingPathComponent:@"livres-audio"];
    NSString *downloadFilename = [downloadFolder stringByAppendingPathComponent:[livreAudio.URLFichier lastPathComponent]];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:downloadFilename]) {
        
        _currentListening = livreAudio;
        _moviePlayerViewController = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL fileURLWithPath:downloadFilename]];
        
        [[NSNotificationCenter defaultCenter] removeObserver:_moviePlayerViewController
                                                        name:MPMoviePlayerPlaybackDidFinishNotification
                                                      object:_moviePlayerViewController.moviePlayer];

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(movieFinishedCallback:)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:_moviePlayerViewController.moviePlayer];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieLoadStateDidChange:) name:MPMoviePlayerLoadStateDidChangeNotification object:nil];
        videoLoaded = NO;
        
        [[_moviePlayerViewController moviePlayer] prepareToPlay];
        [_moviePlayerViewController.moviePlayer setControlStyle:MPMovieControlStyleFullscreen];
        [_moviePlayerViewController.moviePlayer setShouldAutoplay:YES];
        [_moviePlayerViewController.moviePlayer setAllowsAirPlay:YES];
        [_moviePlayerViewController.moviePlayer setFullscreen:YES animated:YES];
        [_moviePlayerViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
        [_moviePlayerViewController.moviePlayer setScalingMode:MPMovieScalingModeNone];
        [_moviePlayerViewController.moviePlayer setIsAccessibilityElement:YES];
        _moviePlayerViewController.moviePlayer.movieSourceType = MPMovieSourceTypeFile;
        
        [[_moviePlayerViewController moviePlayer] play];
        [MyAppDelegate.window.rootViewController presentMoviePlayerViewControllerAnimated:_moviePlayerViewController];
        
        // Background audio title
        NSDictionary *info = @{ MPMediaItemPropertyTitle: livreAudio.titre,
                                MPMediaItemPropertyArtist: @"ABS Essential",
                                MPMediaItemPropertyPlaybackDuration: [NSNumber numberWithFloat:livreAudio.duree]};
        [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = info;
        [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    }
    
}

- (void)movieFinishedCallback:(NSNotification*)aNotification
{
    TTLivreAudio *livreAudio = _currentListening;
    
    // Sauvegarder avancement du livre audio
    [[RLMRealm defaultRealm] beginWriteTransaction];
    if (livreAudio.duree > _moviePlayerViewController.moviePlayer.currentPlaybackTime) {
        [livreAudio setStatut:(int)LivreAudioListening];
    } else {
        [livreAudio setStatut:(int)LivreAudioListen];
    }
    [livreAudio setCurrent:_moviePlayerViewController.moviePlayer.currentPlaybackTime];
    [[RLMRealm defaultRealm] commitWriteTransaction];
    
    NSNumber *finishReason = [[aNotification userInfo] objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    if ([finishReason intValue] != MPMovieFinishReasonPlaybackEnded) {
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:MPMoviePlayerPlaybackDidFinishNotification
                                                      object:_moviePlayerViewController.moviePlayer];
        
        [MyAppDelegate.window.rootViewController dismissViewControllerAnimated:YES completion:^{
            _currentListening = nil;
            [self reloadView];
        }];
    }
}

- (void)movieLoadStateDidChange:(NSNotification *)notification {
    MPMoviePlayerController *player = notification.object;
    MPMovieLoadState loadState = player.loadState;
    
    /* Enough data has been buffered for playback to continue uninterrupted. */
    if (loadState & MPMovieLoadStatePlaythroughOK)
    {
        //should be called only once , so using self.videoLoaded - only for  first time  movie loaded , if required. This function wil be called multiple times after stalled
        if(!videoLoaded)
        {
            [[_moviePlayerViewController moviePlayer] setCurrentPlaybackTime:_currentListening.current];
            videoLoaded = YES;
        }
    }
}

#pragma mark - UITableView Delete

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        deletePath = indexPath;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Supprimer" message:@"Êtes-vous sûr de vouloir supprimer ce livre audio de votre bibliothèque ?" delegate:self cancelButtonTitle:@"Annuler" otherButtonTitles:@"Oui", nil];
        [alert setTag:1];
        [alert show];
    }
}


#pragma mark Delete Slide

- (NSArray*)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:[self tableView:tableView titleForDeleteConfirmationButtonForRowAtIndexPath:indexPath] handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [self tableView:tableView commitEditingStyle:UITableViewCellEditingStyleDelete forRowAtIndexPath:indexPath];
    }];
    deleteAction.backgroundColor = [UIColor colorWithRed:182.0/255.0 green:50.0/255.0 blue:1.0/255.0 alpha:1.0];
    return @[deleteAction];
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"Supprimer";
}

- (void)tableView:(UITableView *)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!IS_IOS8) {
        [tableView reloadData];
    }
}

#pragma mark - UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 1) {
        
        if (buttonIndex == 1) {
            TTLivreAudio *livreAudio = [_livresAudio objectAtIndex:deletePath.row];
            
            NSString *documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
            NSString *downloadFolder = [documentsPath stringByAppendingPathComponent:@"livres-audio"];
            NSString *downloadFilename = [downloadFolder stringByAppendingPathComponent:[livreAudio.URLFichier lastPathComponent]];
            if ([[NSFileManager defaultManager] fileExistsAtPath:downloadFilename]) {
                [[NSFileManager defaultManager] removeItemAtPath:downloadFilename error:nil];
            }
            
            RLMRealm *realm = [RLMRealm defaultRealm];
            [realm beginWriteTransaction];
            [realm deleteObject:livreAudio];
            [realm commitWriteTransaction];
            _livresAudio = [[TTLivreAudio objectsWhere:[NSString stringWithFormat:@"type == %i && isDownload == YES", _type]] sortedResultsUsingProperty:@"titre" ascending:YES];
            
            [_aTableView beginUpdates];
            [_aTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:deletePath] withRowAnimation:UITableViewRowAnimationLeft];
            
            // [_aTableView performSelector:@selector(reloadData) withObject:nil afterDelay:0.3];
            [_aTableView endUpdates];
            
            if (_livresAudio.count == 0) {
                if (!UIAccessibilityIsVoiceOverRunning()) {
                    [noResultView setHidden:NO];
                }
                [_aTableView setAccessibilityLabel:@"Vous n'avez pas encore téléchargé de livre audio."];
                UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _aTableView.frame.size.width, _aTableView.frame.size.height)];
                [_aTableView setTableFooterView:footer];
                [_aTableView setScrollEnabled:NO];
            } else {
                [noResultView setHidden:YES];
                CGFloat height = _aTableView.frame.size.height-_livresAudio.count*kGetFontSize*65;
                if (height <= 0) {
                    height = 0;
                    [_aTableView setScrollEnabled:YES];
                } else {
                    [_aTableView setScrollEnabled:NO];
                }
                UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _aTableView.frame.size.width, height)];
                [_aTableView setTableFooterView:footer];
                [_aTableView setAccessibilityLabel:@"Chargement"];
            }
            return;
        } else {
            [_aTableView setEditing:NO animated:YES];
        }
    }
}

- (void)viewDidLayoutSubviews {
    [_aTableView setSeparatorInset:UIEdgeInsetsZero];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(WPBibliothequeCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]){
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero]; // ios 8 newly added
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - Download Manager Delegate

- (void)progress:(float)progress downloadForURL:(NSString *)URLStr {
    int i = 0;
    for (TTLivreAudio *livreAudio in _livresAudio) {
        if ([livreAudio.URLFichier isEqualToString:URLStr]) {
            WPBibliothequeCell *cell = (WPBibliothequeCell*)[_aTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
            [cell setProgress:[NSString stringWithFormat:@"Téléchargement en cours %.f%%", progress]];
            [cell reloadInputViews];
        }
        i++;
    }
}

- (void)finishedDownloadForURL:(NSString *)URLStr {
    [self reloadView];
}

@end
