//
//  WPFavorisViewController.h
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WPFavorisViewControllerDelegate <NSObject>
- (void) openArticleViewWithArticle:(TTPost*)item andType:(ArticleType)type ;
@end

@interface WPFavorisViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    
    UILabel *noResultView;
}

@property (nonatomic, weak) id<WPFavorisViewControllerDelegate>delegate;
@property (strong, nonatomic) UITableView *aTableView;
@property (strong, nonatomic) NSArray *articles;

- (id)initWithDelegate:(id)delegate;
- (void) reloadFavorisDatas;

@end