//
//  WPFavorisViewController.m
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import "WPFavorisViewController.h"
#import "WPPlistRecup.h"
#import "WPArticleCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation WPFavorisViewController
@synthesize delegate = _delegate;
@synthesize aTableView = _aTableView;
@synthesize articles = _articles;

- (id)initWithDelegate:(id)delegate
{
    self = [super init];
    if (self) {
        
        _delegate = delegate;
        [self.view setBackgroundColor:kColorBackground];
        
        [self setTitle:@"Mes Favoris"];

        _aTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 44.0)];
        [_aTableView setBackgroundColor:kColorBackground];
        [_aTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [_aTableView setSeparatorColor:kColorSeparator];
        [_aTableView setDelegate:self];
        [_aTableView setDataSource:self];
        [_aTableView setShowsVerticalScrollIndicator:NO];
        [_aTableView setAccessibilityLabel:@"Aucun favoris"];
        [self.view addSubview:_aTableView];
        
        // Vue pas de résultats
        noResultView = [[UILabel alloc] initWithFrame:_aTableView.frame];
        [noResultView setBackgroundColor:[UIColor clearColor]];
        [noResultView setBaselineAdjustment:UIBaselineAdjustmentAlignCenters];
        [noResultView setNumberOfLines:2];
        [noResultView setText:@"Vous n'avez pas de favoris."];
        [noResultView setTextAlignment:NSTextAlignmentCenter];
        [noResultView setTextColor:kColorNextArticles];
        [noResultView setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*14.0]];
        [self.view addSubview:noResultView];
        
        if ([_articles count] != 0) {
            [noResultView setHidden:YES];
        } else {
            [noResultView setHidden:NO];
        }
        
        // Footer
        UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, _aTableView.frame.size.height - _articles.count*60.0)];
        [footer setBackgroundColor:kColorBackground];
        [_aTableView setTableFooterView:footer];
        
        _articles = [NSArray new];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self reloadFavorisDatas];
    
    // Google Analytics
    id<GAITracker> googleTracker = [[GAI sharedInstance] trackerWithTrackingId:kTrackingID];
    [googleTracker set:kGAIScreenName value:@"Mes Favoris"];
    [googleTracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

#pragma mark - Reload

- (void)reloadFavorisDatas {
    
    _articles = [[WPPlistRecup shared] getListOfFavoris];
    [_aTableView reloadData];
    
    if ([_articles count] != 0) {
        [noResultView setHidden:YES];
    } else {
        [noResultView setHidden:NO];
    }
    [noResultView setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*14.0]];
}

#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kGetFontSize*65.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [(NSArray*)_articles count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TTPost *item = [_articles objectAtIndex:indexPath.row];
    
    static NSString *CellIdentifier = @"FavorisCell";
    WPArticleCell *cell = (WPArticleCell*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[WPArticleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier andWidth:self.view.frame.size.width];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    [cell setPost:item];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    TTPost *item = [_articles objectAtIndex:indexPath.row];
    
    if ([_delegate respondsToSelector:@selector(openArticleViewWithArticle:andType:)]) {
        [_delegate openArticleViewWithArticle:item andType:ArticleFavoris];
    }
}

// No separator inset !
- (void)viewDidLayoutSubviews {
    [_aTableView setSeparatorInset:UIEdgeInsetsZero];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(WPArticleCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]){
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero]; // ios 8 newly added
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

@end