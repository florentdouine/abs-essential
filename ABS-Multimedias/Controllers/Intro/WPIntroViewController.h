//
//  WPIntroViewController.h
//  ABS Essential
//
//  Created by Virginie Delaitre on 20/08/2015.
//  Copyright (c) 2015 Toutdanslapoche. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WPIntroViewControllerDelegate <NSObject>
- (void) closeIntro;
- (void) selectTabBarItem:(int)tag;
@end

@interface WPIntroViewController : UIViewController <UIScrollViewDelegate> {
    
    UIPageControl *_pageControl;
    NSMutableArray *accessibilityElts;
}

@property (nonatomic, weak) id<WPIntroViewControllerDelegate>delegate;
@property (nonatomic, strong) UIScrollView *scroll;

- (id) initWithFrame:(CGRect)frame andDelegate:(id)delegate;

@end
