//
//  WPIntroViewController.m
//  ABS Essential
//
//  Created by Virginie Delaitre on 20/08/2015.
//  Copyright (c) 2015 Toutdanslapoche. All rights reserved.
//

#import "WPIntroViewController.h"

@interface WPIntroViewController ()

@end

@implementation WPIntroViewController
@synthesize delegate = _delegate;
@synthesize scroll = _scroll;

- (id)initWithFrame:(CGRect)frame andDelegate:(id)delegate {
    self = [super init];
    if (self) {
        
        _delegate = delegate;
        
        [[self view] setFrame:frame];
        [[self view] setBackgroundColor:[UIColor clearColor]];
        
        NSArray *visite = @[@"Bienvenue dans ABS Essential, l’essentiel de l'organisation ABS-Multimédias.\n\nCette application vous donne accès à notre actualité, ainsi qu’à notre catalogue complet de livres audio via la librairie intégrée.\nÉcoutez et téléchargez nos livres audio, nos podcasts, lisez nos articles culture et bien plus...\n\nAccueil :\n\nD’un coup d’œil, vous avez un aperçu de nos dernières actualités...\nLisez, écoutez, commentez, partagez, ajoutez à vos favoris...",
                            @"Navigation :\n\nNaviguez par rubriques, allez directement à ce qui vous intéresse...\nLes podcasts (Genius, Des lettres et des mots), les articles d’actus, les énigmes, Vos p'tits papiers, et bien plus...",
                            @"Librairie :\n\nTout notre catalogue de livres audio à porté de doigts !\nÉcoutez, téléchargez, achetez, tout ça, sans quitter votre Smartphone !",
                            @"Bibliothèque :\n\nStockez vos podcasts favoris directement sur votre Smartphone, écoutez ultérieurement vos livres audio préférés... La bibliothèque c’est votre espace !",
                            @"Annuaire :\n\nTout plein de surprises !\nAccédez au plus grand annuaire de livres audio francophone directement depuis votre Smartphone... Les dernières parutions chez les éditeurs partenaires... Un nouveau livre vient de sortir en version papier ? Vérifiez si il existe en livre audio... Et ce n’est qu’un début...\nCet onglet vous réserve et réservera tout plein d’autres surprises.\n\nCommencez maintenant !"];
        
        // Visite guidée
        _scroll = [[UIScrollView alloc] initWithFrame:frame];
        [_scroll setBackgroundColor:[UIColor clearColor]];
        [_scroll setShowsHorizontalScrollIndicator:NO];
        [_scroll setPagingEnabled:YES];
        [_scroll setDelegate:self];
        [[self view] addSubview:_scroll];
        
        _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, 20+20, self.view.frame.size.width, 20.0)];
        [_pageControl setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter],
        [_pageControl setCurrentPage:1];
        [_pageControl setNumberOfPages:5];
        [_pageControl addTarget:self action:@selector(changePage:) forControlEvents:UIControlEventValueChanged];
        [_pageControl setCurrentPageIndicatorTintColor:kColorNavBar];
        [_pageControl setPageIndicatorTintColor:[UIColor colorWithWhite:0 alpha:0.1]];
        [_pageControl setAccessibilityElementsHidden:YES];
        [self.view addSubview:_pageControl];
        
        accessibilityElts = [NSMutableArray array];
        
        for (int i = 0; i<5; i++) {
            
            // Label
            UIView *fondView  = [[UIView alloc] initWithFrame:CGRectMake(i*_scroll.frame.size.width, 20, self.view.frame.size.width, self.view.frame.size.height-49-20)];
            [fondView setBackgroundColor:[UIColor whiteColor]];
            [_scroll addSubview:fondView];
            
            NSArray *arr = [(NSString*)[visite objectAtIndex:i] componentsSeparatedByString:@"\n\n"];
            NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:[arr objectAtIndex:0] attributes:@{NSFontAttributeName : [UIFont fontWithName:kFontNameBold size:kGetFontSize*16],
                                                                                                                                      NSForegroundColorAttributeName : [UIColor blackColor]}];
            [attrStr appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n\n" attributes:@{}]];
            [attrStr appendAttributedString:[[NSAttributedString alloc] initWithString:[arr objectAtIndex:1] attributes:@{NSFontAttributeName : [UIFont fontWithName:kFontNameRegular size:kGetFontSize*14],
                                                                                                                          NSForegroundColorAttributeName : [UIColor blackColor]}]];
            if (arr.count >2) {
                [attrStr appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n\n" attributes:@{}]];
                [attrStr appendAttributedString:[[NSAttributedString alloc] initWithString:[arr objectAtIndex:2] attributes:@{NSFontAttributeName : [UIFont fontWithName:kFontNameBold size:kGetFontSize*16],
                                                                                                                        NSForegroundColorAttributeName : [UIColor blackColor]}]];
            }
            if (arr.count >3) {
                [attrStr appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n\n" attributes:@{}]];
                [attrStr appendAttributedString:[[NSAttributedString alloc] initWithString:[arr objectAtIndex:3] attributes:@{NSFontAttributeName : [UIFont fontWithName:kFontNameRegular size:kGetFontSize*14],
                                                                                                                              NSForegroundColorAttributeName : [UIColor blackColor]}]];
            }
            
            UIScrollView *textScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(10, 10+64-20, _scroll.frame.size.width-20, 250)];
            [textScroll setShowsVerticalScrollIndicator:NO];
            [fondView addSubview:textScroll];
            
            UILabel *texteLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, _scroll.frame.size.width-20, 250)];
            [texteLbl setNumberOfLines:0];
            [texteLbl setFont:[UIFont fontWithName:kFontNameBold size:15]];
            [texteLbl setAttributedText:attrStr];
            [texteLbl setIsAccessibilityElement:YES];
            [textScroll addSubview:texteLbl];
            [texteLbl sizeToFit];
            
            [textScroll setContentSize:CGSizeMake(textScroll.frame.size.width, texteLbl.frame.size.height+10)];
            if (textScroll.contentSize.height <= texteLbl.frame.size.height) {
                [textScroll setScrollEnabled:NO];
            }
            
            NSString *imgArrow = @"arrow_left";
            if (i>2) {
                imgArrow = @"arrow_right";
            }
            UIImageView *arrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgArrow]];
            [arrow setFrame:CGRectMake(i*_scroll.frame.size.width+20+((self.view.frame.size.width-10)/5)*i, _scroll.frame.size.height-49-45, 42, 42)];
            [_scroll addSubview:arrow];
            
            // Button
            UIButton *nextBtn = [[UIButton alloc] initWithFrame:CGRectMake(i*_scroll.frame.size.width+(_scroll.frame.size.width-200)/2, self.view.frame.size.height-49-100, 200, 40)];
            [nextBtn setBackgroundColor:kColorNextArticles];
            if (i == 4) {
                [nextBtn setTitle:@"Commencer" forState:UIControlStateNormal];
            } else {
                [nextBtn setTitle:@"Suivant" forState:UIControlStateNormal];
            }
            [nextBtn setTitleColor:kColorTextLight forState:UIControlStateNormal];
            [nextBtn setTitleColor:kColorText forState:UIControlStateHighlighted];
            [nextBtn setContentMode:UIViewContentModeCenter];
            [nextBtn setTag:i];
            [[nextBtn titleLabel] setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*17.0]];
            [nextBtn addTarget:self action:@selector(goToNext:) forControlEvents:UIControlEventTouchUpInside];
            [_scroll addSubview:nextBtn];
            
            [accessibilityElts addObject:texteLbl];
        }
        
        [_scroll setContentSize:CGSizeMake(_scroll.frame.size.width*5, _scroll.frame.size.height)];
        [[WPPlistRecup shared] setReglageSon:NO];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if ([_delegate respondsToSelector:@selector(selectTabBarItem:)]) {
        [_delegate selectTabBarItem:0];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (void) goToNext:(UIButton*)sender {
    CGFloat width = (sender.tag+1)*_scroll.frame.size.width;
    if (width >= _scroll.contentSize.width) {
        [self closeIntro];
    } else {
        [_scroll scrollRectToVisible:CGRectMake(width, 0, _scroll.frame.size.width, _scroll.frame.size.height) animated:YES];
        int page = width/_scroll.frame.size.width;
        [_pageControl setCurrentPage:page];
        UIAccessibilityPostNotification(UIAccessibilityScreenChangedNotification, [accessibilityElts objectAtIndex:(sender.tag+1)]);
    
        // Select TabBar
        if ([_delegate respondsToSelector:@selector(selectTabBarItem:)]) {
            [_delegate selectTabBarItem:page];
        }
    }
}

- (void) closeIntro {
    [[WPPlistRecup shared] setReglageSon:YES];
    if ([_delegate respondsToSelector:@selector(closeIntro)]) {
        [_delegate closeIntro];
    }
}

#pragma mark - ScrollView Delegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    int page = scrollView.contentOffset.x/scrollView.frame.size.width;
    [_pageControl setCurrentPage:page];
    UIAccessibilityPostNotification(UIAccessibilityScreenChangedNotification, [accessibilityElts objectAtIndex:page]);
    
    // Select TabBar
    if ([_delegate respondsToSelector:@selector(selectTabBarItem:)]) {
        [_delegate selectTabBarItem:page];
    }
}

- (void) changePage:(UIPageControl*)pageControl {
    [_scroll scrollRectToVisible:CGRectMake(pageControl.currentPage*_scroll.frame.size.width, 0, _scroll.frame.size.width, _scroll.frame.size.height) animated:YES];
}

@end
