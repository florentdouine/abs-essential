//
//  WPRechercheViewController.h
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WPAPIRequest.h"
#import "WPDetailArticleViewController.h"
#import <AVFoundation/AVFoundation.h>

@protocol WPRechercheViewControllerDelegate <NSObject>
- (void) openArticleViewWithArticle:(TTPost*)item andType:(ArticleType)type;
- (void) openBibliothequeWithType:(BiblioType)type;
@end

@interface WPRechercheViewController : UIViewController <UITextFieldDelegate, WPAPIRequestDelegate, UITableViewDataSource, UITableViewDelegate, WPDetailArticleViewControllerDelegate> {
    
    BOOL _isSearch;
    UILabel *noResultView;
    UIView *_chargementView;
}

@property (strong, nonatomic) id <WPRechercheViewControllerDelegate> delegate;
@property (strong, nonatomic) UITextField *rechercheField;
@property (strong, nonatomic) NSArray *articles;
@property (strong, nonatomic) UITableView *aTableView;
@property (strong, nonatomic) WPDetailArticleViewController *detailArticleController;

/* AUDIO */
@property (nonatomic, strong) AVAudioPlayer *soundPlayer;

- (id) initWithFrame:(CGRect)frame andDelegate:(id)delegate;

@end