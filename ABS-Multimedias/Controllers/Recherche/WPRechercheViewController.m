//
//  WPRechercheViewController.m
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import "WPRechercheViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "WPArticleCell.h"

@implementation WPRechercheViewController
@synthesize rechercheField = _rechercheField;
@synthesize aTableView = _aTableView;
@synthesize articles = _articles;
@synthesize detailArticleController = _detailArticleController;
@synthesize soundPlayer = _soundPlayer;

- (id)initWithFrame:(CGRect)frame andDelegate:(id)delegate
{
    self = [super init];
    if (self) {
        
        _isSearch = NO;
        _delegate = delegate;
        
        [[self view] setFrame:frame];
        [[self view] setBackgroundColor:kColorBackground];
        
        // UIButton
        /*UIButton *rechercheCustomButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
        [rechercheCustomButton setBackgroundImage:[UIImage imageNamed:@"button_search_off.png"] forState:UIControlStateNormal];
        [rechercheCustomButton setBackgroundImage:[UIImage imageNamed:@"button_search_on.png"] forState:UIControlStateHighlighted];
        [rechercheCustomButton setBackgroundImage:[UIImage imageNamed:@"button_search_on.png"] forState:UIControlStateSelected];
        [rechercheCustomButton addTarget:self action:@selector(lancerLaRecherche) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *rechercheButton = [[UIBarButtonItem alloc] initWithCustomView:rechercheCustomButton];
        [self.navigationItem setRightBarButtonItem:rechercheButton];*/
        
        UIButton *retourCustomButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
        [retourCustomButton setBackgroundImage:[UIImage imageNamed:@"button_retour_off.png"] forState:UIControlStateNormal];
        [retourCustomButton setBackgroundImage:[UIImage imageNamed:@"button_retour_on.png"] forState:UIControlStateHighlighted];
        [retourCustomButton addTarget:self action:@selector(closeTheView) forControlEvents:UIControlEventTouchUpInside];
        [retourCustomButton setAccessibilityLabel:@"Retour"];
        UIBarButtonItem *retourButton = [[UIBarButtonItem alloc] initWithCustomView:retourCustomButton];
        [self.navigationItem setLeftBarButtonItem:retourButton];
        
        // Recherche
        _rechercheField = [[UITextField alloc] initWithFrame:CGRectMake(37,7,self.view.frame.size.width-74,31)];
        _rechercheField.delegate = self;
        _rechercheField.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _rechercheField.textColor = [UIColor blackColor];
        _rechercheField.textAlignment = NSTextAlignmentLeft;
        _rechercheField.borderStyle = UITextBorderStyleRoundedRect;

        _rechercheField.backgroundColor = [UIColor whiteColor];
        _rechercheField.font = [UIFont fontWithName:kFontNameRegular size:kGetFontSize*14];
        _rechercheField.autocorrectionType = UITextAutocorrectionTypeNo;
        _rechercheField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        [_rechercheField setPlaceholder:@"Entrez votre recherche"];
        [_rechercheField setReturnKeyType:UIReturnKeySearch];
        [_rechercheField setClearButtonMode:UITextFieldViewModeAlways];
        
        [[UITextField appearance] setTintColor:kColorNavBar];
        [self.navigationItem setTitleView:_rechercheField];
        [_rechercheField becomeFirstResponder];
        
        // UITableView
        _aTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 44.0)];
        [_aTableView setBackgroundColor:kColorBackground];
        [_aTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [_aTableView setSeparatorColor:[UIColor colorWithRed:159.0/255.0 green:159.0/255.0 blue:159.0/255.0 alpha:1.0]];
        [_aTableView setDelegate:self];
        [_aTableView setDataSource:self];
        [_aTableView setShowsVerticalScrollIndicator:NO];
        [_aTableView setAccessibilityLabel:@"Pas de résultats"];
        [self.view addSubview:_aTableView];
        
        UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _aTableView.frame.size.width, _aTableView.frame.size.height)];
        [_aTableView setTableFooterView:footer];
        
        // Vue pas de résultats
        noResultView = [[UILabel alloc] initWithFrame:_aTableView.frame];
        [noResultView setBackgroundColor:[UIColor clearColor]];
        [noResultView setBaselineAdjustment:UIBaselineAdjustmentAlignCenters];
        [noResultView setNumberOfLines:2];
        [noResultView setText:@"Aucun article ne correspond\nà votre recherche."];
        [noResultView setTextAlignment:NSTextAlignmentCenter];
        [noResultView setTextColor:kColorNextArticles];
        [noResultView setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*14.0]];
        [self.view addSubview:noResultView];
        [noResultView setHidden:YES];
        
        // Load
        _chargementView = [[TTChargementView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];;
        [[self view] addSubview:_chargementView];
        [_chargementView setHidden:YES];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [_rechercheField resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (void) lancerLaRecherche {
    
    if (_rechercheField.text == nil || [_rechercheField.text isEqualToString:@""] || _isSearch) {
        return;
    }
    
    // Nouvelle recherche
    _isSearch = YES;
    [noResultView setHidden:YES];
    [noResultView setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*14.0]];
    
    [_rechercheField resignFirstResponder];
    [_rechercheField setUserInteractionEnabled:NO];
    
    // Chargement
    [_chargementView setHidden:NO];
    
    // Lancer la requête sur le serveur
    [[WPAPIRequest shared] makeSearchWithString:_rechercheField.text withDelegate:self];
}

- (void) closeTheView {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    //    [_rechercheField resignFirstResponder];
    [self lancerLaRecherche];
    return YES;
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    // Google Analytics
    id<GAITracker> googleTracker = [[GAI sharedInstance] trackerWithTrackingId:kTrackingID];
    [googleTracker set:kGAIScreenName value:@"Recherche"];
    [googleTracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

#pragma mark - APIDelegate

- (void)finishedLoadSearchWithStatut:(RequestStatut)aStatut andJSONArray:(NSArray *)aJSON {
    
    if (aStatut == RequestStatutFailed) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur de chargement" message:@"Un problème est survenu lors du chargement des articles. Vérifiez votre connexion internet et essayez de nouveau votre recherche." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        // Fin du chargement
        [_chargementView setHidden:YES];
        
        [_rechercheField setUserInteractionEnabled:YES];
        _isSearch = NO;
        return;
    }
    
    [_rechercheField setText:@""];
    _articles = aJSON;
    
    // Cas où le resultat est vide
    if (_articles.count == 0) {
        [noResultView setHidden:NO];
        UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _aTableView.frame.size.width, _aTableView.frame.size.height)];
        [_aTableView setTableFooterView:footer];
    }
    
    // Cas où le resultat n'est pas vide
    [_aTableView reloadData];
    [_aTableView setTableFooterView:[UIView new]];
    
    // Fin du chargement
    [_chargementView setHidden:YES];
    if (kisAccessibiliteSounds) {
            // Son
            if ([[WPPlistRecup shared] getReglageSon]) {
                _soundPlayer = [[AVAudioPlayer alloc]
                                initWithContentsOfURL:[[NSBundle mainBundle]
                                                       URLForResource:@"sound-rafrechire-postes-abs"
                                                       withExtension:@"caf"]
                                error:0];
                [_soundPlayer play];
            }
        
        } else {
            UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, @"Contenu chargé");
        }
    
    [_rechercheField setUserInteractionEnabled:YES];
    _isSearch = NO;
}

#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kGetFontSize*65.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [(NSArray*)_articles count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TTPost *item = [_articles objectAtIndex:indexPath.row];
    static NSString *CellIdentifier = @"RechercheCell";
    WPArticleCell *cell = (WPArticleCell*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[WPArticleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier andWidth:self.view.frame.size.width];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    [cell setPost:item];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    TTPost *item = [_articles objectAtIndex:indexPath.row];
    _detailArticleController = [[WPDetailArticleViewController alloc] initWithArticle:item andDelegate:self andType:ArticleSearch];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
    [self.navigationController pushViewController:_detailArticleController animated:YES];
}

// No separator inset !
- (void)viewDidLayoutSubviews {
    [_aTableView setSeparatorInset:UIEdgeInsetsZero];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(WPArticleCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]){
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero]; // ios 8 newly added
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - Delegate

- (void) backToArticlesListWithType:(ArticleType)type {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void) openPrecedentArticle:(TTPost *)item andType:(ArticleType)type {
    
    BOOL suivant = NO;
    
    for (TTPost *article in _articles) {
        
        if (suivant) {
            [_detailArticleController reloadViewWithArticle:item];
            return;
        }
        
        if (article.postID == item.postID) {
            suivant = YES;
        }
    }

    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Précédent" message:@"Vous êtes déjà au dernier article." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alert show];
    
}

- (void) openSuivantArticle:(TTPost *)item andType:(ArticleType)type {
    
    TTPost *precedent = [TTPost new];
    
    for (TTPost *article in _articles) {
        
        if (article.postID == item.postID && precedent.postID != 0) {
            [_detailArticleController reloadViewWithArticle:precedent];
            return;
        }
        
        precedent = article;
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Suivant" message:@"Il n'y a pas d'articles plus récent." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alert show];
}

- (void) openBibliothequeWithType:(BiblioType)type {
    if ([_delegate respondsToSelector:@selector(openBibliothequeWithType:)]) {
        [_delegate openBibliothequeWithType:type];
    }
}

@end