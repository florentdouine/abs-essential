//
//  WPReglagesViewController.h
//  Toutdanslapoche
//
//  Created by Virginie Delaitre on 23/02/2015.
//  Copyright (c) 2015 Toutdanslapoche. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WPReglagesViewControllerDelegate <NSObject>
@optional
- (void) backToNav;
@end

@interface WPReglagesViewController : UIViewController  <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate, UITextFieldDelegate> {
    
    NSArray *_datasSection1;
    NSArray *_datasSection2;
    NSArray *_datasSection3;
    
    UISlider *tailleSlider;
    UITextField *nomField;
    UITextField *emailField;
    UISwitch *sonSwitch;
}

@property (nonatomic, strong) id<WPReglagesViewControllerDelegate>delegate;
@property (nonatomic, strong) UITableView *tableView;

- (id) initWithDelegate:(id)delegate;

@end
