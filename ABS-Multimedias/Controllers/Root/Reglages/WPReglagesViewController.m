//
//  WPReglagesViewController.m
//  Toutdanslapoche
//
//  Created by Virginie Delaitre on 23/02/2015.
//  Copyright (c) 2015 Toutdanslapoche. All rights reserved.
//

#import "WPReglagesViewController.h"
#import "WPAppDelegate.h"

@interface WPReglagesViewController ()

@end

@implementation WPReglagesViewController
@synthesize tableView = _tableView;
@synthesize delegate = _delegate;

- (id)initWithDelegate:(id)delegate {
    
    self = [super init];
    if (self) {
        
        [self.view setBackgroundColor:kColorBackground];
        _delegate = delegate;
        
        [self setTitle:@"Réglages"];
        
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 64.0f) style:UITableViewStyleGrouped];
        [_tableView setDelegate:self];
        [_tableView setDataSource:self];
        [_tableView setScrollEnabled:NO];
        [_tableView setBackgroundColor:[UIColor whiteColor]];
        [_tableView setBackgroundView: nil];
        [_tableView setScrollEnabled:NO];
        [[self view] addSubview:_tableView];
        
        _datasSection1 = [NSArray arrayWithObjects:@"", nil];
        _datasSection2 = [NSArray arrayWithObjects:@"Nom", @"Email", nil];
        _datasSection3 = [NSArray arrayWithObjects:@"Effets sonores", nil];
        
        [[UITextField appearance] setTintColor:kColorTextRead];
        [[UITextView appearance] setTintColor:kColorTextRead];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    // Google Analytics
    id<GAITracker> googleTracker = [[GAI sharedInstance] trackerWithTrackingId:kTrackingID];
    [googleTracker set:kGAIScreenName value:@"Navigation - Réglages"];
    [googleTracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [emailField resignFirstResponder];
    [nomField resignFirstResponder];
    if ([_delegate respondsToSelector:@selector(backToNav)]) {
        [_delegate backToNav];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (void) modifiedSound:(UISwitch*)sw {
    [[WPPlistRecup shared] setReglageSon:[sw isOn]];
}

#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 1:
            return _datasSection1.count;
            break;
        case 0:
            return _datasSection2.count;
            break;
        case 2:
            return _datasSection3.count;
            break;
            
        default:
            break;
    };
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.height, 50)];
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 25, self.view.frame.size.height, 25)];
    [lbl setFont:[UIFont fontWithName:kFontNameLight size:kGetFontSize*14]];
    [lbl setTextColor:kColorText];
    [lbl setNumberOfLines:1];
    [lbl setBaselineAdjustment:UIBaselineAdjustmentAlignCenters];
    [lbl setTextAlignment:NSTextAlignmentLeft];
    [lbl setBackgroundColor:[UIColor clearColor]];
    [lbl setAdjustsFontSizeToFitWidth:YES];
    if (section == 1) {
        [lbl setText:[@"Taille de police" uppercaseString]];
        [lbl setAccessibilityLabel:@"Taille de police"];
    } else if (section == 0) {
        [lbl setText:[@"Infos personnelles" uppercaseString]];
        [lbl setAccessibilityLabel:@"Infos personnelles"];
    } else if (section == 2) {
        [lbl setText:[@"Audio" uppercaseString]];
        [lbl setAccessibilityLabel:@"Audio"];
    }
    [header addSubview:lbl];
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ReglagesCell";
    UITableViewCell *cell = (UITableViewCell*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    NSDictionary *dict = [[WPPlistRecup shared] getCommentaireReglages];
    
    switch (indexPath.section) {
        case 1:
            // Slider
            tailleSlider = [[UISlider alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width-20, 44)];
            [tailleSlider setMinimumValue:4];
            [tailleSlider setMaximumValue:7];
            [tailleSlider setValue:((kGetFontSize*10)/2)];
            [tailleSlider setAccessibilityValue:[NSString stringWithFormat:@"%.0f%%", kGetFontSize*100]];
            [tailleSlider setMinimumTrackTintColor:kColorTextRead];
            [tailleSlider setMaximumValueImage:[UIImage imageNamed:@"police_hight"]];
            [tailleSlider setMinimumValueImage:[UIImage imageNamed:@"police_small"]];
            [tailleSlider addTarget:self action:@selector(sliderChanged:) forControlEvents:UIControlEventValueChanged];
            [cell addSubview:tailleSlider];
            break;
        case 0:
            [[cell textLabel] setText:[_datasSection2 objectAtIndex:indexPath.row]];
            [[cell textLabel] setAccessibilityElementsHidden:YES];
            switch (indexPath.row) {
                case 0:
                    nomField = [[UITextField alloc] initWithFrame:CGRectMake(70, 0, self.view.frame.size.width-80, 44)];
                    [nomField setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*14]];
                    [nomField setAccessibilityLabel:@"Nom"];
                    [nomField setText:[dict objectForKey:@"nom"]];
                    [nomField setDelegate:self];
                    [nomField setClearButtonMode:UITextFieldViewModeAlways];
                    [nomField setAutocorrectionType:UITextAutocorrectionTypeNo];
                    [nomField setReturnKeyType:UIReturnKeyDone];
                    [cell addSubview:nomField];
                    break;
                case 1:
                    emailField = [[UITextField alloc] initWithFrame:CGRectMake(70, 0, self.view.frame.size.width-80, 44)];
                    [emailField setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*14]];
                    [emailField setAccessibilityLabel:@"Email"];
                    [emailField setText:[dict objectForKey:@"email"]];
                    [emailField setKeyboardType:UIKeyboardTypeEmailAddress];
                    [emailField setAutocorrectionType:UITextAutocorrectionTypeNo];
                    [emailField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
                    [emailField setDelegate:self];
                    [emailField setClearButtonMode:UITextFieldViewModeAlways];
                    [emailField setReturnKeyType:UIReturnKeyDone];
                    [cell addSubview:emailField];
                    break;
                    
                default:
                    break;
            }
            break;
            
        case 2:
            [[cell textLabel] setText:[_datasSection3 objectAtIndex:indexPath.row]];
            [[cell textLabel] setAccessibilityElementsHidden:YES];
            // Switch
            sonSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 60, 7, 60, 20)];
            if (!IS_IOS7) {
                [sonSwitch setFrame:CGRectMake(self.view.frame.size.width - 95.0f, 8.5f, 95.0f, 17)];
            }
            [sonSwitch setOn:[[WPPlistRecup shared] getReglageSon]];
            [sonSwitch addTarget:self action:@selector(modifiedSound:) forControlEvents:UIControlEventValueChanged];
            [sonSwitch setOnTintColor:kColorNavBar];
            [cell addSubview:sonSwitch];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [cell addSubview:sonSwitch];
            break;
            
        default:
            break;
    };
    
    [[cell textLabel] setFont:[UIFont fontWithName:kFontNameBold size:kGetFontSize*16.0]];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

// No separator inset !
- (void)viewDidLayoutSubviews {
    [_tableView setSeparatorInset:UIEdgeInsetsZero];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]){
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero]; // ios 8 newly added
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - UISlider

- (void)sliderChanged:(UISlider *)mySlider {
//    mySlider.value = round(mySlider.value);
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:[NSNumber numberWithFloat:((round(mySlider.value)*2)/10)]
                     forKey:kFontSize];
    [mySlider setAccessibilityValue:[NSString stringWithFormat:@"%.0f%%",(((round(mySlider.value)*2)/10)*100)]];
    if ([userDefaults synchronize]) {
        [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : kColorTextNavBar, NSFontAttributeName: [UIFont fontWithName:kFontNameRegular size:kGetFontSize*18.0]}];
        [MyAppDelegate reloadAllApp];
    }
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == emailField && [emailField text] != nil && ![[emailField text] isEqualToString:@""]) {
        BOOL isGoodMail = NO;
        
        // Vérifier le format du mail
        NSArray *mail = [[emailField text] componentsSeparatedByString:@"@"];
        if ([mail count] == 2) {
            NSString *endMail = [mail objectAtIndex:1];
            NSArray *endMailArr = [endMail componentsSeparatedByString:@"."];
            if ([endMailArr count] >= 2) {
                // Le mail est ok !
                isGoodMail = YES;
            }
        }
        
        if (!isGoodMail) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur" message:@"Merci de rentrer une adresse mail valide !" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
            [emailField setText:@""];
        }
    }
    
    [[WPPlistRecup shared] sauvCommentaireReglagesWithEmail:[emailField text] andNom:[nomField text]];
}

@end
