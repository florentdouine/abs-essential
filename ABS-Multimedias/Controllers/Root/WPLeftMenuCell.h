//
//  WPLeftMenuCell.h
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WPLeftMenuCell : UITableViewCell {
    
    UILabel *titleLabel;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier andHeight:(CGFloat)height;
- (void)setTitre:(NSString*)titre andHeight:(CGFloat)height;

@end
