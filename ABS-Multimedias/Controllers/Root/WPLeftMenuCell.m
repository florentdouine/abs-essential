//
//  WPLeftMenuCell.m
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import "WPLeftMenuCell.h"

@implementation WPLeftMenuCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier andHeight:(CGFloat)height
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self setBackgroundColor:kColorBackground];
        
        // Label from
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 0, self.frame.size.width-40, height)];
        [titleLabel setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*16.0]];
        [titleLabel setTextColor:[UIColor blackColor]];
        [titleLabel setBaselineAdjustment:UIBaselineAdjustmentAlignCenters];
        [titleLabel setTextAlignment:NSTextAlignmentLeft];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [self addSubview:titleLabel];
    }
    return self;
}

- (void)setTitre:(NSString*)titre andHeight:(CGFloat)height {
    
    [titleLabel setText:titre];
    [titleLabel setFont:[UIFont fontWithName:kFontNameRegular size:kGetFontSize*16.0]];
    [titleLabel setFrame:CGRectMake(20.0, 0, self.frame.size.width-40, height)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}


@end
