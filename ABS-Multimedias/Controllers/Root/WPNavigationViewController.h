//
//  WPNavigationViewController.h
//  Toutdanslapoche
//
//  Created by Virginie Delaitre on 06/10/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WPAPIRequest.h"
#import "WPReglagesViewController.h"
#import "InAppPurchaseStoreManager.h"
#import "WPFavorisViewController.h"

@protocol WPNavigationViewControllerDelegate <NSObject>
- (void) pushViewControllerToRoot:(UIViewController*)viewController;
- (void) showIntro;
- (void) pushArticlesForType:(ArticlesType)type andLibrairie:(LibrairieType)type;
- (void) openArticleViewWithArticle:(TTPost *)item andType:(ArticleType)type;
- (void) pushAPIJSONArticlesForType:(ArticlesType)type andAPIType:(APIType)apiType;
@end

@interface WPNavigationViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, WPAPIRequestDelegate, UIAlertViewDelegate, WPReglagesViewControllerDelegate, InAppPurchaseStoreManagerDelegate, WPFavorisViewControllerDelegate> {
    
    UIView *_chargementView;
}

@property (strong, nonatomic) id<WPNavigationViewControllerDelegate>delegate;
@property (strong, nonatomic) UITableView *theTableView;
@property (strong, nonatomic) NSArray *dataSource;

- (id) initWithFrame:(CGRect)frame andDelegate:(id)delegate;
- (void)followOnTwitterFromUsername:(NSString *)fromUsername;

@end
