//
//  WPNavigationViewController.m
//  Toutdanslapoche
//
//  Created by Virginie Delaitre on 06/10/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import "WPNavigationViewController.h"
#import "WPLeftMenuCell.h"
#import "WPAppDelegate.h"

#import <TTRequest/OAuth+Additions.h>

#import "WPContactViewController.h"
#import "WPWebSiteViewController.h"

#import <VTFollowOnTwitter/VTFollowOnTwitter.h>
#import <Realm/Realm.h>

@implementation WPNavigationViewController
@synthesize theTableView = _theTableView;
@synthesize dataSource = _dataSource;
@synthesize delegate = _delegate;

- (id)initWithFrame:(CGRect)frame andDelegate:(id)delegate
{
    self = [super init];
    if (self) {
        
        [[self view] setFrame:frame];
        _delegate = delegate;
        [self.view setBackgroundColor:kColorBackground];
        
        _theTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, kViewHeight)];
        [_theTableView setDelegate:self];
        [_theTableView setDataSource:self];
        [_theTableView setRowHeight:kMenuCellHeight];
        [_theTableView setSectionHeaderHeight:kMenuHeaderHeight];
        [_theTableView setBackgroundColor:kColorBackground];
        [_theTableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
//        [_theTableView setScrollEnabled:NO];
        [_theTableView setSeparatorColor:kColorSeparator];
        [[self view] addSubview:_theTableView];
        
        // Data Source
        _dataSource = [[WPPlistRecup shared] getListOfHeaders];
        
        // Footer
        CGFloat height = _theTableView.frame.size.height - [_dataSource count]*kMenuHeaderHeight;
        for (Header* header in _dataSource) {
            height -= [[header listOfTitre] count]*kMenuCellHeight;
        }
        height += 27;
        if (height > 0) {
            UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, height)];
            [footer setBackgroundColor:kColorBackground];
            [_theTableView setTableFooterView:footer];
            [_theTableView setScrollEnabled:NO];
        } else {
            [_theTableView setTableFooterView:[UIView new]];
            [_theTableView setScrollEnabled:YES];
        }
        
        // Load
        _chargementView = [[TTChargementView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];;
        [[self view] addSubview:_chargementView];
        [_chargementView setHidden:YES];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated {
    
    [_theTableView scrollRectToVisible:CGRectMake(0, 0, _theTableView.frame.size.width, _theTableView.frame.size.height) animated:NO];
    [super viewDidAppear:animated];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    // Google Analytics
    id<GAITracker> googleTracker = [[GAI sharedInstance] trackerWithTrackingId:kTrackingID];
    [googleTracker set:kGAIScreenName value:@"Navigation"];
    [googleTracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return [_dataSource count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    Header *aHeader = [_dataSource objectAtIndex:section];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, kMenuHeaderHeight)];
    [view setBackgroundColor:kColorNextArticles];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0, self.view.frame.size.width-20, kMenuHeaderHeight)];
    [label setBackgroundColor:[UIColor clearColor]];
    [label setTextColor:kColorTextLight];
    [label setFont:[UIFont fontWithName:kFontNameBold size:kGetFontSize*16.0]];
    [label setTextAlignment:NSTextAlignmentLeft];
    [label setContentMode:UIViewContentModeCenter];
    [label setText:[aHeader titre]];
    [view addSubview:label];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    Header *aHeader = [_dataSource objectAtIndex:section];
    
    if ([[aHeader titre] isEqualToString:@""]) {
        return 0;
    } else {
        return kMenuHeaderHeight;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    Header *aHeader = [_dataSource objectAtIndex:section];
    return [[aHeader listOfTitre] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"WPLeftMenuCell";
    WPLeftMenuCell *cell = (WPLeftMenuCell*) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[WPLeftMenuCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier andHeight:kMenuCellHeight];
    }
    
    Header *aHeader = [_dataSource objectAtIndex:indexPath.section];
    NSArray *values = [aHeader listOfTitre];
    NSString *aValue = [values objectAtIndex:indexPath.row];

    [cell setTitre:aValue andHeight:kMenuCellHeight];
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kMenuCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) { // Publications
        if ([_delegate respondsToSelector:@selector(pushArticlesForType:andLibrairie:)]) {
            [_delegate pushArticlesForType:(int)indexPath.row+1 andLibrairie:LibrairieNone];
        }
    }
    
    if (indexPath.section == 1) { // Podcasts
        if ([_delegate respondsToSelector:@selector(pushAPIJSONArticlesForType:andAPIType:)]) {
            [_delegate pushAPIJSONArticlesForType:ArticlesAll andAPIType:APIPodcast];
        }
    }
    
    // Pour tous les menus
    if (indexPath.section == 2) { // Social
        
        if (indexPath.row == 0) {
            // S'abonner sur Twitter
            [self followOnTwitterFromUsername:nil];
        }
        
        if (kUrlFacebook != nil && indexPath.row == 1) {
            // Page Facebook
            [self openFacebookPage];
        }
        
        if ((kUrlFacebook == nil && indexPath.row == 1) || (kUrlFacebook != nil && indexPath.row == 2)) {
            // Pop Up
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Noter l'application" message:@"Voulez-vous vous rendre sur l'App Store pour noter cette application ?" delegate:self cancelButtonTitle:@"Annuler" otherButtonTitles:@"Continuer", nil];
            [alert setTag:1];
            [alert show];
        }
    }
    
    
    if (indexPath.section == 3) {
        switch (indexPath.row) {
            case 0:
                [self openContact];
                break;
            case 1:
                [self openFavorisView];
                break;
            case 2:
                [self restaurerAchatsInApp];
                break;
            case 3:
                [self openReglagesView];
                break;
                
            default:
                break;
        }
        return;
    }
}

// No separator inset !
- (void)viewDidLayoutSubviews {
    [_theTableView setSeparatorInset:UIEdgeInsetsZero];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(WPLeftMenuCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]){
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsZero]; // ios 8 newly added
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - Actions

- (void) showIntro {
    if ([_delegate respondsToSelector:@selector(showIntro)]) {
        [_delegate showIntro];
    }
}

- (void) openFavorisView {
    
    WPFavorisViewController* _favorisController = [[WPFavorisViewController alloc] initWithDelegate:self];
    [_favorisController.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    if ([_delegate respondsToSelector:@selector(pushViewControllerToRoot:)]) {
        [_delegate pushViewControllerToRoot:_favorisController];
    }
}

- (void) openReglagesView {
    
    WPReglagesViewController* _reglagesController = [[WPReglagesViewController alloc] initWithDelegate:self];
    [_reglagesController.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    if ([_delegate respondsToSelector:@selector(pushViewControllerToRoot:)]) {
        [_delegate pushViewControllerToRoot:_reglagesController];
    }
}

- (void) openContact {
    
    WPContactViewController* _contactController = [[WPContactViewController alloc] init/*WithDelegate:self*/];
    [_contactController.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];

    if ([_delegate respondsToSelector:@selector(pushViewControllerToRoot:)]) {
        [_delegate pushViewControllerToRoot:_contactController];
    }
}

- (void) openFacebookPage {
    
    WPWebSiteViewController* _websiteController = [[WPWebSiteViewController alloc] initWithUrl:kUrlFacebook];
    [_websiteController.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [_websiteController setTitle:@"Suivre sur Facebook"];
    
    if ([_delegate respondsToSelector:@selector(pushViewControllerToRoot:)]) {
        [_delegate pushViewControllerToRoot:_websiteController];
    }
}

#pragma mark - Noter l'application

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 1 && buttonIndex == 1) {
        
        // Ouvrir l'App Store
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:kUrlComplet]]];

        [[WPPlistRecup shared] setNoterApp:1];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - Twitter

- (void)followOnTwitterFromUsername:(NSString *)fromUsername {
    
    [_chargementView setHidden:NO];
    
    [VTFollowOnTwitter followUsername:kTwitterName fromPreferredUsername:fromUsername success:^{
        
        [_chargementView setHidden:YES];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter" message:[NSString stringWithFormat:@"Merci de suivre @%@ !", kTwitterName] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        
    } multipleAccounts:^(NSArray *usernames) {
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Quel compte souhaitez-vous utilisez pour nous suivre ?" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
        
        for (NSString *username in usernames)
            [actionSheet addButtonWithTitle:[@"@" stringByAppendingString:username]];
        
        [actionSheet addButtonWithTitle:@"Annuler"];
        [actionSheet setCancelButtonIndex:[usernames count]];
        [actionSheet showInView:self.view];
        
    } failure:^(NSError *error) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur" message:@"Vous ne pouvez actuellement pas utiliser votre compte Twitter, assurez-vous que votre appareil dispose d'une connexion internet et que vous avez au moins une configuration de compte Twitter." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        [_chargementView setHidden:YES];
    }];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex != actionSheet.cancelButtonIndex) {
        [self followOnTwitterFromUsername:[[actionSheet buttonTitleAtIndex:buttonIndex] stringByReplacingOccurrencesOfString:@"@" withString:@""]];
    } else {
        [_chargementView setHidden:YES];
    }
}

#pragma mark - Favoris Delegate

- (void)openArticleViewWithArticle:(TTPost *)item andType:(ArticleType)type {
    if ([_delegate respondsToSelector:@selector(openArticleViewWithArticle:andType:)]) {
        [_delegate openArticleViewWithArticle:item andType:type];
    }
}

#pragma mark - Reglages Delegate

- (void)backToNav {
    [_theTableView reloadData];
    
    // Footer
    CGFloat height = _theTableView.frame.size.height - [_dataSource count]*kMenuHeaderHeight;
    for (Header* header in _dataSource) {
        height -= [[header listOfTitre] count]*kMenuCellHeight;
    }
    height += 27;
    if (height > 0) {
        UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, height)];
        [footer setBackgroundColor:kColorBackground];
        [_theTableView setTableFooterView:footer];
        [_theTableView setScrollEnabled:NO];
    } else {
        [_theTableView setTableFooterView:[UIView new]];
        [_theTableView setScrollEnabled:YES];
    }
}

#pragma mark - In App Purchase Delegate

- (void) restaurerAchatsInApp {
    [_chargementView setHidden:NO];
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
    [self.navigationItem.leftBarButtonItem setEnabled:NO];
    [[InAppPurchaseStoreManager sharedManager] setDelegate:self];
    [[InAppPurchaseStoreManager sharedManager] restoreInAppPurchase];
}

#pragma mark - InAppPurchaseStoreManager Delegate

- (void)inAppPurchaseStoreManagerFailedToRestore {
    [_chargementView setHidden:YES];
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
    [self.navigationItem.leftBarButtonItem setEnabled:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur" message:@"Une erreur s'est produite lors de la restauration. Merci de réessayer ultérieurement." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alert show];
}

- (void)inAppPurchaseStoreManagerSuccessToRestore {
    [_chargementView setHidden:YES];
    [self.navigationItem.rightBarButtonItem setEnabled:YES];
    [self.navigationItem.leftBarButtonItem setEnabled:YES];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Restauration" message:@"Vos achats ont été restaurés avec succès ! Vous pouvez dès à présent, écouter les livres audio achetés dans la librairie." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alert show];
}

- (void)inAppPurchaseStoreManager:(InAppPurchaseStoreManager *)manager didPurchasedProduct:(NSString *)productId {
    
    RLMResults *livresAudio = [TTLivreAudio objectsWhere:[NSString stringWithFormat:@"identifier == \"%@\"", productId]];
    if (livresAudio.count == 0) {
        
        // Ajouter
        TTLivreAudio *livreAudio = [[TTLivreAudio alloc] init];
        [livreAudio setIdentifier:[NSString stringWithFormat:@"%@", productId]];
        [livreAudio setIsBuy:YES];
        [livreAudio setStatut:(int)LivreAudioBuy];
        [livreAudio setIsDownload:NO];
        [livreAudio setType:1];
        
        [livreAudio setArticleID:0];
        [livreAudio setTitre:@""];
        [livreAudio setContent:@""];
        [livreAudio setImageURL:@""];
        [livreAudio setDate:[NSDate date]];
        [livreAudio setURLFichier:@""];
        [livreAudio setCurrent:0];
        [livreAudio setPrice:0];
        
        // Get the default Realm
        RLMRealm *realm = [RLMRealm defaultRealm];
        [realm beginWriteTransaction];
        [realm addObject:livreAudio];
        [realm commitWriteTransaction];
        
    } else {
        // Mettre à jour
        for (TTLivreAudio *livreAudio in livresAudio) {
            [[RLMRealm defaultRealm] beginWriteTransaction];
            [livreAudio setIsBuy:YES];
            [[RLMRealm defaultRealm] commitWriteTransaction];
        }
    }
}

@end
