//
//  WPRootViewController.h
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WPAPIRequest.h"
#import "WPContactViewController.h"
#import "WPWebSiteViewController.h"
#import "WPNavigationViewController.h"
#import "WPIntroViewController.h"
// WordPress
#import "WPArticlesViewController.h"
#import "WPDetailArticleViewController.h"
#import "WPFavorisViewController.h"
#import "WPRechercheViewController.h"
#import "WPCategoriesViewController.h"
#import "WPBibliothequeViewController.h"
// WooCommerce
#import "WPArticlesWCViewController.h"
#import "WPDetailArticleWCViewController.h"

@interface WPRootViewController : UIViewController <WPAPIRequestDelegate, WPArticlesViewControllerDelegate, UIAlertViewDelegate, UITabBarDelegate, WPNavigationViewControllerDelegate, WPCategoriesViewControllerDelegate, WPIntroViewControllerDelegate> {
    
    ArticleType _type;
    int   _postID;
    
    // Tabbar items
    NSArray *tabbarItems;
    
    WPDetailArticleViewController *_detailArticleController;
    WPDetailArticleWCViewController *_detailArticleWCController;
}

@property (strong, nonatomic) UITabBar *tabBar;
@property (strong, nonatomic) UIViewController *currentController;
@property (strong, nonatomic) WPContactViewController *contactController;
@property (strong, nonatomic) WPWebSiteViewController *websiteController;
@property (strong, nonatomic) WPNavigationViewController *navigationTabBarController;
@property (strong, nonatomic) WPIntroViewController *introController;
// WordPress
@property (strong, nonatomic) WPArticlesViewController *articlesController;
@property (strong, nonatomic) WPArticlesViewController *subArticlesController;
@property (strong, nonatomic) WPRechercheViewController *rechercheController;
@property (strong, nonatomic) WPFavorisViewController *favorisController;
@property (strong, nonatomic) WPCategoriesViewController *categoriesController;
@property (strong, nonatomic) WPCategoriesViewController *librairieController;
@property (strong, nonatomic) WPBibliothequeViewController *bibliothequeController;
@property (strong, nonatomic) WPArticlesViewController *articlesJSONController;
@property (strong, nonatomic) WPArticlesViewController *annuaireController;
// WooCommerce
@property (strong, nonatomic) WPArticlesWCViewController *articlesWCController;

- (id)initWithFrame:(CGRect)frame;
- (id)initWithFrame:(CGRect)frame andModal:(NSString *)postID;
- (void) reloadView;

@end
