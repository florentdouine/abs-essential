//
//  WPRootViewController.m
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import "WPRootViewController.h"
#import <TTRequest/Reachability.h>
#import "WPIntroViewController.h"
#import "WPAppDelegate.h"

@implementation WPRootViewController
@synthesize tabBar = _tabBar;
@synthesize currentController = _currentController;
@synthesize contactController = _contactController;
@synthesize websiteController = _websiteController;
@synthesize navigationTabBarController = _navigationTabBarController;
@synthesize introController = _introController;
// WordPress
@synthesize articlesController = _articlesController;
@synthesize subArticlesController = _subArticlesController;
@synthesize rechercheController = _rechercheController;
@synthesize favorisController = _favorisController;
@synthesize categoriesController = _categoriesController;
// WooCommerce
@synthesize articlesWCController = _articlesWCController;

- (id)initWithFrame:(CGRect)frame andModal:(NSString *)postID {
    
    self = [super init];
    if (self) {
        [self.view setFrame:frame];
        [self initTheViewWithModal:[postID intValue]];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super init];
    if (self) {
        [self.view setFrame:frame];
        [self initTheViewWithModal:0];
    }
    return self;
}

- (void) initTheViewWithModal:(int)postID {
    
    [self.view setBackgroundColor:kColorBackground];
    
    _type = ArticleMenu;
    _postID = postID;
    
    Header *topHeader = [[WPPlistRecup shared] getHeaderForPosition:1];
    
    [self setTitle:[[topHeader listOfTitre] objectAtIndex:0]];
    
    // Check Version
    if (![[[WPPlistRecup shared] getCurrentVersion] isEqualToString:kCurrentAppVersion]) {
        [[WPPlistRecup shared] setNoterApp:0];
        [[WPPlistRecup shared] setCurrentVersion];
    }
    
    // Check Noter App
    if ([[WPPlistRecup shared] getNbOuvertures]%5 == 0) { // Tous les 5 ouvertures de l'application
        if (![[WPPlistRecup shared] getNoterApp]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Noter l'application" message:@"Vous aimez cette application ? Merci de laisser une note ou un commentaire :)" delegate:self cancelButtonTitle:@"Plus tard" otherButtonTitles:@"Continuer", nil];
            [alert setTag:1];
            [alert show];
        }
    }
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:kFontNameRegular size:12.0f], NSFontAttributeName, kColorTextRead, NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:kFontNameRegular size:12.0f], NSFontAttributeName, kColorTextNavBar, NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
    
    /*
    UITabBarItem *tousArticleItem = [[UITabBarItem alloc] initWithTitle:@"Accueil" image:nil tag:0];
    tousArticleItem.image = [[UIImage imageNamed:@"tabbar_item_1_off.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    tousArticleItem.selectedImage = [[UIImage imageNamed:@"tabbar_item_1_on.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    */
    
    UITabBarItem *livreAudioItem = [[UITabBarItem alloc] initWithTitle:@"Librairie" image:nil tag:0];
    livreAudioItem.image = [[UIImage imageNamed:@"tabbar_item_3_off.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    livreAudioItem.selectedImage = [[UIImage imageNamed:@"tabbar_item_3_on.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];

    
    UITabBarItem *categoriesItem = [[UITabBarItem alloc] initWithTitle:@"Navigation" image:nil tag:1];
    categoriesItem.image = [[UIImage imageNamed:@"tabbar_item_2_off.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    categoriesItem.selectedImage = [[UIImage imageNamed:@"tabbar_item_2_on.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    UITabBarItem *bibliothequeItem = [[UITabBarItem alloc] initWithTitle:@"Bibliothèque" image:nil tag:2];
    bibliothequeItem.image = [[UIImage imageNamed:@"tabbar_item_5_off.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    bibliothequeItem.selectedImage = [[UIImage imageNamed:@"tabbar_item_5_on.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    /*
    UITabBarItem *navigationItem = [[UITabBarItem alloc] initWithTitle:@"Annuaire"  image:nil tag:3];
    navigationItem.image = [[UIImage imageNamed:@"tabbar_item_4_off.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    navigationItem.selectedImage = [[UIImage imageNamed:@"tabbar_item_4_on.png"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    */
    
    tabbarItems = @[livreAudioItem, categoriesItem, bibliothequeItem];
    
    _tabBar = [[UITabBar alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 49.0f - 64.0f, self.view.frame.size.width, 49.0f)];
    [_tabBar setItemPositioning:UITabBarItemPositioningCentered];
    [_tabBar setBarTintColor:kColorNavBar];
    [_tabBar setDelegate:self];
    [_tabBar setItems:tabbarItems];
    [[self view] addSubview:_tabBar];
    [[self view] bringSubviewToFront:_tabBar];
    
    [_tabBar setSelectedItem:livreAudioItem];
    [self openCategorieViewWithArray:kArraySubWebsite01CategoriesName andTitle:@"Librairie" andType:LibrairieTous];
    
    // Message WiFi
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    
    NetworkStatus status = [reachability currentReachabilityStatus];
    if (status == ReachableViaWiFi) {
        [[TTDownloadManager shared] startDownload];
    } if ([[TTDownloadManager shared] isLivreAudioToDownload] && status == ReachableViaWWAN) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Téléchargements en cours" message:@"Il y a des livres audio en attente de téléchargement, voulez-vous les télécharger maintenant ? Il est préférable d'être connecté en Wifi." delegate:self cancelButtonTitle:@"Plus tard" otherButtonTitles:@"Oui", nil];
        [alert setTag:2];
        [alert show];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Intro
    if ([[WPPlistRecup shared] isFirstOpen]) {
        [[WPPlistRecup shared] setFirstOpenToNo];
        [self.view setAccessibilityElementsHidden:YES];
        [self.navigationController setAccessibilityElementsHidden:YES];
        //[self showIntro];
    }
}

#pragma mark - Intro

- (void) showIntro {
    _introController = [[WPIntroViewController alloc] initWithFrame:CGRectMake(0, 0, MyAppDelegate.window.frame.size.width, MyAppDelegate.window.frame.size.height) andDelegate:self];
    [MyAppDelegate.window addSubview:_introController.view];
}

- (void) closeIntro {
    [self selectTabBarItem:0];
    [UIView animateWithDuration:0.3 animations:^{
        [_introController.view setAlpha:0];
    } completion:^(BOOL finished) {
        [_introController.view removeFromSuperview];
        [self.view setAccessibilityElementsHidden:NO];
        [self.navigationController setAccessibilityElementsHidden:NO];
        
        UIAccessibilityPostNotification(UIAccessibilityScreenChangedNotification, self.navigationController);
    }];
}

#pragma mark -

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    if (_postID != 0) {
        // Ouvrir l'article
        WPDetailArticleViewController *detailController = [[WPDetailArticleViewController alloc] initWithArticleID:_postID andDelegate:self andType:ArticleStandalone];
        [detailController.view setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:detailController];
        [navController.navigationBar setTranslucent:NO];
        
        [self presentViewController:navController animated:YES completion:nil];
        _postID = 0;
    }
}

- (void) setDefaultNavigationButtons {
    
    UIButton *reloadCustomButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    [reloadCustomButton setBackgroundImage:[UIImage imageNamed:@"button_reload_off.png"] forState:UIControlStateNormal];
    [reloadCustomButton setBackgroundImage:[UIImage imageNamed:@"button_reload_on.png"] forState:UIControlStateHighlighted];
    [reloadCustomButton addTarget:self action:@selector(refreshAction) forControlEvents:UIControlEventTouchUpInside];
    [reloadCustomButton setAccessibilityLabel:@"Recharger la liste"];
    UIBarButtonItem *reloadButton = [[UIBarButtonItem alloc] initWithCustomView:reloadCustomButton];
    [self.navigationItem setLeftBarButtonItem:reloadButton];
    
    if (_currentController != _annuaireController) {
        UIButton *rightCustomButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
        [rightCustomButton setBackgroundImage:[UIImage imageNamed:@"button_search_off.png"] forState:UIControlStateNormal];
        [rightCustomButton setBackgroundImage:[UIImage imageNamed:@"button_search_on.png"] forState:UIControlStateHighlighted];
        [rightCustomButton addTarget:self action:@selector(openRightMenu) forControlEvents:UIControlEventTouchUpInside];
        [rightCustomButton setAccessibilityLabel:@"Recherche"];
        UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:rightCustomButton];
        [self.navigationItem setRightBarButtonItems:[NSArray new]];
        [self.navigationItem setRightBarButtonItem:rightButton];
    } else {
        [self.navigationItem setRightBarButtonItem:nil];
    }
    
    [self.navigationItem setTitleView:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) reloadView {
    [_articlesController reloadView];
}

#pragma mark - Actions

- (void) openRightMenu {
    _rechercheController = [[WPRechercheViewController alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+44) andDelegate:self];
    [self.navigationController pushViewController:_rechercheController animated:YES];
}

- (void) refreshAction {
    if ([_currentController respondsToSelector:@selector(refreshAction)]) {
        [_currentController performSelector:@selector(refreshAction)];
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 1 && buttonIndex == 1) {
        [[WPPlistRecup shared] setNoterApp:1];
        
        // Ouvrir l'App Store
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:kUrlComplet]]];
    }
    if (alertView.tag == 2 && buttonIndex == 1) {
        [[TTDownloadManager shared] startDownload];
    }
}

#pragma mark - Favoris

- (void) openFavorisView {
    
    _type = ArticleMenu;
    
    if (_favorisController == nil) {
        _favorisController = [[WPFavorisViewController alloc] initWithDelegate:self];
        [_favorisController.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    } else {
        [_favorisController reloadFavorisDatas];
    }
    
    [self setTitle:@"Mes Favoris"];
    
    [self setDefaultNavigationButtons];
    [UIView animateWithDuration:0.2
                     animations:^{
                         [self.view addSubview:_favorisController.view];
                         [[self view] bringSubviewToFront:_tabBar];
                         _currentController = _favorisController;
                         [self.navigationController.navigationBar setTranslucent:NO];
                     } completion:^(BOOL finished) {
                         [self removeFromSuperviewUnlessTheView:_favorisController];
                     }];
}

#pragma mark - Contact

- (void) openContactView {
    
    _type = ArticleMenu;
    
    if (_contactController == nil) {
        _contactController = [[WPContactViewController alloc] init/*WithDelegate:self*/];
        [_contactController.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    } else {
//        [_contactController reloadFavorisDatas];
    }
    
    // Navigation Buttons
//    UIButton *leftCustomButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
//    [leftCustomButton setBackgroundImage:[UIImage imageNamed:@"button_menu_off.png"] forState:UIControlStateNormal];
//    [leftCustomButton setBackgroundImage:[UIImage imageNamed:@"button_menu_on.png"] forState:UIControlStateHighlighted];
//    [leftCustomButton addTarget:self action:@selector(openLeftMenu) forControlEvents:UIControlEventTouchUpInside];
//    [leftCustomButton setAccessibilityLabel:@"Navigation"];
//    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:leftCustomButton];
    [self.navigationItem setLeftBarButtonItem:nil];
    [self.navigationItem setRightBarButtonItem:nil];
    
    [self setTitle:@"Contact"];
    
    [UIView animateWithDuration:0.2
                     animations:^{
                         [self.view addSubview:_contactController.view];
                         _currentController = _contactController;
                     } completion:^(BOOL finished) {
                         [self removeFromSuperviewUnlessTheView:_contactController];
                     }];
}

#pragma mark Pratique

- (void) openWebSiteViewWithUrl:(NSString *)urlString andTitle:(NSString*)title {
    
    _type = ArticleMenu;
    
    if (_websiteController == nil) {
        _websiteController = [[WPWebSiteViewController alloc] initWithUrl:urlString];
        [_websiteController.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    } else {
        [_websiteController reloadKeynoteDataWithUrl:urlString];
    }
    
    // Navigation Buttons
//    UIButton *leftCustomButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
//    [leftCustomButton setBackgroundImage:[UIImage imageNamed:@"button_menu_off.png"] forState:UIControlStateNormal];
//    [leftCustomButton setBackgroundImage:[UIImage imageNamed:@"button_menu_on.png"] forState:UIControlStateHighlighted];
//    [leftCustomButton addTarget:self action:@selector(openLeftMenu) forControlEvents:UIControlEventTouchUpInside];
//    [leftCustomButton setAccessibilityLabel:@"Navigation"];
//    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:leftCustomButton];
    [self.navigationItem setLeftBarButtonItem:nil];
    
    UIButton *reloadCustomButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 27, 27)];
    [reloadCustomButton setBackgroundImage:[UIImage imageNamed:@"button_reload_off.png"] forState:UIControlStateNormal];
    [reloadCustomButton setBackgroundImage:[UIImage imageNamed:@"button_reload_on.png"] forState:UIControlStateHighlighted];
    [reloadCustomButton addTarget:_websiteController.browserWebView action:@selector(reload) forControlEvents:UIControlEventTouchUpInside];
    [reloadCustomButton setAccessibilityLabel:@"Recharger la page"];
    UIBarButtonItem *reloadButton = [[UIBarButtonItem alloc] initWithCustomView:reloadCustomButton];
    
    [self.navigationItem setRightBarButtonItem:reloadButton];
    
    [self setTitle:title];
    
    [UIView animateWithDuration:0.2
                     animations:^{
                         [self.view addSubview:_websiteController.view];
                         [[self view] bringSubviewToFront:_tabBar];
                         _currentController = _websiteController;
                     } completion:^(BOOL finished) {
                         [self removeFromSuperviewUnlessTheView:_websiteController];
                     }];
}

#pragma mark - Navigation

- (void) openNavigationView {
    
    _type = ArticleMenu;
    
    if (_navigationTabBarController == nil) {
        _navigationTabBarController = [[WPNavigationViewController alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) andDelegate:self];
    }
    
    [self setTitle:@"Navigation"];
    
    [self setDefaultNavigationButtons];
    [self.navigationItem setLeftBarButtonItem:nil];
    [UIView animateWithDuration:0.2
                     animations:^{
                         [self.view addSubview:_navigationTabBarController.view];
                         [[self view] bringSubviewToFront:_tabBar];
                         _currentController = _navigationTabBarController;
                         [self.navigationController.navigationBar setTranslucent:NO];
                     } completion:^(BOOL finished) {
                         [self removeFromSuperviewUnlessTheView:_navigationTabBarController];
                     }];
}

#pragma mark - Bibliothèque

- (void) openBibliothequeWithType:(BiblioType)type {
    [[self navigationController] popToRootViewControllerAnimated:YES];
    [_tabBar setSelectedItem:[tabbarItems objectAtIndex:2]];
    [self openBibliothequeViewWithType:type];
}

- (void) openBibliothequeView {
    [self openBibliothequeWithType:BiblioLivresAudio];
}

- (void) openBibliothequeViewWithType:(BiblioType)type {
    
    _type = ArticleMenu;
    
    if (_bibliothequeController == nil) {
        _bibliothequeController = [[WPBibliothequeViewController alloc] initWithType:type andDelegate:self];
        [_bibliothequeController.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    } else {
        [_bibliothequeController reloadViewWithType:type];
    }
    
    [self setTitle:@"Bibliothèque"];
    
    [self.navigationItem setLeftBarButtonItems:nil];
    [self.navigationItem setLeftBarButtonItem:nil];
    [self.navigationItem setRightBarButtonItems:nil];
    [self.navigationItem setRightBarButtonItem:nil];
    [UIView animateWithDuration:0.2
                     animations:^{
                         [self.view addSubview:_bibliothequeController.view];
                         [[self view] bringSubviewToFront:_tabBar];
                         _currentController = _bibliothequeController;
                         [self.navigationController.navigationBar setTranslucent:NO];
                     } completion:^(BOOL finished) {
                         [self removeFromSuperviewUnlessTheView:_bibliothequeController];
                         [[TTDownloadManager shared] setDelegate:_bibliothequeController];
                     }];
}

#pragma mark - Annuaire

- (void) openAnnuaireView {
    
    _type = ArticleMenu;
    
    if (_annuaireController == nil) {
        _annuaireController = [[WPArticlesViewController alloc] initWithType:ArticlesAll andAPIType:APIAnnuaire andFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) andDelegate:self];
        [_annuaireController reloadViewWithArticles];
    } else {
        [_annuaireController reloadViewWithArticles];
    }
    
    [self setTitle:@"Annuaire"];
    
    [self setDefaultNavigationButtons];
    [self.navigationItem setRightBarButtonItems:nil];
    [self.navigationItem setRightBarButtonItem:nil];
    [UIView animateWithDuration:0.2
                     animations:^{
                         [self.view addSubview:_annuaireController.view];
                         [[self view] bringSubviewToFront:_tabBar];
                         _currentController = _annuaireController;
                         [self.navigationController.navigationBar setTranslucent:NO];
                     } completion:^(BOOL finished) {
                         [self removeFromSuperviewUnlessTheView:_annuaireController];
                     }];
}

#pragma mark - Catégories

- (void) openCategorieViewWithArray:(NSArray*)categories andTitle:(NSString*)title andType:(LibrairieType)type {
    [self openCategorieViewWithArray:categories andTitle:title andPushArticleID:0 andType:type];
}

- (void) openCategorieViewWithArray:(NSArray*)categories andTitle:(NSString*)title andPushArticleID:(int)articleID andType:(LibrairieType)type {
    
    _type = ArticleMenu;
    
    if (type == LibrairieNone) {
        if (_categoriesController == nil) {
            _categoriesController = [[WPCategoriesViewController alloc] initWithDelegate:self andCategories:categories andTitle:title andType:type];
            [_categoriesController.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        } else {
            [_categoriesController reloadViewWithCategories:categories andTitle:title andType:type];
        }
        
        [self setTitle:title];
        
        [self setDefaultNavigationButtons];
        [UIView animateWithDuration:0.2
                         animations:^{
                             [self.view addSubview:_categoriesController.view];
                             [[self view] bringSubviewToFront:_tabBar];
                             _currentController = _categoriesController;
                             [self.navigationController.navigationBar setTranslucent:NO];
                         } completion:^(BOOL finished) {
                             [self removeFromSuperviewUnlessTheView:_categoriesController];
                         }];
        
    } else {
        if (_librairieController == nil) {
            _librairieController = [[WPCategoriesViewController alloc] initWithDelegate:self andCategories:categories andTitle:title andType:type];
            [_librairieController.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        } else {
            [_librairieController reloadViewWithCategories:categories andTitle:title andType:type];
        }
        
        [self setTitle:title];
        
        [self setDefaultNavigationButtons];
        [self.navigationItem setRightBarButtonItem:nil];
        [self.navigationItem setRightBarButtonItems:nil];
        [UIView animateWithDuration:0.2
                         animations:^{
                             [self.view addSubview:_librairieController.view];
                             [[self view] bringSubviewToFront:_tabBar];
                             _currentController = _librairieController;
                             [self.navigationController.navigationBar setTranslucent:NO];
                         } completion:^(BOOL finished) {
                             [self removeFromSuperviewUnlessTheView:_librairieController];
                         }];
    }
    
    if (articleID != 0) {
        [self openArticleViewWithArticleID:articleID andType:ArticleSubWebsite];
    }
}

- (void)pushArticlesForType:(ArticlesType)aType andLibrairie:(LibrairieType)type {
    
    if (type == LibrairieNone) {
        _type = ArticleSubArticle;
        
        _subArticlesController = nil;
        _subArticlesController = [[WPArticlesViewController alloc] initWithType:aType andAPIType:APIPost andFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) andDelegate:self];
        
        [_subArticlesController reloadViewWithArticlesType:aType andAPIType:APIPost];
        [self.navigationController pushViewController:_subArticlesController animated:YES];
        
    } else {
        _articlesWCController = nil;
        _articlesWCController = [[WPArticlesWCViewController alloc] initWithType:aType andFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) andDelegate:self];
        
        [_articlesWCController reloadViewWithArticlesType:aType];
        [self.navigationController pushViewController:_articlesWCController animated:YES];
    }
}

- (void)pushAPIJSONArticlesForType:(ArticlesType)aType andAPIType:(APIType)apiType {
    
    _type = ArticleSubArticle;
    
    _articlesJSONController = nil;
    _articlesJSONController = [[WPArticlesViewController alloc] initWithType:aType andAPIType:apiType andFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) andDelegate:self];
    [_articlesJSONController reloadViewWithArticlesType:aType andAPIType:apiType];
    [self.navigationController pushViewController:_articlesJSONController animated:YES];
}

#pragma mark - Articles

- (void) reloadArticles {
    if (_currentController == _articlesController && !(_articlesController.theType == ArticlesAll && _articlesController.apiType == APIPost)) {
        [self reloadArticlesWithType:_articlesController.theType];
    }
}

- (void)reloadArticlesWithType:(ArticlesType)aType {
    [self reloadArticlesWithType:aType andOpenArticleID:0];
}

- (void)reloadArticlesWithType:(ArticlesType)aType andOpenArticleID:(int)articleID {
    
    _type = ArticleMenu;
    
    if (_articlesController == nil) {
        _articlesController = [[WPArticlesViewController alloc] initWithType:aType andAPIType:APIPost andFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-64) andDelegate:self];
        [_articlesController reloadViewWithArticlesType:ArticlesAll andAPIType:APIPost];
    } else {
        // Push le SubWebsite
        if (articleID != 0) {
            _articlesController.theType = aType;
            [self openArticleViewWithArticleID:articleID andType:ArticleSubWebsite andReload:YES];
        } else {
            [_articlesController reloadViewWithArticlesType:aType andAPIType:APIPost];
        }
    }
    
    if (_currentController != _articlesController) {
        [UIView animateWithDuration:0.2
                         animations:^{
                             [self.view addSubview:_articlesController.view];
                             [[self view] bringSubviewToFront:_tabBar];
                             _currentController = _articlesController;
                             //[_articlesController.aTableView scrollRectToVisible:CGRectMake(0, 0, _articlesController.aTableView.frame.size.width, _articlesController.aTableView.frame.size.height-64) animated:YES];
                         } completion:^(BOOL finished) {
                             [self removeFromSuperviewUnlessTheView:_articlesController];
                         }];
    }
    
    // Navigation Bar
    // Title
    if (aType == ArticlesAll) {
        [self setTitle:@"Accueil"];
    } else {
        NSArray *categories = kArrayCategoriesName;
        [self setTitle:[categories objectAtIndex:aType-1]];
    }
    
    [self.navigationController.navigationBar setTranslucent:NO];
    [self setDefaultNavigationButtons];
}

- (void) openArticleViewWithArticle:(TTPost*)item andType:(ArticleType)type {
    
    _type = type;
    
    _detailArticleController = [[WPDetailArticleViewController alloc] initWithArticle:item andDelegate:self andType:type];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController pushViewController:_detailArticleController animated:YES];
}

- (void) openArticleWCViewWithArticle:(TTPost*)item andType:(ArticleType)type {
    
    _type = type;
    
    _detailArticleWCController = [[WPDetailArticleWCViewController alloc] initWithArticle:item andDelegate:self andType:type];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController pushViewController:_detailArticleWCController animated:YES];
}

- (void) openArticleAPIJSONViewWithArticle:(TTPost *)item andType:(ArticleType)type andAPIType:(APIType)apiType {
    
    _type = type;
    
    _detailArticleController = [[WPDetailArticleViewController alloc] initWithArticle:item andDelegate:self andType:type];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController pushViewController:_detailArticleController animated:YES];
}

- (void) openArticleViewWithArticleID:(int)articleID andType:(ArticleType)type {
    [self openArticleViewWithArticleID:articleID andType:type andReload:NO];
}

- (void) openArticleViewWithArticleID:(int)articleID andType:(ArticleType)type andReload:(BOOL)isNeededReload {
    
    _type = type;
    
    _detailArticleController = [[WPDetailArticleViewController alloc] initWithArticleID:articleID andDelegate:self andType:type];
    _detailArticleController.isArticlesNeededReload = isNeededReload;
    [self.navigationController.navigationBar setTranslucent:NO];
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
    [self.navigationController pushViewController:_detailArticleController animated:YES];
}

- (void) backToArticlesListWithType:(ArticleType)type {
    [self backToArticlesListWithType:type andReload:NO];
}

- (void) backToArticlesListWithType:(ArticleType)type andReload:(BOOL)isNeededReload {
    
    if (_currentController == _articlesController) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    [_detailArticleController removeFromParentViewController];
    _detailArticleController = nil;
    
    switch (type) {  // Mettre en grisé les articles lus
        case ArticleMenu:
            [_articlesController.aTableView reloadData];
            break;
        case ArticleFavoris:
            [_favorisController.aTableView reloadData];
            break;
        case ArticleSearch:
            [_rechercheController.aTableView reloadData];
            break;
        case ArticleSubArticle:
            [_subArticlesController.aTableView reloadData];
            break;
            
        default:
            break;
    }
    
    if (isNeededReload && type != ArticleFavoris && type != ArticleSearch) {
        if (_currentController == _articlesController) {
            [_articlesController reloadViewWithArticles];
        } else {
            [_subArticlesController reloadViewWithArticles];
        }
    }
    
    if (type != ArticleSearch) {
        [self.navigationController.navigationBar setTranslucent:NO];
    }
    
    if (type == ArticleFavoris) {
        [_favorisController reloadFavorisDatas];
    }
}

- (void) openPrecedentArticle:(TTPost *)item andType:(ArticleType)type {
    
    BOOL suivant = NO;
    
    NSArray *articles = [NSArray array];
    
    switch (type) {
        case ArticleMenu:
            articles = _articlesController.articles;
            break;
        case ArticleSearch:
            articles = _rechercheController.articles;
            break;
        case ArticleFavoris:
            articles = _favorisController.articles;
            break;
        case ArticleSubArticle:
            articles = _subArticlesController.articles;
            break;
        case ArticleSubWebsite:
            articles = _subArticlesController.articles;
            break;
            
        default:
            break;
    }
    
    for (TTPost *item in articles) {
        
        if (suivant) {
            [_detailArticleController reloadViewWithArticle:item];
            return;
        }
        
        if (item.postID == item.postID) {
            suivant = YES;
        }
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Précédent" message:@"Vous êtes déjà au dernier article." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alert show];
    
}

- (void) openSuivantArticle:(TTPost *)item andType:(ArticleType)type {
    
    TTPost *precedent = [TTPost new];
    NSArray *articles = [NSArray array];
    
    switch (type) {
        case ArticleMenu:
            articles = _articlesController.articles;
            break;
        case ArticleSearch:
            articles = _rechercheController.articles;
            break;
        case ArticleFavoris:
            articles = _favorisController.articles;
            break;
        case ArticleSubArticle:
            articles = _subArticlesController.articles;
            break;
        case ArticleSubWebsite:
            articles = _subArticlesController.articles;
            break;
            
        default:
            break;
    }
    
    for (TTPost *article in articles) {
        
        if (article.postID == item.postID && article.postID != 0) {
            [_detailArticleController reloadViewWithArticle:precedent];
            return;
        }
        
        precedent = article;
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Suivant" message:@"Il n'y a pas d'articles plus récent." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alert show];
}

#pragma mark - RemoveFromSuperview

- (void) removeFromSuperviewUnlessTheView:(UIViewController*)viewcontroller {
    
    NSMutableArray *controllers = [[NSMutableArray alloc] init];
    
    
    if (_categoriesController != nil) {
        [controllers addObject:_categoriesController];
    }

    if (_favorisController != nil) {
        [controllers addObject:_favorisController];
    }
    
    if (_contactController != nil) {
        [controllers addObject:_contactController];
    }
    
    if (_websiteController != nil) {
        [controllers addObject:_websiteController];
    }
    
    if (_navigationTabBarController != nil) {
        [controllers addObject:_navigationTabBarController];
    }
    
    if (_bibliothequeController != nil) {
        [controllers addObject:_bibliothequeController];
    }
    
    if (_librairieController != nil) {
        [controllers addObject:_librairieController];
    }
    
    if (_subArticlesController != nil) {
        [controllers addObject:_subArticlesController];
    }
    
    if (_articlesWCController != nil) {
        [controllers addObject:_articlesWCController];
    }
    
    if (_articlesJSONController != nil) {
        [controllers addObject:_articlesJSONController];
    }
    
    if (_annuaireController != nil) {
        [controllers addObject:_annuaireController];
    }
        
    for (UIViewController *aController in controllers) {
        
        if (aController != viewcontroller && aController != nil) {
            [aController.view removeFromSuperview];
        }
    }
}

#pragma mark - APIDelegate

- (void)finishedLoadArticlesWithStatut:(RequestStatut)aStatut andJSONArray:(NSArray *)aJSON {
    
    if (aStatut == RequestStatutSucceeded) {
        
        [_articlesController reloadArticleViewWithArticles:aJSON];
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur de chargement" message:@"Un problème est survenu lors du chargement des articles. Vérifiez votre connexion internet et essayez de recharger la vue." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];

        [_articlesController stopLoadingViewWithFooter:NO];
        [_articlesController addReloadViewForCenter:_articlesController.contentView.center];
    }
}

#pragma mark - TabBar Delegate

- (void) selectTabBarItem:(int)tag {
    [_tabBar setSelectedItem:[tabbarItems objectAtIndex:tag]];
    
    UITabBarItem *item = [UITabBarItem new];
    [item setTag:tag];
    [self tabBar:_tabBar didSelectItem:item];
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    
    switch (item.tag) {
        case 0:
            [self openCategorieViewWithArray:kArraySubWebsite01CategoriesName andTitle:@"Librairie" andType:LibrairieTous];
            break;
        case 1:
            if (_currentController == _navigationTabBarController) {
                break;
            }
            [self openNavigationView];
            break;
        case 2:
            [self openBibliothequeView];
            break;
            
        default:
            break;
    }
}

- (void)pushViewControllerToRoot:(UIViewController *)viewController {
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
