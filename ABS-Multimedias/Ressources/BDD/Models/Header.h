//
//  Header.h
//  MacinPoche
//
//  Created by Virginie on 04/06/13.
//  Copyright (c) 2013 toutdanslapoche. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Header : NSObject

@property (assign, nonatomic) NSInteger id_header;
@property (strong, nonatomic) NSString *titre;
@property (strong, nonatomic) NSArray *listOfTitre;

@end
