//
//  TTLivreAudio.h
//  ABS-Multimedias
//
//  Created by Virginie Delaitre on 07/04/2015.
//  Copyright (c) 2015 Toutdanslapoche. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Realm/Realm.h>

@interface TTLivreAudio : RLMObject
@property int articleID;
@property NSString *titre;
@property NSString *content;
@property NSString *URLFichier;
@property CGFloat duree;
@property CGFloat current;
@property int statut;
@property NSDate *date;
@property NSString *imageURL;
@property int type;
@property BOOL isBuy;
@property BOOL isDownload;
@property float price;
@property NSString *identifier;
@end

