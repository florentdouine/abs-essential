//
//  TTPost.h
//  ABS Essential
//
//  Created by Virginie Delaitre on 11/02/2016.
//  Copyright © 2016 Toutdanslapoche. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TTPost : NSObject

@property (nonatomic, assign) int           postID;

@property (nonatomic, strong) NSString *    URLFeatureImg;
@property (nonatomic, strong) NSString *    categories;
@property (nonatomic, strong) NSString *    soustitre;
@property (nonatomic, strong) NSString *    titre;
@property (nonatomic, assign) int           nbComments;
@property (nonatomic, assign) int           isCommentsOpen;

@property (nonatomic, strong) NSString *    dateStr;
@property (nonatomic, strong) NSDate *      date;

@property (nonatomic, assign) BOOL          isCompletePost;
@property (nonatomic, assign) BOOL          inStock;
@property (nonatomic, strong) NSString *    content;
@property (nonatomic, strong) NSString *    shortContent;

@property (nonatomic, strong) NSString *    auteur;
@property (nonatomic, strong) NSString *    narrateur;
@property (nonatomic, strong) NSString *    URLArticle;
@property (nonatomic, strong) NSString *    URLEditor;
@property (nonatomic, strong) NSString *    presentateurs;
@property (nonatomic, strong) NSString *    invites;

@property (nonatomic, strong) NSString *    URLMp3Extract;
@property (nonatomic, strong) NSString *    URLMp3File;
@property (nonatomic, strong) NSString *    URLZipFile;
@property (nonatomic, strong) NSString *    codeISRC;

@property (nonatomic, strong) NSString *    auteur2;
@property (nonatomic, strong) NSString *    editeur;
@property (nonatomic, strong) NSString *    prix;
@property (nonatomic, strong) NSString *    identifier;

@property (nonatomic, strong) NSString *    duree;
@property (nonatomic, strong) NSString *    tags;

@property (nonatomic, assign) APIType       type;

@end
