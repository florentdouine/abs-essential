//
//  TTPost.m
//  ABS Essential
//
//  Created by Virginie Delaitre on 11/02/2016.
//  Copyright © 2016 Toutdanslapoche. All rights reserved.
//

#import "TTPost.h"

@implementation TTPost

@synthesize postID;
@synthesize URLFeatureImg;
@synthesize categories;
@synthesize soustitre;
@synthesize titre;
@synthesize nbComments;
@synthesize isCommentsOpen;
@synthesize dateStr;
//@synthesize date;
@synthesize isCompletePost;
@synthesize inStock;
@synthesize content;
@synthesize shortContent;
@synthesize auteur;
@synthesize narrateur;
@synthesize URLArticle;
@synthesize URLEditor;
@synthesize presentateurs;
@synthesize invites;
@synthesize URLMp3File;
@synthesize URLMp3Extract;
@synthesize URLZipFile;
@synthesize codeISRC;
@synthesize auteur2;
@synthesize editeur;
@synthesize prix;
@synthesize identifier;
@synthesize duree;
@synthesize tags;
@synthesize type;
@end
