//
//  WPComment.h
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WPComment : NSObject

@property (strong, nonatomic) NSString *message;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *courriel;

@end
