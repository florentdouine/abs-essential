//
//  WPAPIRequest.h
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TTRequest/ASIFormDataRequest.h>

@protocol WPAPIRequestDelegate <NSObject>
@optional
// WordPress
- (void) finishedLoadArticlesWithStatut:(RequestStatut)aStatut andJSONArray:(NSArray *)aJSON;
- (void) finishedLoadMoreArticlesWithStatut:(RequestStatut)aStatut andJSONArray:(NSArray *)aJSON;
- (void) finishedLoadAnArticleWithStatut:(RequestStatut)aStatut andItem:(TTPost *)item;
- (void) finishedLoadSearchWithStatut:(RequestStatut)aStatut andJSONArray:(NSArray *)aJSON;
- (void) finishedLoadCommentairesWithStatut:(RequestStatut)aStatut andJSONArray:(NSArray *)aJSON;
- (void) finishedLoadMoreCommentairesWithStatut:(RequestStatut)aStatut andJSONArray:(NSArray *)aJSON;
- (void) finishedPostCommentWithStatut:(RequestStatut)aStatut;

// WooCommerce
- (void) finishedLoadArticlesFromWooCommerceWithStatut:(RequestStatut)aStatut andJSONArray:(NSArray *)aJSON;
- (void) finishedLoadLast25ArticlesFromWooCommerceWithStatut:(RequestStatut)aStatut andJSONArray:(NSArray *)aJSON;

// APIJSON
- (void) finishedLoadAPIJSONArticlesWithStatut:(RequestStatut)aStatut andJSONArray:(NSArray *)aJSON;
- (void) finishedLoadMoreAPIJSONArticlesWithStatut:(RequestStatut)aStatut andJSONArray:(NSArray *)aJSON;
@end

@interface WPAPIRequest : NSObject <ASIHTTPRequestDelegate, UIAlertViewDelegate> {
}

@property (nonatomic, strong) id<WPAPIRequestDelegate>delegate;
@property (nonatomic, strong) id<WPAPIRequestDelegate>articlesDelegate;
@property (nonatomic, strong) id<WPAPIRequestDelegate>searchDelegate;
@property (nonatomic, strong) id<WPAPIRequestDelegate>commentairesDelegate;
@property (nonatomic, strong) id<WPAPIRequestDelegate>wooCommerceDelegate;
@property (nonatomic, strong) id<WPAPIRequestDelegate>wooCommerceTousDelegate;
@property (nonatomic, strong) id<WPAPIRequestDelegate>apiJsonDelegate;

+ (WPAPIRequest*) shared;

// WordPress

- (void) loadArticlesForType:(ArticlesType)aType withDelegate:(id)delegate;
- (void) loadLast25ArticlesForType:(ArticlesType)aType withDelegate:(id)delegate;

- (void) loadMoreArticlesForType:(ArticlesType)aType andPage:(int)page withDelegate:(id)delegate;
- (void) loadOneArticleWithID:(int)articleID andArticleType:(ArticleType)type withDelegate:(id)delegate;

- (void) makeSearchWithString:(NSString*)mySearch withDelegate:(id)delegate;
- (void) makeSearchWithString:(NSString *)mySearch andAPIType:(APIType)type withDelegate:(id)delegate;

- (void) loadCommentairesForArticleID:(int)astuceID withDelegate:(id)delegate;
- (void) loadMoreCommentairesForArticleID:(int)astuceID andDate:(NSString*)date withDelegate:(id)delegate;
- (void) postCommentOnPluggin:(WPComment*)aComment andArticleID:(int)articleID WithDelegate:(id)delegate;

// WooCommerce

- (void) loadArticlesFromWooCommerceForCategory:(NSString*)category withDelegate:(id)delegate;
- (void) loadLast25ArticlesFromWooCommerceWithDelegate:(id)delegate;

// API JSON

- (void) loadAPIJSONArticlesForType:(ArticlesType)aType andAPIType:(APIType)apiType withDelegate:(id)delegate;
- (void) loadLast5APIJSONArticlesForType:(ArticlesType)aType andAPIType:(APIType)apiType withDelegate:(id)delegate;

- (void) loadMoreAPIJSONArticlesForType:(ArticlesType)aType andAPIType:(APIType)apiType andPage:(int)page withDelegate:(id)delegate;

@end

