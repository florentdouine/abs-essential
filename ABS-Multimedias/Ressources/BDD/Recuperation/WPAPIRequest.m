//
//  WPAPIRequest.m
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import "WPAPIRequest.h"
#import "WPAppDelegate.h"
#import <TTRequest/ASIHTTPRequest+OAuth.h>
#import <TTRequest/JSONKit.h>
#import <TTCommons/NSString+HTML.h>
#import <TTCommons/NSString+URLEncoding.h>
#import <TTRequest/JSON.h>

@implementation WPAPIRequest
@synthesize delegate = _delegate;
@synthesize articlesDelegate = _articlesDelegate;
@synthesize searchDelegate = _searchDelegate;
@synthesize commentairesDelegate = _commentairesDelegate;
@synthesize wooCommerceDelegate = _wooCommerceDelegate;
@synthesize wooCommerceTousDelegate = _wooCommerceTousDelegate;
@synthesize apiJsonDelegate = _apiJsonDelegate;

- (id) init {
    
    self = [super init];
    
    if (self) {
    }
    
    return self;
}

#pragma mark - singleton

+ (WPAPIRequest*) shared {
    
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

#pragma mark - Wordpress
#pragma mark Categories

- (NSString*) setUrlString:(NSString*)stringRequest forType:(ArticlesType)aType {
    
    NSString *urlString = [NSString stringWithFormat:kAPIRequest, stringRequest];
    
    if (aType == ArticlesAll) {
        return urlString;
    }
    urlString = [self addType:aType toUrl:urlString];
    return urlString;
}

- (NSString*) addType:(ArticlesType)aType toUrl:(NSString*)urlString {
    
    if (aType == ArticlesAll) {
        return urlString;
    }
    NSArray *categories = kAPIArrayCategories;
    urlString = [NSString stringWithFormat:@"%@&filter[category_name]=%@", urlString, [categories objectAtIndex:aType-1]];
    return urlString;
}

#pragma mark Load Articles

- (void)loadLast25ArticlesForType:(ArticlesType)aType withDelegate:(id)delegate {
    [self loadArticlesForType:aType andNumber:5 withDelegate:delegate];
}

- (void)loadArticlesForType:(ArticlesType)aType withDelegate:(id)delegate {
    [self loadArticlesForType:aType andNumber:10 withDelegate:delegate];
}

- (void) loadArticlesForType:(ArticlesType)aType andNumber:(int)nb withDelegate:(id)delegate {
    
    _articlesDelegate = delegate;
    
    NSString *urlString = [self setUrlString:[NSString stringWithFormat:@"posts/?filter[posts_per_page]=%i", nb] forType:aType];
//    DLog(@"URLString : %@", urlString);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlString]];

    [request setRequestMethod:@"GET"];
    [request setValidatesSecureCertificate:YES];
    [request setDelegate:self];
    [request setTimeOutSeconds:30.0];
    [request setNumberOfTimesToRetryOnTimeout:1];
    [request setDidFinishSelector:@selector(requestFinishedLoadArticles:)];
    [request setDidFailSelector:@selector(requestFailedLoadArticles:)];
    [request startAsynchronous];
}

- (void)requestFinishedLoadArticles:(ASIHTTPRequest*)request {
    
    NSArray *jsonDict = [[request responseString] objectFromJSONString];
//    DLog(@"Requête Succeeded : %@", jsonDict);
    
    RequestStatut aStatut = RequestStatutFailed;
    if ([jsonDict isKindOfClass:[NSArray class]]) {
        aStatut = RequestStatutSucceeded;
    }
    
    NSArray *items = [[WPPlistRecup shared] getPostsFromAPI:jsonDict];
    
    if ([_articlesDelegate respondsToSelector:@selector(finishedLoadArticlesWithStatut:andJSONArray:)]) {
        [_articlesDelegate finishedLoadArticlesWithStatut:aStatut andJSONArray:items];
    }
}

- (void)requestFailedLoadArticles:(ASIHTTPRequest*)request {
    DLog(@"Requête Failed with error : %@", [request error]);
    if ([_articlesDelegate respondsToSelector:@selector(finishedLoadArticlesWithStatut:andJSONArray:)]) {
        [_articlesDelegate finishedLoadArticlesWithStatut:RequestStatutFailed andJSONArray:nil];
    }
}

#pragma mark Load More Articles

- (void) loadMoreArticlesForType:(ArticlesType)aType andPage:(int)page withDelegate:(id)delegate {
 
    _articlesDelegate = delegate;
    
    NSString *urlString = [self setUrlString:[NSString stringWithFormat:@"posts?filter[posts_per_page]=10&page=%i&filter[orderby]=date&filter[order]=desc", page] forType:aType];
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    [request setRequestMethod:@"GET"];
    [request setValidatesSecureCertificate:YES];
    [request setDelegate:self];
    [request setTimeOutSeconds:30.0];
    [request setNumberOfTimesToRetryOnTimeout:1];
    [request setDidFinishSelector:@selector(requestFinishedLoadMoreArticles:)];
    [request setDidFailSelector:@selector(requestFailedLoadMoreArticles:)];
    [request startAsynchronous];
    
}

- (void)requestFinishedLoadMoreArticles:(ASIHTTPRequest*)request {
    
    NSArray *jsonDict = [[request responseString] objectFromJSONString];
    DLog(@"Requête Succeeded : %@", jsonDict);
    
    RequestStatut aStatut = RequestStatutFailed;
    if ([jsonDict isKindOfClass:[NSArray class]]) {
        aStatut = RequestStatutSucceeded;
    }
    
    NSArray *items = [[WPPlistRecup shared] getPostsFromAPI:jsonDict];
    
    if ([_articlesDelegate respondsToSelector:@selector(finishedLoadMoreArticlesWithStatut:andJSONArray:)]) {
        [_articlesDelegate finishedLoadMoreArticlesWithStatut:aStatut andJSONArray:items];
    }
}

- (void)requestFailedLoadMoreArticles:(ASIHTTPRequest*)request {
    
    DLog(@"Requête Failed with error : %@", [request error]);

    if ([_articlesDelegate respondsToSelector:@selector(finishedLoadMoreArticlesWithStatut:andJSONArray:)]) {
        [_articlesDelegate finishedLoadMoreArticlesWithStatut:RequestStatutFailed andJSONArray:nil];
    }
}

#pragma mark Load On Article

- (void) loadOneArticleWithID:(int)articleID andArticleType:(ArticleType)type withDelegate:(id)delegate {
    
    _delegate = delegate;
    
    NSString *urlString = [NSString stringWithFormat:kAPIRequest, [NSString stringWithFormat:@"posts/%i", articleID]];
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    [request setRequestMethod:@"GET"];
    [request setValidatesSecureCertificate:YES];
    [request setDelegate:self];
    [request setTimeOutSeconds:30.0];
    [request setNumberOfTimesToRetryOnTimeout:1];
    [request setDidFinishSelector:@selector(requestFinishedLoadOneArticle:)];
    [request setDidFailSelector:@selector(requestFailedLoadOneArticle:)];
    [request startAsynchronous];
}

- (void)requestFinishedLoadOneArticle:(ASIHTTPRequest*)request {

    NSDictionary *jsonDict = [[request responseString] objectFromJSONString];
    //    DLog(@"Requête Succeeded : %@", jsonDict);
    
    RequestStatut aStatut = RequestStatutFailed;
    TTPost *item = [TTPost new];
    if ([jsonDict isKindOfClass:[NSDictionary class]]) {
        if ([[jsonDict objectForKey:@"code"] isKindOfClass:[NSNull class]]) {
            aStatut = RequestStatutFailed;
        } else {
            aStatut = RequestStatutSucceeded;
            item = [[WPPlistRecup shared] getOneItemFromDict:jsonDict];
        }
    }
    
    if ([_delegate respondsToSelector:@selector(finishedLoadAnArticleWithStatut:andItem:)]) {
        [_delegate finishedLoadAnArticleWithStatut:aStatut andItem:item];
    }
}

- (void)requestFailedLoadOneArticle:(ASIHTTPRequest*)request {
    
    DLog(@"Requête Failed with error : %@", [request error]);

    if ([_delegate respondsToSelector:@selector(finishedLoadAnArticleWithStatut:andItem:)]) {
        [_delegate finishedLoadAnArticleWithStatut:RequestStatutFailed andItem:nil];
    }
}

#pragma mark - Search

- (void)makeSearchWithString:(NSString *)mySearch withDelegate:(id)delegate {
    [self makeSearchWithString:mySearch andAPIType:APIPost withDelegate:delegate];
}

- (void)makeSearchWithString:(NSString *)mySearch andAPIType:(APIType)type withDelegate:(id)delegate {
    
    _searchDelegate = delegate;
    
    mySearch = [mySearch stringByEncodingHTMLEntities];
    mySearch = [mySearch lowercaseString];
    mySearch = [mySearch stringByAddingPercentEscapesUsingEncoding:NSStringEncodingConversionAllowLossy];
    
    NSString *urlString;
    if (type == APIAnnuaire) {
        urlString = [NSString stringWithFormat:kAPIAnnuaireRequest, [NSString stringWithFormat:@"posts/?filter[posts_per_page]=10&filter[s]=%@&type[]=livre-audio", mySearch]];
    } else {
        urlString = [NSString stringWithFormat:kAPIRequest, [NSString stringWithFormat:@"posts/?filter[posts_per_page]=20&filter[s]=%@&type[]=abs_podcast&type[]=post", mySearch]];
    }
//    DLog(@"Url : %@", urlString);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlString]];

    [request setRequestMethod:@"GET"];
    [request setValidatesSecureCertificate:YES];
    [request setDelegate:self];
    [request setTimeOutSeconds:30.0];
    [request setNumberOfTimesToRetryOnTimeout:1];
    [request setDidFinishSelector:@selector(requestFinishedMakeSearch:)];
    [request setDidFailSelector:@selector(requestFailedMakeSearch:)];
    [request startAsynchronous];
}

- (void)requestFinishedMakeSearch:(ASIHTTPRequest*)request {
    
    NSArray *jsonDict = [[request responseString] objectFromJSONString];
    //    DLog(@"Requête Succeeded : %@", jsonDict);
    
    RequestStatut aStatut = RequestStatutFailed;
    if ([jsonDict isKindOfClass:[NSArray class]]) {
        aStatut = RequestStatutSucceeded;
    }
    
    NSArray *items = [[WPPlistRecup shared] getPostsFromAPI:jsonDict];
    
    if ([_searchDelegate respondsToSelector:@selector(finishedLoadSearchWithStatut:andJSONArray:)]) {
        [_searchDelegate finishedLoadSearchWithStatut:aStatut andJSONArray:items];
    }
}

- (void)requestFailedMakeSearch:(ASIHTTPRequest*)request {
    
    DLog(@"Requête Failed with error : %@", [request error]);
    
    if ([_searchDelegate respondsToSelector:@selector(finishedLoadSearchWithStatut:andJSONArray:)]) {
        [_searchDelegate finishedLoadSearchWithStatut:RequestStatutFailed andJSONArray:nil];
    }
}

#pragma mark Load Commentaires

- (void) loadCommentairesForArticleID:(int)astuceID withDelegate:(id)delegate {
    
    _commentairesDelegate = delegate;
    
    NSString *urlString = [NSString stringWithFormat:kAPIRequest, [NSString stringWithFormat:@"comments/?filter[per_page]=20&filter[post]=%i", astuceID]];
    DLog(@"%@", urlString);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlString]];

    [request setValidatesSecureCertificate:YES];
    [request setDelegate:self];
    [request setTimeOutSeconds:30.0];
    [request setNumberOfTimesToRetryOnTimeout:1];
    [request setDidFinishSelector:@selector(requestFinishedLoadCommentaires:)];
    [request setDidFailSelector:@selector(requestFailedLoadCommentaires:)];
    [request startAsynchronous];
}

- (void)requestFinishedLoadCommentaires:(ASIHTTPRequest*)request {
    
    NSDictionary *jsonDict = [[request responseString] objectFromJSONString];
    //    DLog(@"Requête Succeeded : %@", jsonDict);
    
    RequestStatut aStatut = RequestStatutFailed;
    if ([jsonDict isKindOfClass:[NSDictionary class]]) {
        aStatut = RequestStatutSucceeded;
    }
    
//    if ([_commentairesDelegate respondsToSelector:@selector(finishedLoadCommentairesWithStatut:andJSONArray:)]) {
//        [_commentairesDelegate finishedLoadCommentairesWithStatut:aStatut andJSONArray:jsonDict];
//    }
}

- (void)requestFailedLoadCommentaires:(ASIHTTPRequest*)request {
    
    DLog(@"Requête Failed with error : %@", [request error]);

    if ([_commentairesDelegate respondsToSelector:@selector(finishedLoadCommentairesWithStatut:andJSONArray:)]) {
        [_commentairesDelegate finishedLoadCommentairesWithStatut:RequestStatutFailed andJSONArray:nil];
    }
}

#pragma mark Load More Commentaires

- (void) loadMoreCommentairesForArticleID:(int)astuceID andDate:(NSString *)date withDelegate:(id)delegate {

    _commentairesDelegate = delegate;
    
    // Date
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    NSDate *theDate = [df dateFromString:date];
    theDate = [theDate dateByAddingTimeInterval:-1];
    
    NSDateFormatter *dayFormatterSecond = [[NSDateFormatter alloc] init];
    [dayFormatterSecond setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    date = [dayFormatterSecond stringFromDate:theDate];
    //    DLog(@"date : %@", date);
    date = [date stringByReplacingOccurrencesOfString:@":" withString:@"%3A"];
    date = [date stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    
    NSString *urlString = [NSString stringWithFormat:@"https://public-api.wordpress.com/rest/v1/sites/%@/posts/%i/replies/?number=20&pretty=1&before=%@&status=approved", kAPIWebsite, astuceID, date];
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlString]];

    [request setValidatesSecureCertificate:YES];
    [request setDelegate:self];
    [request setTimeOutSeconds:30.0];
    [request setNumberOfTimesToRetryOnTimeout:1];
    [request setDidFinishSelector:@selector(requestFinishedLoadMoreCommentaires:)];
    [request setDidFailSelector:@selector(requestFailedLoadMoreCommentaires:)];
    [request startAsynchronous];
    
}

- (void)requestFinishedLoadMoreCommentaires:(ASIHTTPRequest*)request {
    
    NSDictionary *jsonDict = [[request responseString] objectFromJSONString];
    //    DLog(@"Requête Succeeded : %@", jsonDict);
    
    RequestStatut aStatut = RequestStatutFailed;
    if ([jsonDict isKindOfClass:[NSDictionary class]]) {
        aStatut = RequestStatutSucceeded;
    }
    
//    if ([_commentairesDelegate respondsToSelector:@selector(finishedLoadMoreCommentairesWithStatut:andJSONArray:)]) {
//        [_commentairesDelegate finishedLoadMoreCommentairesWithStatut:aStatut andJSONArray:jsonDict];
//    }
}

- (void)requestFailedLoadMoreCommentaires:(ASIHTTPRequest*)request {
    
    DLog(@"Requête Failed with error : %@", [request error]);

    if ([_commentairesDelegate respondsToSelector:@selector(finishedLoadMoreCommentairesWithStatut:andJSONArray:)]) {
        [_commentairesDelegate finishedLoadMoreCommentairesWithStatut:RequestStatutFailed andJSONArray:nil];
    }
}

- (void)postCommentOnPluggin:(WPComment *)aComment andArticleID:(int)articleID WithDelegate:(id)delegate {
    
    _commentairesDelegate = delegate;
    
    NSString *urlString = [NSString stringWithFormat:@"http://www.abs-multimedias.com/sendmail/mail-postcomment-abs.php"];
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
    NSNumber *timestamp = [NSNumber numberWithInt: timeStamp];
    
    [request setPostValue:@"yjjf56432100xcbn" forKey:@"aTokenAcces"];
    [request setPostValue:timestamp forKey:@"aTime"];
    [request setPostValue:[aComment name] forKey:@"aName"];
    [request setPostValue:[aComment courriel] forKey:@"aCourriel"];
    [request setPostValue:[aComment message] forKey:@"aMessage"];
    [request setPostValue:[NSString stringWithFormat:@"%i", articleID] forKey:@"anArticleID"];

    [request setDelegate:self];
    [request setTimeOutSeconds:30.0];
    [request setNumberOfTimesToRetryOnTimeout:1];
    [request setDidFinishSelector:@selector(requestFinishedPostPlugginCommentaires:)];
    [request setDidFailSelector:@selector(requestFailedPostCommentaires:)];
    [request startAsynchronous];
}

- (void)requestFinishedPostPlugginCommentaires:(ASIHTTPRequest*)request {
    
//    DLog(@"Requête Succeeded : %@", [request responseString]);
    
    if ([_commentairesDelegate respondsToSelector:@selector(finishedPostCommentWithStatut:)]) {
        [_commentairesDelegate finishedPostCommentWithStatut:RequestStatutSucceeded];
    }
}

- (void)requestFailedPostCommentaires:(ASIHTTPRequest*)request {
    
    DLog(@"Requête Failed with error : %@", [request error]);
    if ([_commentairesDelegate respondsToSelector:@selector(finishedPostCommentWithStatut:)]) {
        [_commentairesDelegate finishedPostCommentWithStatut:RequestStatutFailed];
    }
}

#pragma mark - WooCommerce
#pragma mark Load Articles

- (void) loadArticlesFromWooCommerceForCategory:(NSString *)category withDelegate:(id)delegate {

    _wooCommerceDelegate = delegate;
    
    NSString *urlString = [NSString stringWithFormat:@"https://www.abs-multimedias.com/wc-api/v3/products?filter[category]=%@", category];
    DLog(@"URLString : %@", urlString);
    //https://www.abs-multimedias.com/boutique/wc-api/v3/products?filter[category]=slug
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlString]];
    [request addBasicAuthenticationHeaderWithUsername:kClientIDWC andPassword:kClientSecretWC];
    
    [request setRequestMethod:@"GET"];
    [request setDelegate:self];
    [request setTimeOutSeconds:30.0];
    [request setValidatesSecureCertificate:YES];
    [request setNumberOfTimesToRetryOnTimeout:1];
    [request setDidFinishSelector:@selector(requestFinishedLoadArticlesFromWooCommerce:)];
    [request setDidFailSelector:@selector(requestFailedLoadArticlesFromWooCommerce:)];
    [request startAsynchronous];
}

- (void)requestFinishedLoadArticlesFromWooCommerce:(ASIHTTPRequest*)request {
   
    NSDictionary *jsonDict = [[request responseString] objectFromJSONString];
//    DLog(@"Requête Succeeded : %@", jsonDict);
    
    RequestStatut aStatut = RequestStatutFailed;
    if ([jsonDict isKindOfClass:[NSDictionary class]]) {
        aStatut = RequestStatutSucceeded;
    }
    
    NSArray *items = [[WPPlistRecup shared] getPostsFromAPI:[jsonDict objectForKey:@"products"] andType:APIWoocommerce];
    
    if ([_wooCommerceDelegate respondsToSelector:@selector(finishedLoadArticlesFromWooCommerceWithStatut:andJSONArray:)]) {
        [_wooCommerceDelegate finishedLoadArticlesFromWooCommerceWithStatut:aStatut andJSONArray:items];
    }
}

- (void)requestFailedLoadArticlesFromWooCommerce:(ASIHTTPRequest*)request {
    
    DLog(@"Requête Failed with error : %@", [request error]);
    if ([_wooCommerceDelegate respondsToSelector:@selector(finishedLoadArticlesFromWooCommerceWithStatut:andJSONArray:)]) {
        [_wooCommerceDelegate finishedLoadArticlesFromWooCommerceWithStatut:RequestStatutFailed andJSONArray:nil];
    }
}


#pragma mark Load Last Articles

- (void) loadLast25ArticlesFromWooCommerceWithDelegate:(id)delegate {
    
    _wooCommerceTousDelegate = delegate;
    
    NSString *urlString = @"https://www.abs-multimedias.com/wc-api/v3/products?filter[limit]=25";
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlString]];
    [request addBasicAuthenticationHeaderWithUsername:kClientIDWC andPassword:kClientSecretWC];
    
    [request setRequestMethod:@"GET"];
    [request setDelegate:self];
    [request setTimeOutSeconds:30.0];
    [request setValidatesSecureCertificate:YES];
    [request setNumberOfTimesToRetryOnTimeout:1];
    [request setDidFinishSelector:@selector(requestFinishedLoadLast25ArticlesFromWooCommerce:)];
    [request setDidFailSelector:@selector(requestFailedLoadLast25ArticlesFromWooCommerce:)];
    [request startAsynchronous];
}

- (void)requestFinishedLoadLast25ArticlesFromWooCommerce:(ASIHTTPRequest*)request {
    
    NSDictionary *jsonDict = [[request responseString] objectFromJSONString];
    DLog(@"Requête Succeeded : %@", jsonDict);
    
    RequestStatut aStatut = RequestStatutFailed;
    if ([jsonDict isKindOfClass:[NSDictionary class]]) {
        aStatut = RequestStatutSucceeded;
    }
    
    NSArray *items = [[WPPlistRecup shared] getPostsFromAPI:[jsonDict objectForKey:@"products"] andType:APIWoocommerce];
    
    if ([_wooCommerceTousDelegate respondsToSelector:@selector(finishedLoadLast25ArticlesFromWooCommerceWithStatut:andJSONArray:)]) {
        [_wooCommerceTousDelegate finishedLoadLast25ArticlesFromWooCommerceWithStatut:aStatut andJSONArray:items];
    }
}

- (void)requestFailedLoadLast25ArticlesFromWooCommerce:(ASIHTTPRequest*)request {
    
    DLog(@"Requête Failed with error : %@", [request error]);
    if ([_wooCommerceTousDelegate respondsToSelector:@selector(finishedLoadLast25ArticlesFromWooCommerceWithStatut:andJSONArray:)]) {
        [_wooCommerceTousDelegate finishedLoadLast25ArticlesFromWooCommerceWithStatut:RequestStatutFailed andJSONArray:nil];
    }
}

#pragma mark - API JSON

- (NSString*) addJSONType:(ArticlesType)aType toUrl:(NSString*)urlString {
    
    if (aType == ArticlesAll) {
        return urlString;
    }
    NSArray *categories = kAPIArrayPodcatsName;
    urlString = [NSString stringWithFormat:@"%@&filter[abs_rub]=%@", urlString, [categories objectAtIndex:aType-1]];
    return urlString;
}

#pragma mark Load Articles

- (void)loadLast5APIJSONArticlesForType:(ArticlesType)aType andAPIType:(APIType)apiType withDelegate:(id)delegate {
    [self loadAPIJSONArticlesForType:aType andAPIType:apiType andNumber:5 withDelegate:delegate];
}

- (void)loadAPIJSONArticlesForType:(ArticlesType)aType andAPIType:(APIType)apiType withDelegate:(id)delegate {
    [self loadAPIJSONArticlesForType:aType andAPIType:apiType andNumber:10 withDelegate:delegate];
}

- (void) loadAPIJSONArticlesForType:(ArticlesType)aType andAPIType:(APIType)apiType andNumber:(int)nb withDelegate:(id)delegate {
    
    _apiJsonDelegate = delegate;

    NSString *type = @"";
    NSString *url = [NSString stringWithFormat:kAPIRequest, @""];
    switch (apiType) {
        case APIAnnuaire:
            type = @"livre-audio";
            url = [NSString stringWithFormat:kAPIAnnuaireRequest, @""];
            break;
        case APIPodcast:
            type = @"abs_podcast";
            break;
        case APILivresAudios:
            type = @"abs_livreaudio";
            break;
            
        default:
            break;
    }
    
    NSString *urlString = [self addJSONType:aType toUrl:[NSString stringWithFormat:@"%@posts?type[]=%@&filter[posts_per_page]=%i&page=1&filter[orderby]=date&filter[order]=desc", url, type, nb]];
    DLog(@"URLString : %@", urlString);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    [request setRequestMethod:@"GET"];
    [request setValidatesSecureCertificate:YES];
    [request setDelegate:self];
    [request setTimeOutSeconds:30.0];
    [request setNumberOfTimesToRetryOnTimeout:1];
    [request setDidFinishSelector:@selector(requestFinishedLoadAPIJSONArticles:)];
    [request setDidFailSelector:@selector(requestFailedLoadAPIJSONArticles:)];
    [request startAsynchronous];
}

- (void)requestFinishedLoadAPIJSONArticles:(ASIHTTPRequest*)request {
    
    NSArray *jsonDict = [[request responseString] objectFromJSONString];
    //        DLog(@"Requête Succeeded : %@", jsonDict);
    
    RequestStatut aStatut = RequestStatutFailed;
    if ([jsonDict isKindOfClass:[NSArray class]]) {
        aStatut = RequestStatutSucceeded;
    }
    
    NSArray *items = [[WPPlistRecup shared] getPostsFromAPI:jsonDict];
    
    if ([_apiJsonDelegate respondsToSelector:@selector(finishedLoadAPIJSONArticlesWithStatut:andJSONArray:)]) {
        [_apiJsonDelegate finishedLoadAPIJSONArticlesWithStatut:aStatut andJSONArray:items];
    }
}

- (void)requestFailedLoadAPIJSONArticles:(ASIHTTPRequest*)request {
    
    DLog(@"Requête Failed with error : %@", [request error]);
    if ([_apiJsonDelegate respondsToSelector:@selector(finishedLoadAPIJSONArticlesWithStatut:andJSONArray:)]) {
        [_apiJsonDelegate finishedLoadAPIJSONArticlesWithStatut:RequestStatutFailed andJSONArray:nil];
    }
}

#pragma mark Load More Articles

- (void) loadMoreAPIJSONArticlesForType:(ArticlesType)aType andAPIType:(APIType)apiType andPage:(int)page withDelegate:(id)delegate {
    
    _apiJsonDelegate = delegate;
    
    NSString *type = @"";
    NSString *url = [NSString stringWithFormat:kAPIRequest, @""];
    switch (apiType) {
        case APIAnnuaire:
            type = @"livre-audio";
            url = [NSString stringWithFormat:kAPIAnnuaireRequest, @""];
            break;
        case APIPodcast:
            type = @"abs_podcast";
            break;
        case APILivresAudios:
            type = @"abs_livreaudio";
            break;
            
        default:
            break;
    }
    
    NSString *urlString = [self addJSONType:aType toUrl:[NSString stringWithFormat:@"%@posts?type[]=%@&filter[posts_per_page]=10&page=%i&filter[orderby]=date&filter[order]=desc", url, type, page]];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    [request setRequestMethod:@"GET"];
    [request setValidatesSecureCertificate:YES];
    [request setDelegate:self];
    [request setTimeOutSeconds:30.0];
    [request setNumberOfTimesToRetryOnTimeout:1];
    [request setDidFinishSelector:@selector(requestFinishedLoadMoreAPIJSONArticles:)];
    [request setDidFailSelector:@selector(requestFailedLoadMoreAPIJSONArticles:)];
    [request startAsynchronous];
    
}

- (void)requestFinishedLoadMoreAPIJSONArticles:(ASIHTTPRequest*)request {
    
    NSArray *jsonDict = [[request responseString] objectFromJSONString];
    //    DLog(@"Requête Succeeded : %@", jsonDict);
    
    RequestStatut aStatut = RequestStatutFailed;
    if ([jsonDict isKindOfClass:[NSArray class]]) {
        aStatut = RequestStatutSucceeded;
    }
    
    NSArray *items = [[WPPlistRecup shared] getPostsFromAPI:jsonDict];
    
    if ([_apiJsonDelegate respondsToSelector:@selector(finishedLoadMoreAPIJSONArticlesWithStatut:andJSONArray:)]) {
        [_apiJsonDelegate finishedLoadMoreAPIJSONArticlesWithStatut:aStatut andJSONArray:items];
    }
}

- (void)requestFailedLoadMoreAPIJSONArticles:(ASIHTTPRequest*)request {
    
    DLog(@"Requête Failed with error : %@", [request error]);
    if ([_apiJsonDelegate respondsToSelector:@selector(finishedLoadMoreAPIJSONArticlesWithStatut:andJSONArray:)]) {
        [_apiJsonDelegate finishedLoadMoreAPIJSONArticlesWithStatut:RequestStatutFailed andJSONArray:nil];
    }
}


@end
