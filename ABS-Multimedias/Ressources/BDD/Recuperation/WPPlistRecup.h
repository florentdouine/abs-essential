//
//  WPPlistRecup.h
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Header;
@class Favoris;

typedef enum {
    APIWPJSON = 0,
    APIWoocommerce = 1
} PostType;

@interface WPPlistRecup : NSObject

+ (WPPlistRecup*)shared;

#pragma mark - Menu

- (NSArray *) getListOfHeaders;
- (Header*)getHeaderForPosition:(int)position;

#pragma mark - Favoris

- (NSArray *) getListOfFavoris;
- (BOOL) ajouterUnFavoris:(TTPost *)aFavoris;
- (void) retirerUnFavorisForID:(int)articleID;
- (BOOL) isFavorisForID:(int)articleID;

#pragma mark - Noter App

- (BOOL) getNoterApp;
- (void) setNoterApp:(int)isNoterApp;

- (NSString*) getCurrentVersion;
- (void) setCurrentVersion;

- (int) getNbOuvertures;

#pragma mark - Articles 

- (BOOL) isInterstitialWithFrequence:(int)frequence;

#pragma mark Read

- (void) setIsReadForArticleID:(int)articleID;
- (BOOL) getIsReadForArticleID:(int)articleID;

#pragma mark - Commentaires

- (void) sauvCommentaireReglagesWithEmail:(NSString*)email andNom:(NSString*)nom;
- (NSDictionary*) getCommentaireReglages;

#pragma mark - Audio

- (BOOL) getReglageSon;
- (void) setReglageSon:(BOOL)isSound;

#pragma mark - First Open

- (BOOL) isFirstOpen;
- (void) setFirstOpenToNo;

#pragma mark - Models

- (NSArray*) getPostsFromAPI:(NSArray*)json;
- (NSArray*) getPostsFromAPI:(NSArray*)json andType:(PostType)type;
- (TTPost*) getOneItemFromDict:(NSDictionary*)dict;

@end
