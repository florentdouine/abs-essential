//
//  WPPlistRecup.m
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import "WPPlistRecup.h"
#import <TTCommons/GestionFichiers.h>
#import <TTCommons/NSString+HTML.h>

@implementation WPPlistRecup

#pragma mark - singleton

+ (WPPlistRecup*)shared {
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

#pragma mark - Menu

- (NSArray *)getListOfHeaders{

    NSString *plistPath = [[GestionFichiers shared] creationFichierPlist:[NSString stringWithFormat:@"tabbar_%@_%@", kMenuType, kMenuCurrentVersion]];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    
    NSMutableArray *mutArr = [NSMutableArray new];
    
    for (int i = 1; i<=[[dict allKeys] count]; i++) {
        
        NSDictionary *aDict = [dict objectForKey:[NSString stringWithFormat:@"%i", i]];
        
        Header *aHeader = [Header new];
        [aHeader setId_header:i];
        [aHeader setTitre:[aDict objectForKey:@"header"]];
        
        if ([[aHeader titre] isEqualToString:@"Navigation"]) {
            [aHeader setListOfTitre:kArrayCategoriesName];
            
        } else if ([[aHeader titre] isEqualToString:@"Social"]) {
            NSMutableArray *arr = [NSMutableArray arrayWithArray:[aDict objectForKey:@"titles"]];
            if (kUrlFacebook == nil) {
                [arr removeObjectAtIndex:1];
            }
            [aHeader setListOfTitre:arr];
            
        } else {
            [aHeader setListOfTitre:[aDict objectForKey:@"titles"]];
        }
        
        [mutArr addObject:aHeader];
    }
    
    return (NSArray*)mutArr;
}

- (Header*)getHeaderForPosition:(int)position {
    
    NSString *plistPath = [[GestionFichiers shared] creationFichierPlist:[NSString stringWithFormat:@"tabbar_%@_%@", kMenuType, kMenuCurrentVersion]];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    
    NSDictionary *aDict = [dict objectForKey:[NSString stringWithFormat:@"%i", position]];
    
    Header *aHeader = [Header new];
    [aHeader setId_header:position];
    [aHeader setTitre:[aDict objectForKey:@"header"]];
    
    if ([[aHeader titre] isEqualToString:@"Navigation"]) {
        [aHeader setListOfTitre:kArrayCategoriesName];
        
    } else {
        [aHeader setListOfTitre:[aDict objectForKey:@"titles"]];
    }

    
    return aHeader;
}

#pragma mark - Favoris

- (NSArray *)getListOfFavoris {
    
    NSString *plistPath = [[GestionFichiers shared] creationFichierPlist:@"favoris_post"];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    
    NSMutableArray *mutArr = [NSMutableArray new];
    
    NSArray *keys = [dict allKeys];
    
    for (NSString *key in keys) {
        
        NSDictionary *aDict = [dict objectForKey:key];
        
        TTPost *item = [TTPost new];
        
        [item setPostID:[[aDict objectForKey:@"postID"] intValue]];
        [item setURLFeatureImg:[aDict objectForKey:@"URLFeatureImg"]];
        [item setCategories:[aDict objectForKey:@"categories"]];
        [item setTitre:[aDict objectForKey:@"titre"]];
        [item setNbComments:[[aDict objectForKey:@"nbComments"] intValue]];
        [item setIsCommentsOpen:[[aDict objectForKey:@"isCommentsOpen"] intValue]];
        
        [item setDate:[aDict objectForKey:@"date"]];
        [item setDateStr:[aDict objectForKey:@"dateStr"]];
        
        [item setIsCompletePost:[[aDict objectForKey:@"isCompletePost"] boolValue]];
        [item setInStock:[[aDict objectForKey:@"inStock"] boolValue]];
        [item setContent:[aDict objectForKey:@"content"]];
        [item setShortContent:[aDict objectForKey:@"shortContent"]];
        
        [item setAuteur:[aDict objectForKey:@"auteur"]];
        [item setNarrateur:[aDict objectForKey:@"narrateur"]];
        [item setURLArticle:[aDict objectForKey:@"URLArticle"]];
        [item setURLEditor:[aDict objectForKey:@"URLEditor"]];
        [item setPresentateurs:[aDict objectForKey:@"presentateurs"]];
        [item setInvites:[aDict objectForKey:@"invites"]];
        
        [item setURLMp3File:[aDict objectForKey:@"URLMp3File"]];
        [item setCodeISRC:[aDict objectForKey:@"codeISRC"]];
        
        [item setAuteur2:[aDict objectForKey:@"auteur2"]];
        [item setEditeur:[aDict objectForKey:@"editeur"]];
        [item setPrix:[aDict objectForKey:@"prix"]];
        [item setIdentifier:[aDict objectForKey:@"identifier"]];
        
        [item setDuree:[aDict objectForKey:@"duree"]];
        [item setTags:[aDict objectForKey:@"tags"]];
        
        [item setType:(APIType)[[aDict objectForKey:@"type"] intValue]];
        
        [mutArr addObject:item];
    }
    
    return (NSArray*)mutArr;
}

- (BOOL)ajouterUnFavoris:(TTPost *)aFavoris {
    
    if (aFavoris == nil || [aFavoris postID] == 0) {
        return NO;
    }
    
    NSString *plistPath = [[GestionFichiers shared] creationFichierPlist:@"favoris_post"];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    
    NSMutableDictionary *favDict = [[NSMutableDictionary alloc] init];
    [favDict setObject:[NSString stringWithFormat:@"%i", [aFavoris postID]] forKey:@"postID"];
    if ([aFavoris URLFeatureImg] != nil) {
        [favDict setObject:[aFavoris URLFeatureImg] forKey:@"URLFeatureImg"];
    }
    if ([aFavoris categories] != nil) {
        [favDict setObject:[aFavoris categories] forKey:@"categories"];
    }
    if ([aFavoris titre] != nil) {
        [favDict setObject:[aFavoris titre] forKey:@"titre"];
    }
    [favDict setObject:[NSString stringWithFormat:@"%i", [aFavoris nbComments]] forKey:@"nbComments"];
    [favDict setObject:[NSString stringWithFormat:@"%i", [aFavoris isCommentsOpen]] forKey:@"isCommentsOpen"];
    
    [favDict setObject:[aFavoris date] forKey:@"date"];
    [favDict setObject:[aFavoris dateStr] forKey:@"dateStr"];
    
    [favDict setObject:[NSString stringWithFormat:@"%i", [aFavoris isCompletePost]] forKey:@"isCompletePost"];
    [favDict setObject:[NSString stringWithFormat:@"%i", [aFavoris inStock]] forKey:@"inStock"];
    
    if ([aFavoris content] != nil) {
        [favDict setObject:[aFavoris content] forKey:@"content"];
    }
    if ([aFavoris shortContent] != nil) {
        [favDict setObject:[aFavoris shortContent] forKey:@"shortContent"];
    }
    
    if ([aFavoris auteur] != nil) {
        [favDict setObject:[aFavoris auteur] forKey:@"auteur"];
    }
    if ([aFavoris narrateur] != nil) {
        [favDict setObject:[aFavoris narrateur] forKey:@"narrateur"];
    }
    if ([aFavoris URLArticle] != nil) {
        [favDict setObject:[aFavoris URLArticle] forKey:@"URLArticle"];
    }
    if ([aFavoris URLEditor] != nil) {
        [favDict setObject:[aFavoris URLEditor] forKey:@"URLEditor"];
    }
    if ([aFavoris presentateurs] != nil) {
        [favDict setObject:[aFavoris presentateurs] forKey:@"presentateurs"];
    }
    if ([aFavoris invites] != nil) {
        [favDict setObject:[aFavoris invites] forKey:@"invites"];
    }
    
    if ([aFavoris URLMp3File] != nil) {
        [favDict setObject:[aFavoris URLMp3File] forKey:@"URLMp3File"];
    }
    if ([aFavoris codeISRC] != nil) {
        [favDict setObject:[aFavoris codeISRC] forKey:@"codeISRC"];
    }
    
    if ([aFavoris auteur2] != nil) {
        [favDict setObject:[aFavoris auteur2] forKey:@"auteur2"];
    }
    if ([aFavoris editeur] != nil) {
        [favDict setObject:[aFavoris editeur] forKey:@"editeur"];
    }
    if ([aFavoris prix] != nil) {
        [favDict setObject:[aFavoris prix] forKey:@"prix"];
    }
    if ([aFavoris identifier] != nil) {
        [favDict setObject:[aFavoris identifier] forKey:@"identifier"];
    }
    
    if ([aFavoris duree] != nil) {
        [favDict setObject:[aFavoris duree] forKey:@"duree"];
    }
    if ([aFavoris tags] != nil) {
        [favDict setObject:[aFavoris tags] forKey:@"tags"];
    }
    
    [favDict setObject:[NSString stringWithFormat:@"%i", [aFavoris type]] forKey:@"type"];
    
    [dict setObject:(NSDictionary*)favDict forKey:[NSString stringWithFormat:@"%i", [aFavoris postID]]];
    
    NSDictionary *dicoSauv = [NSDictionary dictionaryWithDictionary:dict];
    return [dicoSauv writeToFile:plistPath atomically:YES];
}

- (void)retirerUnFavorisForID:(int)articleID {
    
    NSString *plistPath = [[GestionFichiers shared] creationFichierPlist:@"favoris_post"];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    [dict removeObjectForKey:[NSString stringWithFormat:@"%i", articleID]];
    
    NSDictionary *dicoSauv = [NSDictionary dictionaryWithDictionary:dict];
    [dicoSauv writeToFile:plistPath atomically:YES];
}

- (BOOL)isFavorisForID:(int)articleID {
    
    NSString *plistPath = [[GestionFichiers shared] creationFichierPlist:@"favoris_post"];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    
    if ([dict valueForKey:[NSString stringWithFormat:@"%i", articleID]] != nil) {
        return YES;
    } else {
        return NO;
    }
}

#pragma mark - Noter App

- (BOOL) getNoterApp {
    
    NSString *plistPath = [[GestionFichiers shared] creationFichierPlist:@"popup"];
    NSDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    return [[dict objectForKey:@"noter_app"] boolValue];
}

- (void) setNoterApp:(int)isNoterApp {
    
    NSString *plistPath = [[GestionFichiers shared] creationFichierPlist:@"popup"];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    [dict setObject:[NSString stringWithFormat:@"%i", isNoterApp] forKey:@"noter_app"];
    NSDictionary *dicoSauv = [NSDictionary dictionaryWithDictionary:dict];
    [dicoSauv writeToFile:plistPath atomically:YES];
}

- (NSString*) getCurrentVersion {
    
    NSString *plistPath = [[GestionFichiers shared] creationFichierPlist:@"popup"];
    NSDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    return [dict objectForKey:@"app_version"];
}

- (void) setCurrentVersion {
    
    NSString *plistPath = [[GestionFichiers shared] creationFichierPlist:@"popup"];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    [dict setObject:kCurrentAppVersion forKey:@"app_version"];
    NSDictionary *dicoSauv = [NSDictionary dictionaryWithDictionary:dict];
    [dicoSauv writeToFile:plistPath atomically:YES];
}

- (int) getNbOuvertures {
    
    NSString *plistPath = [[GestionFichiers shared] creationFichierPlist:@"popup"];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    int nbOuvertures = [[dict objectForKey:@"ouvertures"] intValue];
    [dict setObject:[NSString stringWithFormat:@"%i", nbOuvertures+1] forKey:@"ouvertures"];
    NSDictionary *dicoSauv = [NSDictionary dictionaryWithDictionary:dict];
    [dicoSauv writeToFile:plistPath atomically:YES];
//    NSLog(@"[WPPlistRecup] nbOuvertures : %i", nbOuvertures);
    return nbOuvertures;
}

#pragma mark - Articles 

- (BOOL) isInterstitialWithFrequence:(int)frequence {
    
    NSString *plistPath = [[GestionFichiers shared] creationFichierPlist:@"popup"];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    int nbInterstitiel = [[dict objectForKey:@"interstitiel"] intValue];
    [dict setObject:[NSString stringWithFormat:@"%i", nbInterstitiel+1] forKey:@"interstitiel"];
    NSDictionary *dicoSauv = [NSDictionary dictionaryWithDictionary:dict];
    [dicoSauv writeToFile:plistPath atomically:YES];
//    NSLog(@"[WPPlistRecup] nbInterstitiel : %i", nbInterstitiel);
    return (nbInterstitiel%frequence == 0);
    
}

#pragma mark Read

- (void) setIsReadForArticleID:(int)articleID {
    
    NSString *plistPath = [[GestionFichiers shared] creationFichierPlist:@"articles_read"];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    [dict setObject:@"1" forKey:[NSString stringWithFormat:@"%i", articleID]];
    NSDictionary *dicoSauv = [NSDictionary dictionaryWithDictionary:dict];
    [dicoSauv writeToFile:plistPath atomically:YES];
}

- (BOOL) getIsReadForArticleID:(int)articleID {
    
    NSString *plistPath = [[GestionFichiers shared] creationFichierPlist:@"articles_read"];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
    return [[dict objectForKey:[NSString stringWithFormat:@"%i", articleID]] boolValue];
}

#pragma mark - Commentaires

- (void) sauvCommentaireReglagesWithEmail:(NSString*)email andNom:(NSString*)nom {
    
    NSString *plistPath = [[GestionFichiers shared] creationFichierPlist:@"commentaire"];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    
    [dict setObject:nom forKey:@"nom"];
    [dict setObject:email forKey:@"email"];
    
    NSDictionary *dicoSauv = [NSDictionary dictionaryWithDictionary:dict];
    [dicoSauv writeToFile:plistPath atomically:YES];
}

- (NSDictionary*) getCommentaireReglages {
    
    NSString *plistPath = [[GestionFichiers shared] creationFichierPlist:@"commentaire"];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
    return dict;
}

#pragma mark - Audio

- (BOOL) getReglageSon {
    
    NSString *plistPath = [[GestionFichiers shared] creationFichierPlist:@"reglages"];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
    return [[dict objectForKey:@"sound"] boolValue];
}

- (void) setReglageSon:(BOOL)isSound {
    
    NSString *plistPath = [[GestionFichiers shared] creationFichierPlist:@"reglages"];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    
    [dict setObject:[NSString stringWithFormat:@"%i", isSound] forKey:@"sound"];
    
    NSDictionary *dicoSauv = [NSDictionary dictionaryWithDictionary:dict];
    [dicoSauv writeToFile:plistPath atomically:YES];
}

#pragma mark - First Open

- (BOOL) isFirstOpen {
    NSString *plistPath = [[GestionFichiers shared] creationFichierPlist:@"intro"];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    return [[dict objectForKey:@"first_open"] boolValue];
}

- (void) setFirstOpenToNo {
    NSString *plistPath = [[GestionFichiers shared] creationFichierPlist:@"intro"];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    [dict setObject:[NSString stringWithFormat:@"%i", NO] forKey:@"first_open"];
    [(NSDictionary*)dict writeToFile:plistPath atomically:YES];
}

#pragma mark - Models

- (NSArray*) getPostsFromAPI:(NSArray*)json {
    return [self getPostsFromAPI:json andType:APIWPJSON];
}

- (NSArray*) getPostsFromAPI:(NSArray*)json andType:(PostType)type {
    
    NSMutableArray *mutArr = [NSMutableArray new];
    for (NSDictionary *dict in json) {
        // Récupérer les éléments
//        DLog(@"%@", json);
        
        TTPost *item = [TTPost new];
        if (type == APIWPJSON) {
            item = [self getOneItemFromDict:dict];
        } else if (type == APIWoocommerce) {
            //item = [self getOneItemFromDict:dict];
            item = [self getOneItemWooCommerceFromDict:dict];
        }
        [mutArr addObject:item];
    }
    return [NSArray arrayWithArray:mutArr];
}

- (TTPost *)getOneItemFromDict:(NSDictionary *)dict {
    
    TTPost *item = [TTPost new];
    [item setPostID:[[dict objectForKey:@"ID"] intValue]];
    [item setTitre:[[dict objectForKey:@"title"] stringByDecodingHTMLEntities]];
    [item setNbComments:0]; // Pas dans l'API ??
    
    // Type
    if ([[dict objectForKey:@"type"] isEqualToString:@"post"]) {
        [item setType:APIPost];
    } else if ([[dict objectForKey:@"type"] isEqualToString:@"abs_podcast"]) {
        [item setType:APIPodcast];
    }  else if ([[dict objectForKey:@"type"] isEqualToString:@"livre-audio"]) {
        [item setType:APIAnnuaire];
    }
    
    // Catégories
    NSArray *categories = [[dict objectForKey:@"terms"] objectForKey:@"category"];
    NSMutableString *mutStr = [NSMutableString new];
    for (NSDictionary *catDict in categories) {
        [mutStr appendString:[catDict objectForKey:@"name"]];
        [mutStr appendString:@", "];
    }
    [item setCategories:[mutStr substringToIndex:mutStr.length-((mutStr.length>0)*2)]];
    
    // Feature image
    NSDictionary *featured_image = [dict objectForKey:@"featured_image"];
    if ([featured_image isKindOfClass:[NSDictionary class]]) {
        NSDictionary *attachment_meta = [featured_image objectForKey:@"attachment_meta"];
        if ([attachment_meta isKindOfClass:[NSDictionary class]]) {
            NSDictionary *sizes = [attachment_meta objectForKey:@"sizes"];
            if ([sizes isKindOfClass:[NSDictionary class]]) {
                NSDictionary *medium = [sizes objectForKey:@"medium"];
                if ([medium isKindOfClass:[NSDictionary class]]) {
                    [item setURLFeatureImg:[medium objectForKey:@"url"]];
                }
            }
        }
    }
    if (item.URLFeatureImg == nil) {
        [item setURLFeatureImg:@""];
    }
    
    // Date
    [item setDateStr:[dict objectForKey:@"date"]];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setLocale:[NSLocale localeWithLocaleIdentifier:@"fr_FR"]];
    [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSDate *date = [df dateFromString:item.dateStr];
    [item setDate:date];
    
    [item setContent:[dict objectForKey:@"content"]];
    if ([[dict objectForKey:@"comment_status"] isEqualToString:@"closed"]) {
        [item setIsCommentsOpen:NO];
    } else {
        [item setIsCommentsOpen:YES];
    }
    
    // Auteur
    NSDictionary *auteurDict = [dict objectForKey:@"author"];
    NSMutableString *auteurStr = [NSMutableString new];
    if (auteurDict != nil) {
        [auteurStr appendString:[auteurDict objectForKey:@"name"]];
    }
    [item setAuteur:auteurStr];
    
    // Terms
    NSDictionary *terms = [dict objectForKey:@"terms"];
    
    // Auteur Terms
    NSArray *auteurArr = [terms objectForKey:@"auteur"];
    NSMutableString *auteurStr2 = [NSMutableString new];
    for (NSDictionary *auteurDict in auteurArr) {
        [auteurStr2 appendString:[auteurDict objectForKey:@"name"]];
    }
    [item setAuteur2:auteurStr2];
    
    // Editeur Terms
    NSArray *editeurArr = [terms objectForKey:@"editeur"];
    NSMutableString *editeurStr = [NSMutableString new];
    for (NSDictionary *editeurDict in editeurArr) {
        [editeurStr appendString:[editeurDict objectForKey:@"name"]];
    }
    [item setEditeur:editeurStr];
    
    // Présentateur et invités
    NSDictionary *cptmeta = [dict objectForKey:@"cptmeta"];
    if ([cptmeta isKindOfClass:[NSDictionary class]]) {
        
        [item setURLEditor:[cptmeta objectForKey:@"url"]];
        
        NSArray *presentateurArr = [cptmeta objectForKey:@"presentateur"];
        NSMutableString *presentateurStr = [NSMutableString new];
        if ([presentateurArr isKindOfClass:[NSArray class]]) {
            for (NSDictionary *presentateurDict in presentateurArr) {
                [presentateurStr appendString:[presentateurDict objectForKey:@"post_title"]];
            }
        }
        [item setPresentateurs:presentateurStr];
        
        NSArray *inviteArr = [cptmeta objectForKey:@"invite"];
        NSMutableString *inviteStr = [NSMutableString new];
        if ([inviteArr isKindOfClass:[NSArray class]]) {
            for (NSDictionary *inviteDict in inviteArr) {
                [inviteStr appendString:[inviteDict objectForKey:@"post_title"]];
            }
        }
        [item setInvites:inviteStr];
        
        NSArray *auteurArr = [cptmeta objectForKey:@"auteur"];
        NSMutableString *auteurStr = [NSMutableString new];
        if ([auteurArr isKindOfClass:[NSArray class]]) {
            for (NSDictionary *auteurDict in auteurArr) {
                [auteurStr appendString:[auteurDict objectForKey:@"post_title"]];
            }
        }
        if (![auteurStr isEqualToString:@""] && auteurStr != nil) {
            [item setAuteur2:auteurStr];
        }
        
        NSArray *narrateurArr = [cptmeta objectForKey:@"narrateur"];
        NSMutableString *narrateurStr = [NSMutableString new];
        if ([narrateurArr isKindOfClass:[NSArray class]]) {
            for (NSDictionary *narrateurDict in narrateurArr) {
                [narrateurStr appendString:[narrateurDict objectForKey:@"post_title"]];
            }
        }
        [item setNarrateur:narrateurStr];
        
        NSString *prix = [cptmeta objectForKey:@"prix"];
        NSString *prixStr = @"";
        if ([prix floatValue] == 0) {
            prixStr = @"Gratuit";
        } else {
            prixStr = [NSString stringWithFormat:@"%@€", prix];
        }
        [item setPrix:prixStr];
        
        // Code ISRC
        if (item.type == APIPodcast) {
            [item setCodeISRC:[cptmeta objectForKey:@"isrc"]];
        }
    }
    
    // URLs
    [item setURLArticle:[dict objectForKey:@"link"]];
    [item setURLMp3File:[dict objectForKey:@"urlmp3"]];
    
    return item;
}


- (TTPost *)getOneItemWooCommerceFromDict:(NSDictionary*)dict {
    
    TTPost *item = [TTPost new];
    [item setPostID:[[dict objectForKey:@"id"] intValue]];
    [item setTitre:[[dict objectForKey:@"title"] stringByDecodingHTMLEntities]];
    [item setContent:[dict objectForKey:@"description"]];
    [item setShortContent:[dict objectForKey:@"short_description"]];
    [item setNbComments: [[dict objectForKey:@"nbavis"] intValue]];
    [item setInStock: [[dict objectForKey:@"in_stock"] boolValue]];
    [item setType:APIWC];
    
    // Catégories
    NSArray *categories = [dict objectForKey:@"categories"];
    NSMutableString *mutStr = [NSMutableString new];
    for (NSString *catStr in categories) {
        [mutStr appendString:catStr];
        [mutStr appendString:@", "];
    }
    [item setCategories:[mutStr substringToIndex:mutStr.length-((mutStr.length>0)*2)]];
    
    // Feature image
    [item setURLFeatureImg:[dict objectForKey:@"featured_src"]];
    if (![item.URLFeatureImg isKindOfClass:[NSString class]]) {
        [item setURLFeatureImg:@""];
    }
    
    NSDictionary *cptmeta = [dict objectForKey:@"cptmeta"];
    if ([cptmeta isKindOfClass:[NSDictionary class]]) {
        
        [item setURLEditor:[cptmeta objectForKey:@"url"]];
        
        NSArray *presentateurArr = [cptmeta objectForKey:@"presentateur"];
        NSMutableString *presentateurStr = [NSMutableString new];
        if ([presentateurArr isKindOfClass:[NSArray class]]) {
            for (NSDictionary *presentateurDict in presentateurArr) {
                [presentateurStr appendString:[presentateurDict objectForKey:@"post_title"]];
            }
        }
        [item setPresentateurs:presentateurStr];
        
        NSArray *inviteArr = [cptmeta objectForKey:@"invite"];
        NSMutableString *inviteStr = [NSMutableString new];
        if ([inviteArr isKindOfClass:[NSArray class]]) {
            for (NSDictionary *inviteDict in inviteArr) {
                [inviteStr appendString:[inviteDict objectForKey:@"post_title"]];
            }
        }
        [item setInvites:inviteStr];
        
        NSArray *auteurArr = [cptmeta objectForKey:@"auteur"];
        NSMutableString *auteurStr = [NSMutableString new];
        if ([auteurArr isKindOfClass:[NSArray class]]) {
            for (NSDictionary *auteurDict in auteurArr) {
                [auteurStr appendString:[auteurDict objectForKey:@"post_title"]];
            }
        }
        if (![auteurStr isEqualToString:@""] && auteurStr != nil) {
            [item setAuteur2:auteurStr];
            [item setAuteur:auteurStr];
        }
        
        NSArray *narrateurArr = [cptmeta objectForKey:@"narrateur"];
        NSMutableString *narrateurStr = [NSMutableString new];
        if ([narrateurArr isKindOfClass:[NSArray class]]) {
            for (NSDictionary *narrateurDict in narrateurArr) {
                [narrateurStr appendString:[narrateurDict objectForKey:@"post_title"]];
            }
        }
        [item setNarrateur:narrateurStr];
        
        // Code ISRC
        if (item.type == APIPodcast) {
            [item setCodeISRC:[cptmeta objectForKey:@"isrc"]];
        }
        
        NSString *length = [cptmeta objectForKey:@"prod_length"];
        if (length != nil && ![length  isEqual: @""]){
            int minutes = [length intValue];
            int hours = (minutes/60);
            minutes = minutes % 60;
            
            item.duree = [NSString stringWithFormat:@"%imin", minutes];
            if (hours>0){
                item.duree = [NSString stringWithFormat:@"%ih %@", hours, item.duree];
            }
        }
        
        NSString *date_de_parution = [cptmeta objectForKey:@"date_de_parution_"];
        if (length != nil){
            item.dateStr = date_de_parution;
        }
        NSString *audioLink = [cptmeta objectForKey:@"audio_version_ios"];
        if (audioLink != nil && ![audioLink isEqualToString:@""]){
            NSRange range = [audioLink rangeOfString:@".m4b."];
            if (range.length>0){
                audioLink = [audioLink substringWithRange: NSMakeRange(0, range.location+4)];
            }
            [item setURLMp3File: audioLink];
        }
        if (![[cptmeta objectForKey:@"prod_extract"] isEqualToString:@""]){
            [item setURLMp3Extract: [cptmeta objectForKey:@"prod_extract"]];
        }        
        /*
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setLocale:[NSLocale localeWithLocaleIdentifier:@"fr_FR"]];
        [df setDateFormat:@"dd MMMM YYYY"];
        NSDate *date = [df dateFromString: item.dateStr];
        [item setDate:date];
         NOT WORKING BUT NOT USED ANYWAY
         */
    }

    
    
    
    
    
    
    [item setIsCommentsOpen:NO];
    
    // Tags
    NSMutableString *tag = [NSMutableString new];
    NSArray *tags = [dict objectForKey:@"tags"];
    int j = 0;
    for (NSString *tagStr in tags) {
        if (j != 0) {
            [tag appendString:@" - "];
        }
        [tag appendString:tagStr];
        j++;
    }
    [item setTags:tag];
    
    // Prix
    NSString *price = [dict objectForKey:@"price"];
    if ([price floatValue] == 0) {
              price = @"Gratuit";
    } else {
        price = [NSString stringWithFormat:@"%@ €", price];
    }
    [item setPrix:price];
    
    // URLs
    NSArray *variations = [dict objectForKey:@"variations"];
    BOOL isAudioFile = NO;
    if (variations.count > 0) {
        for ( NSDictionary *var0 in variations){
            [item setIdentifier:[NSString stringWithFormat:@"%@", [var0 objectForKey:@"sku"]]];
         
             NSArray *downloads = [var0 objectForKey:@"downloads"];
             for (NSDictionary *dict in downloads) {
                NSString *file = [dict objectForKey:@"file"];
                NSArray *components = [file componentsSeparatedByString:@"https://www.abs-multimedias.com/medias/app/"];
                 if (components.count >1) {
                     [item setURLZipFile:file];
                     isAudioFile = YES;
                     break;
                 }
             }
            if (isAudioFile) {
                break;
            }
        }
    }
     if (!isAudioFile) {
     NSArray *downloads = [dict objectForKey:@"downloads"];
         for (NSDictionary *dict in downloads) {
             NSString *file = [dict objectForKey:@"file"];
             NSArray *components = [file componentsSeparatedByString:@"https://www.abs-multimedias.com/medias/app/"];
             if (components.count >1) {
                 [item setURLZipFile:file];
                 break;
             }
         }
     }
    
    
    
    // URLs
    
    [item setURLArticle:[dict objectForKey:@"permalink"]];
    
    return item;
}

@end
