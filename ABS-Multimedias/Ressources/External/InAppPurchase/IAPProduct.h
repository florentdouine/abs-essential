//
//  IAPProduct.h
//  Guide Dofus
//
//  Created by Virginie on 25/02/13.
//
//

#import <Foundation/Foundation.h>

#define kIdentifier @"identifier"
#define kPurchased @"purchased"

@interface IAPProduct : NSObject <NSCoding>
@property (nonatomic, retain) NSString *productID;
@property (nonatomic, assign) BOOL purchased;

@end
