//
//  IAPProduct.m
//  Guide Dofus
//
//  Created by Virginie on 25/02/13.
//
//

#import "IAPProduct.h"

@implementation IAPProduct
@synthesize productID = _productID, purchased = _purchased;

- (id) initWithCoder:(NSCoder *)coder {
    
    if (self = [super init]) {
        self.productID = [coder decodeObjectForKey:kIdentifier];
        self.purchased = [coder decodeBoolForKey:kPurchased];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:self.productID forKey:kIdentifier];
    [coder encodeBool:self.purchased forKey:kPurchased];
}

@end
