//
//  InAppPurchaseStoreManager.h
//  Guide Dofus
//
//  Created by Virginie on 25/02/13.
//
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

@class IAPProduct;

@protocol InAppPurchaseStoreManagerDelegate;

@interface InAppPurchaseStoreManager : NSObject <SKProductsRequestDelegate, SKPaymentTransactionObserver> {
    NSMutableArray *_listOfProductsPurchasedOrNot;
    NSMutableArray *_listOfProductsAvailabled;
}

@property (nonatomic, assign) id<InAppPurchaseStoreManagerDelegate> delegate;

+ (InAppPurchaseStoreManager*)sharedManager;
- (void) requestProductData;
- (void) restoreInAppPurchase;
- (void) wantToPurchaseProducts:(SKProduct*)product;
- (BOOL) havePurchasedProduct:(NSString*)productID;
- (void) requestSKProductData:(IAPProduct*)iapProduct;

@end

@protocol InAppPurchaseStoreManagerDelegate <NSObject>
@optional
// Informe le Delegate de l'achat d'un produit
- (void)inAppPurchaseStoreManager:(InAppPurchaseStoreManager*)manager didPurchasedProduct:(NSString*)productId;
- (void)inAppPurchaseStoreManager:(InAppPurchaseStoreManager*)manager didFailedToPurchasedProduct:(NSString*)productId;
- (void)inAppPurchaseStoreManagerDidFailedAndStopLoading:(InAppPurchaseStoreManager *)manager;
- (void)inAppPurchaseStoreManagerFailedToRestore;
- (void)inAppPurchaseStoreManagerSuccessToRestore;

//@required
// Demande au Delegate d'afficher la liste des produits
- (void)inAppPurchaseStoreManager:(InAppPurchaseStoreManager *)manager askToDisplayListOfProducts:(NSMutableArray*)list;

@end
