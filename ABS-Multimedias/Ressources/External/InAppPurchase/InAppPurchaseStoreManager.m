//
//  InAppPurchaseStoreManager.m
//  Guide Dofus
//
//  Created by Virginie on 25/02/13.
//
//

#import "InAppPurchaseStoreManager.h"
#import "IAPProduct.h"
#import "WPAppDelegate.h"

@interface InAppPurchaseStoreManager(private)
- (void) retrieveListProductsFromSavedFile;
- (void) completeTransaction:(SKPaymentTransaction*)transaction;
- (void) failedTransaction:(SKPaymentTransaction*)transaction;
- (void) recordTransaction:(SKPaymentTransaction*)transaction;
- (void) restoreTransaction:(SKPaymentTransaction*)transaction;
@end

@implementation InAppPurchaseStoreManager
@synthesize delegate = _delegate;

// Notre manager
static InAppPurchaseStoreManager *sharedManager;

+ (InAppPurchaseStoreManager*)sharedManager {
    
    @synchronized(self) {
        if (!sharedManager) {
            sharedManager = [[InAppPurchaseStoreManager alloc] init];
            // On récupère la liste des produits stockés dans un fichier ou les préférences
            [sharedManager retrieveListProductsFromSavedFile];
            
            // On ajoute un observateur du store le plus tôt possible pour gérer les achats
            [[SKPaymentQueue defaultQueue] addTransactionObserver:sharedManager];
        }
        return sharedManager;
    }
    return sharedManager;
}

#pragma mark - Singleton Methods

+ (id)allocWithZone:(NSZone *)zone {
    @synchronized(self) {
        if (sharedManager == nil) {
            sharedManager = [super allocWithZone:zone];
            return sharedManager;
        }
    }
    return nil;
}

- (id)copyWithZone:(NSZone*)zone {
    return self;
}

#pragma mark - Methods

- (void) retrieveListProductsFromSavedFile {
    // On récupère depuis le fichier avec NSCoding
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentationDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"products.myArchive"];
    NSData *data;
    NSKeyedUnarchiver *unarchiver;
    data = [[NSData alloc] initWithContentsOfFile:path];
    
    if (data) {
        // Si le fichier contient des données, on désarchive
        unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
        NSMutableArray *arrayArchived = [unarchiver decodeObjectForKey:@"Products"];
        _listOfProductsPurchasedOrNot = arrayArchived;
        [unarchiver finishDecoding];
    }
    
    if (!_listOfProductsPurchasedOrNot) {
        // Le fichier n'existe pas, on le crée
        _listOfProductsPurchasedOrNot = [[NSMutableArray alloc] init];
        
        // Ajout des produits
        /*IAPProduct *product1 = [[IAPProduct alloc] init];
        product1.productID = kIdProduit1;
        product1.purchased = NO;
        [_listOfProductsPurchasedOrNot addObject:product1];*/

    }
}

// Demande la liste des produits disponibles auprès de l'AppStore
- (void) requestProductData {
    
    // Crée un tableau ne contenant que les identifiants disponibles
    // Remarquez que la clé est le nom de l'objet NSString *productID de IAPProduct
    NSArray *arrayOfIdentifiers = [_listOfProductsPurchasedOrNot valueForKey:@"productID"];
    // Création de la requête
    SKProductsRequest *request = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithArray:arrayOfIdentifiers]];
    // self est delegate de SKProductsRequest
    request.delegate = self;
    // On démarre
    [request start];
}

- (void) requestSKProductData:(IAPProduct*)iapProduct {
    
    NSArray *arrayOfIdentifiers = @[iapProduct.productID];
    SKProductsRequest *request = [[SKProductsRequest alloc] initWithProductIdentifiers:[NSSet setWithArray:arrayOfIdentifiers]];
    request.delegate = self;
    [request start];
}

- (void) restoreInAppPurchase {
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue {
    if ([_delegate respondsToSelector:@selector(inAppPurchaseStoreManagerSuccessToRestore)]) {
        [_delegate inAppPurchaseStoreManagerSuccessToRestore];
    }
}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error {
    if ([_delegate respondsToSelector:@selector(inAppPurchaseStoreManagerFailedToRestore)]) {
        [_delegate inAppPurchaseStoreManagerFailedToRestore];
    }
}

#pragma mark - SKProductsRequestDelegate methods

// Appelée lorsque le Delegate reçoit la réponse de l'App Store
- (void) productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    
    if ([response.products isKindOfClass:[NSArray class]]) {
        _listOfProductsAvailabled = [[NSMutableArray alloc] init];
        // On relève tous les objets
        [_listOfProductsAvailabled removeAllObjects];
        [_listOfProductsAvailabled addObjectsFromArray:response.products];
        
        // A vous d'afficher une vue pour présenter les données
        if ([_delegate respondsToSelector:@selector(inAppPurchaseStoreManager:askToDisplayListOfProducts:)]) {
            [_delegate inAppPurchaseStoreManager:self askToDisplayListOfProducts:_listOfProductsAvailabled];
        } else {
            
            if ([_delegate respondsToSelector:@selector(inAppPurchaseStoreManager:didFailedToPurchasedProduct:)]) {
                [_delegate inAppPurchaseStoreManager:self didFailedToPurchasedProduct:nil];
                return;
            }
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur" message:@"Une erreur s'est produite durant l'achat-in-app. Celui-ci n'a pas pu être effectué, merci de réessayer." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [alert show];
        }

        // On release ici la requête instanciée plus haut dans requestProductData
        request = nil;
        
    } else {
        
        if ([_delegate respondsToSelector:@selector(inAppPurchaseStoreManager:didFailedToPurchasedProduct:)]) {
            [_delegate inAppPurchaseStoreManager:self didFailedToPurchasedProduct:nil];
            return;
        }
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Erreur" message:@"Une erreur s'est produite durant l'achat-in-app. Celui-ci n'a pas pu être effectué, merci de réessayer." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
}

#pragma mark - Buy methods

- (void) wantToPurchaseProducts:(SKProduct*)product {
    
    if ([SKPaymentQueue canMakePayments]) {
        
        SKPayment *payment = [SKPayment paymentWithProduct:product];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
        
    } else {
        
        // L'utilisateur à desactivé la possibilité d'acheter -> présenter une alert si besoin
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Achats désactivés" message:@"Les achats-in-app sont désactivés sur votre appareil, merci de les activer les réglages et de réessayer." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        if ([_delegate respondsToSelector:@selector(inAppPurchaseStoreManagerDidFailedAndStopLoading:)]) {
            [_delegate inAppPurchaseStoreManagerDidFailedAndStopLoading:self];
        }

    }
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
    
    for (SKPaymentTransaction *transaction in transactions) {
        
        switch (transaction.transactionState) {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
                break;
                
            default:
                break;
        }
    }
}

- (void) completeTransaction:(SKPaymentTransaction *)transaction {
    // On enregistre
    [self recordTransaction:transaction];
    // On informe le delegate
    if ([_delegate respondsToSelector:@selector(inAppPurchaseStoreManager:didPurchasedProduct:)]) {
        [_delegate inAppPurchaseStoreManager:self didPurchasedProduct:transaction.payment.productIdentifier];
    }
    // On finit la transaction
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction {
    // On enregistre
    [self recordTransaction:transaction];
    // On informe le delegate
    if ([_delegate respondsToSelector:@selector(inAppPurchaseStoreManager:didPurchasedProduct:)]) {
        [_delegate inAppPurchaseStoreManager:self didPurchasedProduct:transaction.payment.productIdentifier];
    }
    // On finit la transaction
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)failedTransaction:(SKPaymentTransaction *)transaction {
    if (transaction.error.code != SKErrorPaymentCancelled) {
        // Il y a eu une erreur dans l'achat
        if ([_delegate respondsToSelector:@selector(inAppPurchaseStoreManager:didFailedToPurchasedProduct:)]) {
            [_delegate inAppPurchaseStoreManager:self didFailedToPurchasedProduct:transaction.payment.productIdentifier];
        }
    } else {
        if ([_delegate respondsToSelector:@selector(inAppPurchaseStoreManagerDidFailedAndStopLoading:)]) {
            [_delegate inAppPurchaseStoreManagerDidFailedAndStopLoading:self];
        }
    }
    // On finit la transaction
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)recordTransaction:(SKPaymentTransaction *)transaction {
    // On énumère pour mettre à YES le booléen du produit acheté
    for (IAPProduct *iapProduct in _listOfProductsPurchasedOrNot) {
        if ([iapProduct.productID isEqualToString:transaction.payment.productIdentifier]) {
            iapProduct.purchased = YES;
        }
    }
    // On sauvegarde avec NSCoding
    NSMutableData *data;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentationDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *archivePath = [documentsDirectory stringByAppendingPathComponent:@"products.myArchive"];
    NSKeyedArchiver *archiver;
    
    data = [NSMutableData data];
    archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    [archiver encodeObject:_listOfProductsPurchasedOrNot forKey:@"Products"];
    [archiver finishEncoding];
    [data writeToFile:archivePath atomically:YES];
}

// Retourne un booléen selon que le produit a déjà été acheté ou non
- (BOOL) havePurchasedProduct:(NSString*)productID {
    for (IAPProduct *iapProduct in _listOfProductsPurchasedOrNot) {
        if ([iapProduct.productID isEqualToString:productID]) {
            return iapProduct.purchased;
        }
    }
    return NO;
}

@end
