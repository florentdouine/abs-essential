//
//  ChargementView.h
//  Spotiful
//
//  Created by Virginie Delaitre on 21/01/2015.
//  Copyright (c) 2015 Toutdanslapoche. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTChargementView : UIView

- (id) initWithFrame:(CGRect)frame;

@end
