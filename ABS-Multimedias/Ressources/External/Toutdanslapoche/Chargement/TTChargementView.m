//
//  ChargementView.m
//  Spotiful
//
//  Created by Virginie Delaitre on 21/01/2015.
//  Copyright (c) 2015 Toutdanslapoche. All rights reserved.
//

#import "TTChargementView.h"

@implementation TTChargementView

- (id) initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setBackgroundColor:[UIColor colorWithWhite:1 alpha:0.3]];
        [self setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        
        UIView *loadView = [[UIView alloc] initWithFrame:CGRectMake((self.frame.size.width-40)/2, (self.frame.size.height-40)/2, 40, 40)];
        [loadView setBackgroundColor:kColorNavBar];
        [[loadView layer] setCornerRadius:4];
        [loadView setAutoresizingMask: UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin];
        [loadView setAccessibilityLabel:@"Chargement"];
        [self addSubview:loadView];
        UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [activity setColor:[UIColor whiteColor]];
        [activity setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [activity startAnimating];
        [activity setAutoresizingMask: UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin];
        [self addSubview:activity];
    }
    return self;
}

@end
