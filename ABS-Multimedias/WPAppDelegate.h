//
//  WPAppDelegate.h
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WPRootViewController;

#define MyAppDelegate       ((WPAppDelegate *)[UIApplication sharedApplication].delegate)

@interface WPAppDelegate : UIResponder <UIApplicationDelegate, UIAlertViewDelegate> {
    
    NSDictionary *_push;
    BOOL _isPush;
    
    NSDictionary *_articleNewsletter;
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) id<GAITracker> tracker;
@property (strong, nonatomic) UINavigationController *navController;
@property (strong, nonatomic) WPRootViewController *wprootController;
@property (nonatomic, assign) BOOL fullScreenVideoIsPlaying;

- (void) reloadAllApp;

@end
