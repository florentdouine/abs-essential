//
//  WPAppDelegate.m
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import "WPAppDelegate.h"
#import "WPRootViewController.h"
#import <Parse/Parse.h>
#import "WPDetailArticleViewController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <SDWebImage/SDImageCache.h>
#import <Realm/Realm.h>

@implementation WPAppDelegate
@synthesize navController = _navController;
@synthesize wprootController = _wprootController;
@synthesize fullScreenVideoIsPlaying = _fullScreenVideoIsPlaying;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    // Personalize
    self.window.backgroundColor = kColorBackground;
    [[UIApplication sharedApplication] setStatusBarStyle:(UIStatusBarStyle)kStatusBarStyle];
    
    
    
    // Realm
    RLMRealmConfiguration *config = [RLMRealmConfiguration defaultConfiguration];
    config.schemaVersion = 7;
    config.migrationBlock = ^(RLMMigration *migration, uint64_t oldSchemaVersion) {
        // We haven’t migrated anything yet, so oldSchemaVersion == 0
        if (oldSchemaVersion < config.schemaVersion) {
            // The enumerateObjects:block: method iterates
            // over every 'Person' object stored in the Realm file
            [migration enumerateObjects:@"TTLivreAudio" block:^(RLMObject *oldObject, RLMObject *newObject) {
                newObject[@"type"] = @1;
                newObject[@"isBuy"] = @0;
                newObject[@"isDownload"] = @1;
                newObject[@"price"] = @0;
                newObject[@"identifier"] = @"";
            }];
        }
    };
    [RLMRealmConfiguration setDefaultConfiguration:config];

    // Font initialization
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults registerDefaults:@{kFontSize:@1}];
    [userDefaults synchronize];
    
    // Initialize Google Analytics with a 120-second dispatch interval. There is a
    // tradeoff between battery usage and timely dispatch.
    [GAI sharedInstance].dispatchInterval = 120;
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    _tracker = [[GAI sharedInstance] trackerWithTrackingId:kTrackingID];
    
    // Parse (Notifications Push)
    [Parse setApplicationId:kParseAppID
                  clientKey:kParseClientID];
    
    [application registerForRemoteNotifications];
    UIUserNotificationSettings* notificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    //
    
    // Réception Push Notification quand l'application n'est pas ouverte
    BOOL isModal = NO;
    NSString *postID = 0;
    NSDictionary *remoteNotif = [launchOptions objectForKey: UIApplicationLaunchOptionsRemoteNotificationKey];
    if (remoteNotif) {
        DLog(@"Réception d'une notification push : %@", remoteNotif);
        isModal = YES;
        postID = [NSString stringWithFormat:@"%i", [[remoteNotif objectForKey:@"post_id"] intValue]];
    }
    
    /* TEST de notification */
    /*postID = @"4560";
    isModal = YES;*/
    
    // RootController
    if (isModal) {
        _wprootController = [[WPRootViewController alloc] initWithFrame:CGRectMake(0, 0, self.window.frame.size.width, self.window.frame.size.height) andModal:postID];
    } else {
        _wprootController = [[WPRootViewController alloc] initWithFrame:CGRectMake(0, 0, self.window.frame.size.width, self.window.frame.size.height)];
    }
    _navController = [[UINavigationController alloc] initWithRootViewController:_wprootController];
    [_navController.view setFrame:CGRectMake(0, 0, self.window.frame.size.width, self.window.frame.size.height)];
    [_navController.view setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
    [_navController.navigationBar setTranslucent:NO];
    
    [[UINavigationBar appearance] setBarTintColor:kColorNavBar];
    [[UINavigationBar appearance] setTintColor:kColorTextNavBar];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : kColorTextNavBar, NSFontAttributeName: [UIFont fontWithName:kFontNameRegular size:kGetFontSize*18.0]}];
    [[UINavigationBar appearance] setTranslucent:NO];
    
    [[UISearchBar appearance] setBarTintColor:kColorNavBar];
    [[UISearchBar appearance] setTintColor:kColorTextNavBar];
    [[UISearchBar appearance] setScopeBarButtonTitleTextAttributes:@{NSForegroundColorAttributeName : kColorTextNavBar, NSFontAttributeName: [UIFont fontWithName:kFontNameRegular size:kGetFontSize*18.0]} forState:UIControlStateNormal];
    [[UISearchBar appearance] setTranslucent:NO];

    // Back Button Appearance
    [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTitleTextAttributes:
     @{NSForegroundColorAttributeName:kColorTextLight,
       NSFontAttributeName:[UIFont fontWithName:kFontNameLight size:14.0f]} forState:UIControlStateNormal];
    //
    
    [self.window setRootViewController:_navController];
    
    //// Permet d'avoir du son même en mode "silence" du device
    AudioSessionInitialize (NULL, NULL, NULL, NULL);
    AudioSessionSetActive(true);
    // Allow playback even if Ring/Silent switch is on mute
    UInt32 sessionCategory = kAudioSessionCategory_MediaPlayback;
    AudioSessionSetProperty (kAudioSessionProperty_AudioCategory,
                             sizeof(sessionCategory),&sessionCategory);
    ////
    
    [self.window makeKeyAndVisible];
    [Fabric with:@[CrashlyticsKit]];
    return YES;
}

- (void)reloadAllApp {
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : kColorTextNavBar, NSFontAttributeName: [UIFont fontWithName:kFontNameRegular size:kGetFontSize*18.0]}];
    [[UISearchBar appearance] setScopeBarButtonTitleTextAttributes:@{NSForegroundColorAttributeName : kColorTextNavBar, NSFontAttributeName: [UIFont fontWithName:kFontNameRegular size:kGetFontSize*18.0]} forState:UIControlStateNormal];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    // Notifications Push
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
//    SDImageCache *imageCache = [SDImageCache sharedImageCache];
//    [imageCache clearMemory];
//    [imageCache clearDisk];
}

#pragma mark - Notifications Push

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    DLog(@"%@", deviceToken);
    // Store the deviceToken in the current installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];
}

- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [PFPush handlePush:userInfo];
    
//    NSLog(@"[WPAppDelegate] Push : %@", userInfo);
    NSString *postID = [NSString stringWithFormat:@"%@", [userInfo objectForKey:@"post_id"]];
    if (![postID isEqualToString:@"0"]) {
        
        // Ouvrir l'article
        WPDetailArticleViewController *detailController = [[WPDetailArticleViewController alloc] initWithArticleID:[postID intValue] andDelegate:_wprootController andType:ArticleStandalone];
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:detailController];
        [navController.navigationBar setTranslucent:NO];
        
        [self.window.rootViewController presentViewController:navController animated:YES completion:nil];
    }
}

#pragma mark - Rotation

- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{
    NSUInteger orientations = UIInterfaceOrientationMaskPortrait;
    
    if (_fullScreenVideoIsPlaying) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return orientations;
    }
}

@end

@implementation UIViewController (rotate)

-(BOOL)shouldAutorotate {
    return YES;
}
@end
