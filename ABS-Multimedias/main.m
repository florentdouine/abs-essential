//
//  main.m
//  WordPress app
//
//  Created by Virginie Delaitre on 13/05/2014.
//  Copyright (c) 2014 Toutdanslapoche. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([WPAppDelegate class]));
    }
}
