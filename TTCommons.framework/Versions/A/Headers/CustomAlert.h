//
//  CustomAlert.h
//  Logos Quizz
//
//  Created by Virginie on 05/04/13.
//  Copyright (c) 2013 toutdanslapoche. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CustomAlert : NSObject {
    
    BOOL _isLocked;
    NSString *_fontName;
}

@property (nonatomic, strong) UIView *view;

+ (CustomAlert*)shared;

- (void)animateWithMessage:(NSString*)message andView:(UIView*)view;
- (void)animateWithMessage:(NSString*)message withFont:(NSString*)fontName andView:(UIView*)view;

@end
