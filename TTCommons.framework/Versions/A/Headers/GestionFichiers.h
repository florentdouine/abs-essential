//
//  GestionFichiers.h
//  Dofus Guide
//
//  Created by Virginie Delaitre on 21/02/12.
//  Copyright (c) 2012 toutdanslapoche. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GestionFichiers : NSObject

+ (GestionFichiers*)shared;

// Fichiers plist
- (NSString *)creationFichierPlist:(NSString *)nomFichierPlist;
- (NSString *)recupFichierPlist:(NSString *)nomFichierPlist;
- (void)supprimerFichierPlist:(NSString *)nomFichierPlist;
// Fichiers JSON
- (NSString *)creationFichierJSON:(NSString *)nomFichierJSON;
- (void)supprimerFichierJSON:(NSString *)nomFichierJSON;
// Fichiers
- (NSString *)creationFichier:(NSString *)nomFichier andSuffixe:(NSString *)suffixe;
- (NSString *)recupFichier:(NSString *)nomFichier andSuffixe:(NSString *)suffixe;

@end