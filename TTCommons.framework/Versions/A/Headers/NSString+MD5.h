//
//  NSString+MD5.h
//  Visites Spirituelles
//
//  Created by Virginie Delaitre on 19/08/2015.
//  Copyright (c) 2015 CFM Spiritual Visits. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MD5)

- (NSString *)MD5String;

@end
