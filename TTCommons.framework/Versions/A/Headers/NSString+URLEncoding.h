//
//  NSString+URLEncoding.h
//  MacinPoche
//
//  Created by Virginie Delaitre on 11/02/2014.
//  Copyright (c) 2014 toutdanslapoche. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (URLEncoding)
-(NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding;
@end
