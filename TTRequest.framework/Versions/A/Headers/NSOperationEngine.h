//
//  NSOperationEngine.h
//  Twitter
//
//  Created by Virginie Delaitre on 20/09/12.
//  Copyright (c) 2012 Virginie Delaitre. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol NSOperationDelegate <NSObject>
@end

@interface NSOperationEngine : NSObject {
    NSOperationQueue *queue;
}

@property (nonatomic, unsafe_unretained) id<NSOperationDelegate> delegate;
@property (nonatomic, retain) NSOperationQueue *queue;

+ (NSOperationEngine*) sharedOperationEngine;

- (void) addInvocationOperationToQueue:(NSInvocationOperation*)invocationOperation;

@end
