//
//  RESTEngine.h
//  Dofus Guide
//
//  Created by Virginie Delaitre on 26/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASINetworkQueue.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"

@interface RESTEngine : NSObject 

@property (nonatomic, strong) NSString *accessToken;
@property (nonatomic, strong) NSString *keyToken;
@property (nonatomic, strong) NSString *valueToken;

+ (RESTEngine*) sharedWithKey:(NSString*)key andToken:(NSString*)token;
- (ASIFormDataRequest*) prepareRequestForUrlString:(NSString *)url;

@end
