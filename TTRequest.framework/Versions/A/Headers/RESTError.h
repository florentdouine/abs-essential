//
//  RESTError.h
//  Dofus Guide
//
//  Created by Virginie Delaitre on 26/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RESTError : NSError

@property (nonatomic, unsafe_unretained) NSString *errorCode;

@end
