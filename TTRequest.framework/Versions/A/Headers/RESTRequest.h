//
//  RESTRequest.h
//  Dofus Guide
//
//  Created by Virginie Delaitre on 26/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIFormDataRequest.h"
#define kRequestErrorDomain @"HTTP_ERROR"
#define kBusinessErrorDomain @"BIZ_ERROR"

@interface RESTRequest : ASIFormDataRequest

@property (nonatomic, strong) NSError *restError;

-(NSMutableDictionary*) responseDictionary;

@end
